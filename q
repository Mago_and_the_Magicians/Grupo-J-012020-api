[33mcommit 0cd5e5d49a386b3288460bb26fc2139cfa8383d6[m[33m ([m[1;36mHEAD -> [m[1;32m14-poder-terminar-una-compra[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 21:34:49 2020 -0300

    finished checkout!

[33mcommit 0b1c834c176aef5faadd3953b43faf9697328164[m[33m ([m[1;31morigin/14-poder-terminar-una-compra[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 21:08:41 2020 -0300

    Added status to cart

[33mcommit 7ebd3995d738cec420609434d86d6663ec9865da[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 20:45:01 2020 -0300

    changed order put

[33mcommit 6b38420033ba1faee31d376596ad87ced1dc72fb[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 20:06:20 2020 -0300

    processed shipping to payment

[33mcommit cd889c7cee3388b77705cfdfda65a5649ebba59d[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;32mmaster[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 00:54:05 2020 -0300

    modified mail service

[33mcommit d627a9987c2021d525702f68c56684dbc147dfff[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 00:27:20 2020 -0300

    modified emailService

[33mcommit 8747e475f48fbc2eb17e9c12261f00689e336dce[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 00:01:12 2020 -0300

    CORS solved

[33mcommit ad2e052f508de640ac64ff31622bd625e4f91a0d[m
Merge: ed47faa 190567d
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 19 02:52:32 2020 +0000

    Merge branch '13-poder-pasar-de-estado-un-carrito' into 'master'
    
    Resolve "Poder pasar de estado un carrito"
    
    Closes #13
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!12

[33mcommit 190567dd0c5d53096919fdec170d0620e9d54bec[m[33m ([m[1;31morigin/13-poder-pasar-de-estado-un-carrito[m[33m, [m[1;32m13-poder-pasar-de-estado-un-carrito[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sat Jul 18 22:01:06 2020 -0300

    turns added and a lot of modifications

[33mcommit 9f1ccf6feda1c4d09507f819946b6555b24263f6[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sat Jul 18 00:12:38 2020 -0300

    added controller to upload product, another to upload variants, and another to upload MarketVariants

[33mcommit 494c69edf1a715e9b98f826a6f367b6f9e2c28df[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 16 23:57:28 2020 -0300

    added auth service to delete responsibility of controller. added get methods /products/list ; /variants/list ;  post of /market-variants and post/put of stores

[33mcommit c9098128ba44efc2012159dfcb584335a109c05f[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 16 00:55:58 2020 -0300

    modified tests

[33mcommit 3c9594be6f825009d13b0c981e00ba84ef7a6ba8[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 16 00:37:50 2020 -0300

    added aspect, logger, emailservice, arch test

[33mcommit 6f192a5d7909d8292fec01506b3cc3b16a3b4212[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Jul 14 15:52:09 2020 -0300

    added validations to cart order

[33mcommit f25380094b5e0337b04c54b2af0ec628802dff7d[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Jul 14 13:41:53 2020 -0300

    modified /products

[33mcommit 3571026fba833326b3ec7b18c3263daaefdb0eb7[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Jul 14 01:08:31 2020 -0300

    added checkout controller

[33mcommit 8137716bb94ea50e76c2cf4aacdff41a36ee5c72[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Jul 14 01:05:29 2020 -0300

    modified jwt and data loader

[33mcommit d20db5b8829722cc7e86f0095974695672cd1e44[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 19:57:30 2020 -0300

    modified data loader

[33mcommit 2447c8a275a82ff7ab6c7c4f2635ecdc28919d83[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 18:37:07 2020 -0300

    added some things and exception handling

[33mcommit ed47faa1e6afa58aaaf8b4e7fec532f10c1eb150[m
Merge: 8c3aa14 a96bf82
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 20:26:20 2020 +0000

    Merge branch '12-poder-cargar-una-lista-de-line-items-en-un-carrito-carrito-no-tiene-que-manejarse-por-id-sino' into 'master'
    
    Resolve "Poder cargar una lista de line items en un carrito, carrito no tiene que manejarse por id sino por usuario"
    
    Closes #12
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!11

[33mcommit a96bf8273767b7732f80c684bfd9ecc32bf96d4c[m[33m ([m[1;31morigin/12-poder-cargar-una-lista-de-line-items-en-un-carrito-carrito-no-tiene-que-manejarse-por-id-sino[m[33m, [m[1;32m12-poder-cargar-una-lista-de-line-items-en-un-carrito-carrito-no-tiene-que-manejarse-por-id-sino[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 17:17:28 2020 -0300

    modifications in cart response

[33mcommit 06c8569651489ae77e48893b7ed75fba4617a24b[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 14:58:49 2020 -0300

    modified time formats and naming in dto's

[33mcommit 583fbf00a250405810e8209866cfce12b82eb99d[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 13:02:54 2020 -0300

    deleted unused dependency

[33mcommit 2acb88254d51ce4bf36744604ba70e0f824b2e56[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jul 13 12:10:26 2020 -0300

    modified data to add code to needed entities

[33mcommit 411b4eaa0ac14b418080002ca657e4e460f0b264[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 12 01:07:22 2020 -0300

    modified store endpoint to validate modifications by user

[33mcommit 78b6a9beede598516f3157743cb7bba644e45d56[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sat Jul 11 16:30:30 2020 -0300

    added open times partially

[33mcommit 922b84d6d70cf8a8480d995acd2df0adb9bd4aed[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sat Jul 11 15:16:53 2020 -0300

    added patch with a list of products

[33mcommit 8c3aa1464ebf220016813a0aa6694e3d661a983d[m
Merge: 7bc209e 8f68b83
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Fri Jul 10 22:28:21 2020 +0000

    Merge branch '11-poder-agregar-productos-a-un-carrito' into 'master'
    
    now we can add products to a cart
    
    Closes #11
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!10

[33mcommit 8f68b83a87a86014ac4b7edbe201fe3f6d283142[m[33m ([m[1;31morigin/11-poder-agregar-productos-a-un-carrito[m[33m, [m[1;32m11-poder-agregar-productos-a-un-carrito[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Fri Jul 10 19:09:48 2020 -0300

    now we can add products to a cart

[33mcommit 7bc209e9ddb4df24da54502cb3c8e05a83304b3e[m
Merge: a1ace49 a8f9902
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Fri Jul 10 18:41:36 2020 +0000

    Merge branch '10-crear-un-carrito' into 'master'
    
    Resolve "Crear un carrito"
    
    Closes #10
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!9

[33mcommit a1ace49a4de9e8bdf1cc2d444ee8965cc276cb39[m
Merge: 7c6d82a c6f131e
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Fri Jul 10 18:18:29 2020 +0000

    Merge branch '9-pasar-toda-la-funcionalidad-de-market-a-store' into 'master'
    
    Resolve "Pasar toda la funcionalidad de market a store"
    
    Closes #9
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!8

[33mcommit a8f99023cf6c1603042782aeda6a9b2418af031b[m[33m ([m[1;31morigin/10-crear-un-carrito[m[33m, [m[1;32m10-crear-un-carrito[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Fri Jul 10 15:17:27 2020 -0300

    cart can be created

[33mcommit 698120df5090725ab30f22d0d86307aae15dd905[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 22:44:16 2020 -0300

    WIP marketOrder

[33mcommit 16079e056b5cef80b051a593ae3eea8f4c68bbd7[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 19:05:05 2020 -0300

    fixed facebook login status

[33mcommit c6f131e32eccb526c137f8b6a75dfcc5ddea7a17[m[33m ([m[1;31morigin/9-pasar-toda-la-funcionalidad-de-market-a-store[m[33m, [m[1;32m9-pasar-toda-la-funcionalidad-de-market-a-store[m[33m)[m
Merge: bfea250 7c6d82a
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 16:18:47 2020 -0300

    Merge branch 'master' of gitlab.com:Mago_and_the_Magicians/Grupo-J-012020-api into 9-pasar-toda-la-funcionalidad-de-market-a-store

[33mcommit 7c6d82aac97196b2a802a7344d71a4cdbfdb10cc[m
Merge: f018d5d 982979d
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 19:16:00 2020 +0000

    Merge branch '8-crear-direcciones-a-partir-de-un-endpoint-para-el-usuario-logeado' into 'master'
    
    added address post
    
    Closes #8
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!7

[33mcommit bfea250e39acf1b82f5dcbd78e7c162d5d305afc[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 15:05:03 2020 -0300

    modified market to store in dto

[33mcommit 982979ddae27b01ff3bbe2cfcd77e2701f6519f2[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 14:58:45 2020 -0300

    added address post

[33mcommit f018d5d71a7674dfe188ac689a5a1f84b4974245[m
Merge: 00d6fb6 60a6020
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu Jul 9 11:48:52 2020 -0300

    Merge branch 'master' of gitlab.com:Mago_and_the_Magicians/Grupo-J-012020-api

[33mcommit 60a602016b885d89bda9074d74fd0db254805630[m
Merge: 3d58bd2 39baffa
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jul 5 22:59:13 2020 +0000

    Merge branch 'oauth-testing' into 'master'
    
    Oauth testing
    
    Closes #7
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!6

[33mcommit 39baffa3b23bf46f4de4d7d22bf0d3336c7b82a0[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun Jul 5 19:51:47 2020 -0300

    fixed tests and redirect uri

[33mcommit 27ba33c0661f0f9d8e9a9a9c67e1a486e4a591ba[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun Jul 5 19:44:20 2020 -0300

    Finished oauth with facebook

[33mcommit 33f529db8ac149c257f317228a11b3f3fee4ec66[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Sun Jul 5 10:04:54 2020 -0300

    updated pom and jwtutils

[33mcommit 2c28d04cbabccf2b0767f80757b71cb072b4b9ef[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Tue Jun 30 18:05:12 2020 -0300

    modified jwtUtils

[33mcommit a0a6244929dd9ebf818289c9e966a128142465a6[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Tue Jun 30 18:04:08 2020 -0300

    changed ahuthtokenfilter

[33mcommit 3d58bd247bb7e2b688d73e0b86e533b2cabf70cc[m
Merge: 8387cf1 8f10bef
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sun Jun 28 15:32:02 2020 +0000

    Merge branch '6-crear-login' into 'master'
    
    Resolve "Crear login"
    
    Closes #6
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!5

[33mcommit 8f10befbe0e9784680b16c42038d2eacd8e7abab[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun Jun 28 12:11:37 2020 -0300

    changed repositories to services

[33mcommit c2d527bd7509144a7ae9d92315dd75802e6301d2[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun Jun 28 11:57:35 2020 -0300

    updated login

[33mcommit 7a291b3272f329c4de40dfa288d6552cf3c87882[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun Jun 28 11:15:33 2020 -0300

    changed endpoints to login / register

[33mcommit 1f82dff39f4bb83287b88522984564edd14b6968[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Tue Jun 23 20:06:13 2020 -0300

    deleted login controller

[33mcommit 84f0576b7761411e24c7a0db041324595e01395b[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Tue Jun 23 20:03:59 2020 -0300

    added exception to auth for /products/**

[33mcommit 3f8937b60226ad810317671cabad4100d641d7da[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Tue Jun 23 19:49:32 2020 -0300

    fixed test properties

[33mcommit 215504fa8f514e184dadb11884b1050dce0c4b28[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Tue Jun 23 19:40:49 2020 -0300

    login working, except for get to products

[33mcommit 8387cf17e09cca4d202cf7d870db0d31c92bccab[m
Merge: 0e13762 ef1ec39
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Sat Jun 20 18:20:20 2020 +0000

    Merge branch '5-crear-filtro-para-productos' into 'master'
    
    Resolve "Crear filtro para productos"
    
    Closes #5
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!4

[33mcommit ef1ec3930bfa566b7f3bb881404277ae0fcbb182[m
Author: gunns <garamburu@exisoft.com.ar>
Date:   Sat Jun 20 13:25:15 2020 -0300

    changed deploy app name

[33mcommit 969a5f6cca5d26a771eed6f0296e2509c5354c6a[m
Author: garamburu <garamburu@exisoft.com.ar>
Date:   Sat Jun 20 13:15:29 2020 -0300

    modified gitlab ci to deploy to development server

[33mcommit 534ade4eba716f55d5674baaaed80caa3938f55a[m
Merge: f03582f 013ae30
Author: garamburu <garamburu@exisoft.com.ar>
Date:   Sat Jun 20 12:55:04 2020 -0300

    Merge branch '5-crear-filtro-para-productos' of gitlab.com:Mago_and_the_Magicians/Grupo-J-012020-api into 5-crear-filtro-para-productos

[33mcommit f03582f4fefefc61e5e57b651257edafcbdaf62c[m
Merge: af63f95 0e13762
Author: garamburu <garamburu@exisoft.com.ar>
Date:   Sat Jun 20 12:54:14 2020 -0300

    Merge branch 'master' of gitlab.com:Mago_and_the_Magicians/Grupo-J-012020-api into 5-crear-filtro-para-productos

[33mcommit 0e13762fa7c0115df86ecbea59d4b67919005463[m[33m ([m[1;33mtag: Entrega_2_1.0[m[33m)[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Thu Jun 18 23:12:48 2020 -0300

    added release notes
    ]

[33mcommit 013ae3010206d60cf54e08cd3116b49060b64e37[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Thu Jun 18 22:58:05 2020 -0300

    changed category to correct one

[33mcommit b323b662fca52acadb47afdbaaa9d405bdb00f86[m
Merge: 1cb037a af63f95
Author: gunns <gunns.gnr@gmail.com>
Date:   Thu Jun 18 22:34:07 2020 -0300

    Merge branch '5-crear-filtro-para-productos' of gitlab.com:Mago_and_the_Magicians/Grupo-J-012020-api into 5-crear-filtro-para-productos

[33mcommit 1cb037aea8e06ad888d146bbd9c3140b25e5def2[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Thu Jun 18 22:33:50 2020 -0300

    added category filter and checked min and max price validations

[33mcommit af63f95f440b9df6d9d6b99118d629acd4de15b3[m
Merge: af895a9 8a60e77
Author: garamburu <garamburu@exisoft.com.ar>
Date:   Tue Jun 16 19:41:58 2020 -0300

    Merge branch '5-crear-filtro-para-productos' of gitlab.com:Mago_and_the_Magicians/Grupo-J-012020-api into 5-crear-filtro-para-productos

[33mcommit af895a96e55218ab155ca74a55374a85d42e1af5[m
Author: garamburu <garamburu@exisoft.com.ar>
Date:   Tue Jun 16 19:40:45 2020 -0300

    fixed product controller and create request body

[33mcommit 2f97ee1a8270da5bdc7b6cfce588ffbda90a0954[m
Merge: 50b62b6 8a60e77
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Jun 16 21:10:49 2020 +0000

    Merge branch '5-crear-filtro-para-productos' into 'master'
    
    Resolve "Crear filtro para productos"
    
    Closes #5
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!3

[33mcommit 8a60e771f49a747829af0287ec089bd233f47fa5[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Tue Jun 16 17:50:07 2020 -0300

    fixed image in data loader

[33mcommit 4864b5ca05809ecd6ee97d515f2e414b3636feb6[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Tue Jun 16 17:42:47 2020 -0300

    fixed category but incomplete filter

[33mcommit bed5cfa635c4d040dd64ba186dae7c07c4658850[m[33m ([m[1;31morigin/5-crear-filtro-para-productos[m[33m, [m[1;32m5-crear-filtro-para-productos[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Jun 16 16:54:42 2020 -0300

    added data to loader

[33mcommit 2937de02479390e6a6c741eea38371e529885fa7[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 23:51:27 2020 -0300

    fixed store with user

[33mcommit 5d1a5c93154e8d48d94556934f85400ba39549cd[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 23:32:18 2020 -0300

    variants lite and products lite, added store

[33mcommit ffe01ae44715beb6bac8d1fb65a14963f98d3cc3[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 22:44:00 2020 -0300

    Address and user

[33mcommit 50b62b65846dc84863f72a4f1551101f40bab3c5[m
Merge: 7023dfe 95cb1ef
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Mon Jun 15 22:42:29 2020 +0000

    Merge branch '4-crear-producto-y-variante' into 'master'
    
    Resolve "Crear Producto y Variante"
    
    Closes #4
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!2

[33mcommit 728790a292851db0555afd8698ff3e52cd183171[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 16:38:48 2020 -0300

    products update

[33mcommit 4e44adb816a2e9771cce60922a0936db658fecdc[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 13:07:15 2020 -0300

    fixed and modified tests

[33mcommit 1f23cde3f91dfd1759a4b127de297c9263efde63[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 12:38:42 2020 -0300

    added setter in product

[33mcommit af7ef81a9f810bf3ceebdf27e062295706231e4d[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 12:36:20 2020 -0300

    Product list with empty filters

[33mcommit 7f3358b94a17c68eadf34345b413da793fce37c0[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon Jun 15 09:55:29 2020 -0300

    WIP filters

[33mcommit 0ab5747bc55b8803605a00a99e26b32108df4ebb[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun Jun 14 12:19:42 2020 -0300

    deleted iml

[33mcommit 95cb1ef3a2d8cba80f3a197705bad03bac832b41[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sun May 31 16:03:22 2020 -0300

    WIP adding product category

[33mcommit c3e41a476fa7abaf3bb1acdc158436acfecd27bd[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon May 25 20:31:05 2020 -0300

    Tag and Entity Refactor

[33mcommit a9a594edad385cf170916d61e016d613ad083408[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon May 25 17:50:44 2020 -0300

    small modifications

[33mcommit bda88f1b63441959066a4afbde893f4d15331021[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Mon May 25 17:24:31 2020 -0300

    WIP adding service implementations

[33mcommit 5a22e255ee8d20da017fdefd086a898cde378f25[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sat May 23 20:41:24 2020 -0300

    WIP: advancing with tags

[33mcommit a11e9c03fdd30ea1e2b97ba293b5977e277b7935[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sat May 23 17:56:25 2020 -0300

    WIP: testing products and adding tag functions

[33mcommit 680cae1b11c9606106e29434739c8a9a50f6ae5b[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Sat May 16 20:45:47 2020 -0300

    WIP: Product specs

[33mcommit d14f2078ca6176c229e60c460188db24cb2e8afd[m
Author: gunns <gunns.gnr@gmail.com>
Date:   Tue May 12 01:20:08 2020 -0300

    WIP market - variants - producs

[33mcommit 7d3defea36d652831b64d4b130d1c0fb75ac0485[m[33m ([m[1;31morigin/4-crear-producto-y-variante[m[33m, [m[1;32m4-crear-producto-y-variante[m[33m)[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 12 00:26:05 2020 -0300

    WIP: modified variant and product

[33mcommit 7023dfe930d1a3c3b9859d18ea889f4263d10359[m[33m ([m[1;31morigin/3-crear-funcionalidad-taxon[m[33m)[m
Merge: fe4c253 e122df3
Author: Javier Corbalan - (Koru el Mago) <korukamui@gmail.com>
Date:   Sat May 9 15:05:30 2020 +0000

    Merge branch 'model-definition' into 'master'
    
    Model definition
    
    See merge request Mago_and_the_Magicians/Grupo-J-012020-api!1

[33mcommit e122df3d1d544c4127118a3769d904e5f100ddfd[m
Merge: 79be53e fe4c253
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sat May 9 12:03:44 2020 -0300

    Merge branch 'master' into model-definition

[33mcommit 79be53e71be94d64ff661b748d8b923082b4d45b[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sat May 9 02:42:56 2020 -0300

    added uml

[33mcommit 6211cd3a377d9cc5fcdcc8e4a4519ea9fb40c508[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sat May 9 02:23:20 2020 -0300

    added uml

[33mcommit dc2b346867377a1d8c69537b3c99cd710a286ae3[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sat May 9 02:08:51 2020 -0300

    added uml

[33mcommit 00d6fb66260133c8442696d59a42b86fa60c4ec8[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Thu May 7 20:51:44 2020 -0300

    deleted iml, to open this project delete .idea folder

[33mcommit fe4c253ce27ff69e9442d13350087923d923e73c[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 5 23:55:24 2020 +0000

    Update .gitlab-ci.yml

[33mcommit 54594187d23a2482da66ddd4e842f4176941bcad[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 5 23:54:19 2020 +0000

    Update .gitlab-ci.yml

[33mcommit 45a3e12f5e5c3b9a5aad4ac359d8ba9a20903000[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 5 23:52:56 2020 +0000

    Update .gitlab-ci.yml

[33mcommit c93e5a1f66a00c0f9fd12fa85b5b354e13afced0[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 5 23:49:03 2020 +0000

    Update .gitlab-ci.yml

[33mcommit 71a6e54414ca2c90d4969abbf7d45d8bf27439f9[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 5 23:43:56 2020 +0000

    Update .gitlab-ci.yml

[33mcommit a1ac50ddf3e8616c11bfd2d3ff19dbb1724131db[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue May 5 23:40:57 2020 +0000

    Add .gitlab-ci.yml

[33mcommit caa02bdcabb25dd6a9abcf9fce6407e53f5e9d5e[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Mon May 4 00:08:07 2020 -0300

    MIP started offfer Module

[33mcommit 13df4825a46b3110f0820e505922a82fbf99401f[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sun May 3 16:46:59 2020 -0300

    added more test

[33mcommit 30d9891857158b59c0f23e6fdd9ade5b642864ff[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sat May 2 18:10:08 2020 -0300

    fixed test

[33mcommit 79ad6c4702465d22aa72fd56c65aa0a49afe45e7[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sat May 2 00:17:56 2020 -0300

    added new test to fix

[33mcommit 5225ae2e7e6c7392d58dc7dd16a76c606ca1e13e[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri May 1 23:20:26 2020 -0300

    filter finished not probed

[33mcommit c2257cebe817def8c3313a519f884c9a75deff8f[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri May 1 23:17:04 2020 -0300

    filter finished not probed

[33mcommit 8ddc40ac05cc455a4ada522cc766e36dc48f1e5a[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Wed Apr 29 23:28:44 2020 -0300

    magic in process

[33mcommit fa53e3ba78942b888ca5b57d3368998f2a6fd0a4[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sun Apr 26 16:21:05 2020 -0300

    added first controller

[33mcommit 87be9cca3f4f59199bd33b7a846bdb936eca9fb1[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sun Apr 26 15:57:26 2020 -0300

    WIP

[33mcommit e944d7ecd196a6cef4b966ae539c1b1d55db99e9[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sun Apr 26 13:56:18 2020 -0300

    WIP filter firts idea!

[33mcommit b4e3a51dd2a1a6785f3c6279c5eb280421a96548[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Mon Apr 20 09:20:33 2020 -0300

    added uml

[33mcommit b2714cd9055dcae95e4254e9830208e46aad429f[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Apr 14 21:32:38 2020 -0300

    WIP

[33mcommit 9897013e19f8b126d1e4cb84d0e1e1bc3f803829[m
Author: Gustavo Aramburu <gunns.gnr@gmail.com>
Date:   Tue Apr 14 20:29:42 2020 -0300

    WIP model definition

[33mcommit 3676ad46eeb1a3072fc8371f9eefc4fc3843fd61[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Sun Apr 12 13:43:24 2020 -0300

    changes on travis yml

[33mcommit af7067a4bd1cb3826a2f1b2f0299ba1afa17b5dc[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 21:23:21 2020 -0300

    added heroku deploy config

[33mcommit f3068252dc42610472791e3d0aae40d7b951ff13[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 20:54:28 2020 -0300

    removed intellij files

[33mcommit 787402c0ce0f750126ec4987d42db7fe501f8a2a[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 20:49:42 2020 -0300

    added install script to config file

[33mcommit bb8568c3ab05c5fba4c27b1210e45850cdccae8f[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 20:46:24 2020 -0300

    removed target and idea folders

[33mcommit 0df5a949e77776dfdf2bb7ce46f411e310f0ba5e[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 20:44:52 2020 -0300

    added jdk to ci config

[33mcommit d577d92879d78a0572a92b5eda60d472a28dd0cd[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 20:44:14 2020 -0300

    added jdk to ci config

[33mcommit fa7cc98a7f9405d17b369af204572dafdb598af9[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 20:07:04 2020 -0300

    added travis file

[33mcommit 052fc9840443d3bf5b6da6551751e9343f36d99c[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 17:31:44 2020 -0300

    initial commit

[33mcommit 25ff5c9cf82729d78dbd4c1f586022fdedad1e37[m
Author: Koru el Mago <javier.corbalan93@gmail.com>
Date:   Fri Apr 10 14:55:52 2020 -0300

    Initial commit
