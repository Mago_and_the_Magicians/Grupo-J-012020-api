package Grupo.C2B9.back.architecture;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import javax.persistence.Entity;
import javax.persistence.metamodel.StaticMetamodel;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = "Grupo.C2B9.back")
public class ArchitectureTest {

  @ArchTest
  public static final ArchRule accessRule = classes()
    .that()
    .resideInAPackage("..service..")
    .should().onlyBeAccessed()
    .byAnyPackage("..webservices..", "..service..");


  @ArchTest
  public static final ArchRule repositoryRule = classes()
    .that()
    .resideInAPackage("..repository")
    .should().beInterfaces();

  @ArchTest
  public static final ArchRule staticMetamodelRule = classes()
    .that()
    .resideInAPackage("..model..")
    .and().haveSimpleNameContaining("_").should().beAnnotatedWith(StaticMetamodel.class);


  @ArchTest
  public static final ArchRule entityRule = classes()
    .that()
    .resideInAPackage("..model..").
      and().haveSimpleNameNotContaining("_")
    .and().haveSimpleNameNotContaining("Test")
    .and().areNotEnums()
    .and()
    .areNotNestedClasses()
    .should().beAnnotatedWith(Entity.class);
}