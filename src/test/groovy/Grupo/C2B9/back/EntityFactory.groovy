package Grupo.C2B9.back

import Grupo.C2B9.back.model.market.Market
import Grupo.C2B9.back.model.market.variant.MarketVariant
import Grupo.C2B9.back.model.product.Product
import Grupo.C2B9.back.model.product.Variant
//import Grupo.C2B9.back.model.product.tag.EntityType

class EntityFactory {


    static Market anyMarket() {
        return new Market().setName(generateRandomName())
    }

    static Product anyProduct() {
        return new Product().setName(generateRandomName())
    }

    static Variant anyVariant(Product product) {
        return new Variant().setProduct(product)
    }

    static MarketVariant anyMarketVariant(Market market, Variant variant) {
        return new MarketVariant().setMarket(market).setVariant(variant)
    }
/*
    static Tag anyProductTag(Product product) {
        return new Tag()
                .setEntityId(product.getId())
                .setEntityType(EntityType.PRODUCT)
    }
    static Tag anyVariantTag(Variant variant) {
        return new Tag()
                .setEntityId(variant.getId())
                .setEntityType(EntityType.VARIANT)
    }
*/

    static String generateRandomName() {
        return new Random()
                .ints(97, 123)
                .limit(12)
                .collect({
                    i ->
                        new StringBuilder().appendCodePoint(i).toString()
                }).stream()
                .reduce({s, s2 -> s.concat(s2)})
                .orElse("random name failure")
    }

}
