package Grupo.C2B9.back.model

import Grupo.C2B9.back.EntityFactory
import Grupo.C2B9.back.repository.MarketRepository
import Grupo.C2B9.back.repository.MarketVariantRepository
import Grupo.C2B9.back.repository.ProductRepository
import Grupo.C2B9.back.repository.VariantRepository
import Grupo.C2B9.back.repository.specs.ProductSpecs
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import spock.lang.Specification

import static org.hamcrest.Matchers.hasSize

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ProductSpecTest extends Specification  {

    @Autowired(required = false)
    ProductRepository productRepository

    @Autowired(required = false)
    VariantRepository variantRepository

    @Autowired(required = false)
    MarketRepository marketRepository

    @Autowired(required = false)
    MarketVariantRepository marketVariantRepository;

    def product = EntityFactory.anyProduct()
    def product2 = EntityFactory.anyProduct()
    def product3 = EntityFactory.anyProduct()
    def variant1p1 = EntityFactory.anyVariant(product)
    def variant2p1 = EntityFactory.anyVariant(product)
    def variant1p2 = EntityFactory.anyVariant(product2)
    def market = EntityFactory.anyMarket()
    def market2 = EntityFactory.anyMarket()
    def marketVariant = EntityFactory.anyMarketVariant(market,variant1p1)
    def marketVariant2 = EntityFactory.anyMarketVariant(market2,variant1p1)
    def marketVariant3 = EntityFactory.anyMarketVariant(market,variant1p2)
    def marketVariant4 = EntityFactory.anyMarketVariant(market2,variant1p2)
    def marketVariant5 = EntityFactory.anyMarketVariant(market,variant2p1)
    def marketVariant6 = EntityFactory.anyMarketVariant(market2,variant2p1)


    def setup() {
        // save market
        market = marketRepository.save(market)
        market2 = marketRepository.save(market2)
        // save variant
        product = productRepository.save(product)
        product2 = productRepository.save(product2)
        product3 = productRepository.save(product3)
        //save variants
        variant1p1 = variantRepository.save(variant1p1)
        variant2p1 = variantRepository.save(variant2p1)
        variant1p2 = variantRepository.save(variant1p2)
        // save market variants
        marketVariant = marketVariantRepository.save(marketVariant)
        marketVariant2 = marketVariantRepository.save(marketVariant2)
        marketVariant3 = marketVariantRepository.save(marketVariant3)
        marketVariant4 = marketVariantRepository.save(marketVariant4)
        marketVariant5 = marketVariantRepository.save(marketVariant5)
        marketVariant6 = marketVariantRepository.save(marketVariant6)
    }


    def "filter by name give me all the products with the word in the name"() {
        given:
        product  = productRepository.save(product.setName("Fanta Light"))
        product2 = productRepository.save(product2.setName("Coca cola ligth"))
        product3 = productRepository.save(product3.setName("Fanta"))

        when:
        def products = productRepository.findAll(ProductSpecs.byName("fanta"))

        then:
        products.size() == 2
    }

    def "filter by brand give me all the products with these brand"() {
        given:
        product  = productRepository.save(product.setBrand("Coca"))
        product2 = productRepository.save(product2.setBrand("Fanta"))
        product3 = productRepository.save(product3.setBrand("Fanta"))

        when:
        def products = productRepository.findAll(ProductSpecs.byBrand("Fanta"))

        then:
        products hasSize(2)
    }

    def "filter by min price give me all the products with price less than given"() {
        given:

        marketVariant = marketVariantRepository.save(marketVariant
                .setPrice(400.00f))
        marketVariant2 = marketVariantRepository.save(marketVariant2
                .setPrice(500.00f))
        marketVariant3 = marketVariantRepository.save(marketVariant3
                .setPrice(600.00f))
        when:
        def products = productRepository.findAll(ProductSpecs.byMinPrice("499.00"))

        then:
        products hasSize(2)
    }

    def "filter by min price give me the products if any of your variants has less price"() {
        given:
        marketVariant = marketVariantRepository.save(marketVariant.setPrice(450.00f))
        marketVariant2 = marketVariantRepository.save(marketVariant2.setPrice(550.00f))
        marketVariant3 = marketVariantRepository.save(marketVariant3.setPrice(640.00f))
        when:
        def products = productRepository.findAll(ProductSpecs.byMinPrice("551.00"))

        then:
        products hasSize(1)
    }
    def "filter by max price give me all the products with price greater than given"() {
        given:
        marketVariant = marketVariantRepository.save(marketVariant.setPrice(-450.00f))
        marketVariant2 = marketVariantRepository.save(marketVariant2.setPrice(-555.00f))
        marketVariant3 = marketVariantRepository.save(marketVariant3.setPrice(-666.00f))

        when:
        def products = productRepository.findAll(ProductSpecs.byMaxPrice("-554.00"))

        then:
        products hasSize(2)
    }

    def "filter by max price give me the products if any of your variants has greater price"() {
        given:
        marketVariant = marketVariantRepository.save(marketVariant.setPrice(1.00f))
        marketVariant2 = marketVariantRepository.save(marketVariant2.setPrice(3.00f))
        marketVariant3 = marketVariantRepository.save(marketVariant3.setPrice(2.00f))

        when:
        def products = productRepository.findAll(ProductSpecs.byMaxPrice("2.40"))

        then:
        products hasSize(2)
    }

    def "filter by market give me all the products present in the market"() {
        given:

        marketVariant = marketVariantRepository.save(marketVariant
                .setPrice(4.00f))
        marketVariant2 = marketVariantRepository.save(marketVariant2
                .setPrice(5.00f))
        marketVariant3 = marketVariantRepository.save(marketVariant3
                .setPrice(6.00f))
        when:
        def products = productRepository.findAll(ProductSpecs.byMarket(market.getId().toString()))

        then:
        //marketVariantRepository.getById(marketVariant.getCode()).getMarket().getCode() == market.getCode()
        //productService.getById(variant.getCode()).getVariants().size() > 0
        products hasSize(2)
    }
}
