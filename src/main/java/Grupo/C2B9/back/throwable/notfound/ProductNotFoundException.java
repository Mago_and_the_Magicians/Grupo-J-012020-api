package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class ProductNotFoundException extends NotFoundException {
  public ProductNotFoundException(String message) {
    super(message);
  }
}
