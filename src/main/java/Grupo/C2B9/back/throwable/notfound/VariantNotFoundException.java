package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class VariantNotFoundException extends NotFoundException {
  public VariantNotFoundException(String message) {
    super(message);
  }
}
