package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class ShippingNotFoundException extends NotFoundException {
  public ShippingNotFoundException(String message) {
    super(message);
  }
}
