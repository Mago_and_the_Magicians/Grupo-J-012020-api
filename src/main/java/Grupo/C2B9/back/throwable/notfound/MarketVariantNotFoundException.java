package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class MarketVariantNotFoundException extends NotFoundException {
  public MarketVariantNotFoundException(String message) {
    super("market variant not found, code: " + message);
  }
}
