package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class OrderNotFoundException extends NotFoundException {
  public OrderNotFoundException(String message) {
    super(message);
  }
}
