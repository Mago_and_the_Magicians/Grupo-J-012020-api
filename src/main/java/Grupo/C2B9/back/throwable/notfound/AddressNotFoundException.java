package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class AddressNotFoundException extends NotFoundException {
  public AddressNotFoundException(String message) {
    super(message);
  }
}
