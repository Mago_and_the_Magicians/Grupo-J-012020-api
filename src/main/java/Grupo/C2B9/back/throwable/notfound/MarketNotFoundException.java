package Grupo.C2B9.back.throwable.notfound;

import Grupo.C2B9.back.throwable.NotFoundException;

public class MarketNotFoundException extends NotFoundException {
  public MarketNotFoundException(String message) {
    super(message);
  }
}
