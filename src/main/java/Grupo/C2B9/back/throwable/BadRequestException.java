package Grupo.C2B9.back.throwable;

public abstract class BadRequestException extends RuntimeException {


  public BadRequestException(String message) {
    super(message);
  }
}
