package Grupo.C2B9.back.throwable.unauthorized;

import Grupo.C2B9.back.throwable.UnauthorizedException;

public class AuthenticationTypeException extends UnauthorizedException {
  public AuthenticationTypeException(String message) {
    super(message);
  }
}
