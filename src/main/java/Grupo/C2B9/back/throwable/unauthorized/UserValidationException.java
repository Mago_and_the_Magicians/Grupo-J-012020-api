package Grupo.C2B9.back.throwable.unauthorized;

import Grupo.C2B9.back.throwable.UnauthorizedException;

public class UserValidationException extends UnauthorizedException {
  public UserValidationException(String message) {
    super(message);
  }
}
