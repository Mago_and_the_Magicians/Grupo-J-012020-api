package Grupo.C2B9.back.throwable.unauthorized;

import Grupo.C2B9.back.throwable.UnauthorizedException;

public class AccessNotAllowedException extends UnauthorizedException {
  public AccessNotAllowedException(String message) {
    super(message);
  }
}
