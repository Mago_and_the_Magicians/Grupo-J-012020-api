package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class EmptyIdException extends BadRequestException {
  public EmptyIdException(String message) {
    super(message);
  }
}
