package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class InvalidCheckoutException extends BadRequestException {
  public InvalidCheckoutException(String message) {
    super(message);
  }
}
