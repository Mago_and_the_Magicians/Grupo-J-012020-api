package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class InvalidTurnDateException extends BadRequestException {
  public InvalidTurnDateException(String message) {
    super(message);
  }
}
