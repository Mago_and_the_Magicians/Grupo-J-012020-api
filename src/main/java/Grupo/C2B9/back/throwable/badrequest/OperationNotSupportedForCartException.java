package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class OperationNotSupportedForCartException extends BadRequestException {
  public OperationNotSupportedForCartException(String message) {
    super(message);
  }
}
