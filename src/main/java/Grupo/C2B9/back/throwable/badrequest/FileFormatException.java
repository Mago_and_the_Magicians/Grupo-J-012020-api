package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class FileFormatException extends BadRequestException {
  public FileFormatException(String message) {
    super(message);
  }
}
