package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class InvalidInfoException extends BadRequestException {
  public InvalidInfoException(String message) {
    super(message);
  }
}
