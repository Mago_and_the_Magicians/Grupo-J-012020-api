package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class InvalidStockException extends BadRequestException {

  public InvalidStockException(String message) {
    super(message);
  }
}
