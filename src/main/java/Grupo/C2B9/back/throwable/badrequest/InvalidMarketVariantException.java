package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class InvalidMarketVariantException extends BadRequestException {
  public InvalidMarketVariantException(String message) {
    super(message);
  }
}
