package Grupo.C2B9.back.throwable.badrequest;

import Grupo.C2B9.back.throwable.BadRequestException;

public class InvalidQuantityException extends BadRequestException {
  public InvalidQuantityException(String message) {
    super(message);
  }
}
