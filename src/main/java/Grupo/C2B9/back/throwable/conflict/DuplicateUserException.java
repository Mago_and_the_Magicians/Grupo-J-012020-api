package Grupo.C2B9.back.throwable.conflict;

import Grupo.C2B9.back.throwable.ConflictException;

public class DuplicateUserException extends ConflictException {

  public DuplicateUserException(String message) {
    super(message);
  }
}
