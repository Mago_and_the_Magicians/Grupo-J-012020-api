package Grupo.C2B9.back.throwable.conflict;

import Grupo.C2B9.back.throwable.ConflictException;

public class DuplicateAddressException extends ConflictException {
  public DuplicateAddressException(String message) {
    super(message);
  }
}
