package Grupo.C2B9.back.throwable.conflict;

import Grupo.C2B9.back.throwable.ConflictException;

public class DuplicateProductException extends ConflictException {
  public DuplicateProductException(String message) {
    super(message);
  }
}
