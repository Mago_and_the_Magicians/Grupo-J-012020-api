package Grupo.C2B9.back.throwable.conflict;

import Grupo.C2B9.back.throwable.ConflictException;

public class DuplicateMarketOrderException extends ConflictException {
  public DuplicateMarketOrderException(String message) {
    super(message);
  }
}
