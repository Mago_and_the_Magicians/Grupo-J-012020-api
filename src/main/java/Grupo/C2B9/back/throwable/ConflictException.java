package Grupo.C2B9.back.throwable;

public abstract class ConflictException extends RuntimeException {

  public ConflictException(String message) {
    super(message);
  }
}
