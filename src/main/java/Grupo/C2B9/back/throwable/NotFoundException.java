package Grupo.C2B9.back.throwable;

public abstract class NotFoundException extends RuntimeException {

  public NotFoundException(String message) {
    super(message);
  }
}
