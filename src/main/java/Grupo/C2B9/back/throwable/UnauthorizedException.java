package Grupo.C2B9.back.throwable;

public abstract class UnauthorizedException extends RuntimeException {

  public UnauthorizedException(String message) {
    super(message);
  }
}
