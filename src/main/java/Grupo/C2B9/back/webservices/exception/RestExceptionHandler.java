package Grupo.C2B9.back.webservices.exception;

import Grupo.C2B9.back.throwable.BadRequestException;
import Grupo.C2B9.back.throwable.ConflictException;
import Grupo.C2B9.back.throwable.NotFoundException;
import Grupo.C2B9.back.throwable.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@ControllerAdvice
public class RestExceptionHandler {


  //////////////BAD REQUEST
  @ExceptionHandler({BadRequestException.class})
  public ResponseEntity<Void> handleException(
    BadRequestException badRequestException
  ) {
    return generateExceptionResponse(HttpStatus.BAD_REQUEST, badRequestException.getMessage());
  }

  @ExceptionHandler({RoleNotFoundException.class})
  public ResponseEntity<Void> handleException(
    RoleNotFoundException roleNotFoundException
  ) {
    return generateExceptionResponse(HttpStatus.BAD_REQUEST, "Role was not found");
  }

  @ExceptionHandler({UnsupportedEncodingException.class})
  public ResponseEntity<Void> handleException(
    UnsupportedEncodingException unsupportedEncodingException
  ) {
    return generateExceptionResponse(HttpStatus.BAD_REQUEST, "Invalid Encoding");
  }


  //////////// CONFLICT

  @ExceptionHandler({ConflictException.class})
  public ResponseEntity<Void> handleException(
    ConflictException conflictException
  ) {
    return generateExceptionResponse(HttpStatus.CONFLICT, conflictException.getMessage());
  }


  //////  UNAUTHORIZED

  @ExceptionHandler({UnauthorizedException.class})
  public ResponseEntity<Void> handleException(
    UnauthorizedException unauthorizedException
  ) {
    return generateExceptionResponse(HttpStatus.UNAUTHORIZED, unauthorizedException.getMessage());
  }


  ///////////////// NOT FOUND

  @ExceptionHandler({NotFoundException.class})
  public ResponseEntity<Void> handleException(
    NotFoundException notFoundException
  ) {
    return generateExceptionResponse(HttpStatus.NOT_FOUND, notFoundException.getMessage());
  }


  private ResponseEntity<Void> generateExceptionResponse(HttpStatus httpStatus, String message) {
    return ResponseEntity
      .status(httpStatus)
      .header("Message", message)
      .build();
  }
}