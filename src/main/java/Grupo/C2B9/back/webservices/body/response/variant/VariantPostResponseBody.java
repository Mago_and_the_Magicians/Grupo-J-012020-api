package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;

public class VariantPostResponseBody {

  private Integer id;
  private String code;
  private String productCode;
  private String description;
  private String imageUrl;

  public static VariantPostResponseBody valueOf(Variant variant) {
    return new VariantPostResponseBody()
      .setId(variant.getId())
      .setCode(variant.getCode())
      .setProductCode(variant.getProduct().getCode())
      .setDescription(variant.getDescription())
      .setImageUrl(variant.getImageUrl());
  }

  public Integer getId() {
    return id;
  }

  public VariantPostResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getProductCode() {
    return productCode;
  }

  public VariantPostResponseBody setProductCode(String productCode) {
    this.productCode = productCode;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public VariantPostResponseBody setDescription(String description) {
    this.description = description;
    return this;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public VariantPostResponseBody setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  public String getCode() {
    return code;
  }

  public VariantPostResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
