package Grupo.C2B9.back.webservices.body.response.checkout.marketOrder;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.webservices.body.response.checkout.lineItem.CheckoutLineItemResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class CheckoutStoreOrderResponseBody {

  private String code;
  private double totalAmount;
  private double total;
  private double totalDiscount;
  private List<CheckoutLineItemResponseBody> conflictItems;

  public String getCode() {
    return code;
  }

  public CheckoutStoreOrderResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public List<CheckoutLineItemResponseBody> getConflictItems() {
    return conflictItems;
  }

  public CheckoutStoreOrderResponseBody setConflictItems(List<CheckoutLineItemResponseBody> conflictItems) {
    this.conflictItems = conflictItems;
    return this;
  }

  public static CheckoutStoreOrderResponseBody valueOf(MarketOrder marketOrder) {
    return new CheckoutStoreOrderResponseBody()
      .setCode(marketOrder.getCode())
      .setTotal(marketOrder.getTotalAmount())
      .setTotalAmount(marketOrder.getTotalAmount())
      .setTotalDiscount(0.00)
      .setConflictItems(marketOrder.getLineItems()
        .stream()
        .filter(lineItem -> !lineItem.validStock())
        .map(CheckoutLineItemResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public CheckoutStoreOrderResponseBody setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public CheckoutStoreOrderResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public CheckoutStoreOrderResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }
}
