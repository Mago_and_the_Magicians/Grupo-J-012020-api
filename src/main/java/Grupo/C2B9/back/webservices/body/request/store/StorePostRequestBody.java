package Grupo.C2B9.back.webservices.body.request.store;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;
import Grupo.C2B9.back.webservices.body.request.store.opentime.StoreOpenTimeRequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StorePostRequestBody {

  private String department;
  private String address;
  private String name;
  private List<StoreOpenTimeRequestBody> openTimes;
  private List<MarketCategory> marketCategories = new ArrayList<>();


  public Market toMarket() {
    return new Market()
      .setName(this.getName())
      .setDepartment(this.getDepartment())
      .setAddress(this.getDepartment())
      .setOpenTimes(
        this.getOpenTimes().stream().map(StoreOpenTimeRequestBody::toOpenTime).collect(Collectors.toList()))
      .setMarketCategories(this.getMarketCategories());
  }

  public String getName() {
    return name;
  }

  public StorePostRequestBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public StorePostRequestBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getAddress() {
    return address;
  }

  public StorePostRequestBody setAddress(String address) {
    this.address = address;
    return this;
  }

  public List<MarketCategory> getMarketCategories() {
    return marketCategories;
  }

  public StorePostRequestBody setMarketCategories(List<MarketCategory> marketCategories) {
    this.marketCategories = marketCategories;
    return this;
  }

  public List<StoreOpenTimeRequestBody> getOpenTimes() {
    return openTimes;
  }

  public StorePostRequestBody setOpenTimes(List<StoreOpenTimeRequestBody> openTimes) {
    this.openTimes = openTimes;
    return this;
  }
}
