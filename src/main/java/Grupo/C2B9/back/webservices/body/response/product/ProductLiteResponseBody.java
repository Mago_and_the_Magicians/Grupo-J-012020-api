package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.model.product.Product;

public class ProductLiteResponseBody {

  private String code;
  private String name;
  private String brand;

  public static ProductLiteResponseBody valueOf(Product product) {
    return new ProductLiteResponseBody()
      .setCode(product.getCode())
      .setName(product.getName())
      .setBrand(product.getBrand());
  }

  public String getCode() {
    return code;
  }

  public ProductLiteResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public String getName() {
    return name;
  }

  public ProductLiteResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getBrand() {
    return brand;
  }

  public ProductLiteResponseBody setBrand(String brand) {
    this.brand = brand;
    return this;
  }
}
