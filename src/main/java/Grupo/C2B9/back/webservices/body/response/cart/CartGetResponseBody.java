package Grupo.C2B9.back.webservices.body.response.cart;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.webservices.body.response.marketOrder.StoreCartResponseBody;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;


public class CartGetResponseBody {
  private Integer id;
  private String code;
  private OrderStatus state;
  private Integer userId;
  private double total;
  private double totalDiscount;
  private double TotalAmount;
  private List<StoreCartResponseBody> storeOrders;

  public static CartGetResponseBody valueOf(Order order) {
    return new CartGetResponseBody().setId(order.getId())
      .setUserId(order.getUser().getId())
      .setCode(order.getCode())
      .setState(order.getStatus())
      .setTotalAmount(BigDecimal.valueOf(
        order.getTotalAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue())
      .setTotal(BigDecimal.valueOf(
        order.getTotalAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue())
      .setTotalDiscount(0.00)
      .setStoreOrders(
        order.getMarketOrders()
          .stream()
          .map(StoreCartResponseBody::valueOf)
          .collect(Collectors.toList()));

  }

  public Integer getId() {
    return id;
  }

  public CartGetResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public Integer getUserId() {
    return userId;
  }

  public CartGetResponseBody setUserId(Integer userId) {
    this.userId = userId;
    return this;
  }

  public List<StoreCartResponseBody> getStoreOrders() {
    return storeOrders;
  }

  public CartGetResponseBody setStoreOrders(List<StoreCartResponseBody> storeOrders) {
    this.storeOrders = storeOrders;
    return this;
  }

  public double getTotalAmount() {
    return TotalAmount;
  }

  public CartGetResponseBody setTotalAmount(double totalAmount) {
    TotalAmount = totalAmount;
    return this;
  }

  public String getCode() {
    return code;
  }

  public CartGetResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public OrderStatus getState() {
    return state;
  }

  public CartGetResponseBody setState(OrderStatus state) {
    this.state = state;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public CartGetResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public CartGetResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }
}
