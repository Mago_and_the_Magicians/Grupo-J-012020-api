package Grupo.C2B9.back.webservices.body.request.checkout.shipping;

import Grupo.C2B9.back.model.order.Order;

import java.util.List;
import java.util.stream.Collectors;

public class CheckoutShippingRequestBody {

  private List<CheckoutShippingStoreOrderRequestBody> storeOrders;

  public Order toOrder() {
    return new Order()
      .setMarketOrders(storeOrders
        .stream()
        .map(CheckoutShippingStoreOrderRequestBody::toMarketOrder)
        .collect(Collectors.toList())
      );
  }


  public List<CheckoutShippingStoreOrderRequestBody> getStoreOrders() {
    return storeOrders;
  }

  public CheckoutShippingRequestBody setStoreOrders(List<CheckoutShippingStoreOrderRequestBody> storeOrders) {
    this.storeOrders = storeOrders;
    return this;
  }
}
