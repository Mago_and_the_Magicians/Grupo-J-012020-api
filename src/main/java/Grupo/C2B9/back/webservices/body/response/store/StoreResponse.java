package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.variant.MarketVariant;

import java.util.List;
import java.util.stream.Collectors;

public class StoreResponse {

  private List<StoreItemResponse> results;

  public static StoreResponse valueOf(List<MarketVariant> marketVariants) {
    return new StoreResponse().setResults(
      marketVariants
        .stream()
        .map(StoreItemResponse::valueOf)
        .collect(Collectors.toList()));
  }

  public List<StoreItemResponse> getResults() {
    return results;
  }

  public StoreResponse setResults(List<StoreItemResponse> results) {
    this.results = results;
    return this;
  }
}
