package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.variant.MarketVariant;

public class StoreItemResponse {

  private String code;
  private String productCode;
  private String brand;
  private String variantCode;
  private double price;
  private Integer stock;
  private String imgUrl;


  public static StoreItemResponse valueOf(MarketVariant marketVariant) {
    return new StoreItemResponse()
      .setBrand(marketVariant.getVariant().getProduct().getBrand())
      .setProductCode(marketVariant.getVariant().getProduct().getCode())
      .setVariantCode(marketVariant.getVariant().getCode())
      .setPrice(marketVariant.getPrice())
      .setStock(marketVariant.getStock())
      .setImgUrl(marketVariant.getVariant().getImageUrl());
  }


  public String getBrand() {
    return brand;
  }

  public StoreItemResponse setBrand(String brand) {
    this.brand = brand;
    return this;
  }

  public double getPrice() {
    return price;
  }

  public StoreItemResponse setPrice(double price) {
    this.price = price;
    return this;
  }

  public Integer getStock() {
    return stock;
  }

  public StoreItemResponse setStock(Integer stock) {
    this.stock = stock;
    return this;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public StoreItemResponse setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StoreItemResponse setCode(String code) {
    this.code = code;
    return this;
  }

  public String getProductCode() {
    return productCode;
  }

  public StoreItemResponse setProductCode(String productCode) {
    this.productCode = productCode;
    return this;
  }

  public String getVariantCode() {
    return variantCode;
  }

  public StoreItemResponse setVariantCode(String variantCode) {
    this.variantCode = variantCode;
    return this;
  }
}
