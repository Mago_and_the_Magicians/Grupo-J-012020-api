package Grupo.C2B9.back.webservices.body.response.cart;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.webservices.body.response.marketOrder.StoreCartResponseBody;

import java.util.List;
import java.util.stream.Collectors;


public class CartPostResponseBody {
  private String code;
  private OrderStatus state;
  private double total;
  private double totalDiscount;
  private double TotalAmount;
  private List<StoreCartResponseBody> storeOrders;

  public static CartPostResponseBody valueOf(Order order) {
    return new CartPostResponseBody()
      .setCode(order.getCode())
      .setState(order.getStatus())
      .setTotalAmount(order.getTotalAmount())
      .setTotal(order.getTotalAmount())
      .setTotalDiscount(0.00)
      .setStoreOrders(
        order.getMarketOrders()
          .stream()
          .map(StoreCartResponseBody::valueOf)
          .collect(Collectors.toList()));

  }

  public List<StoreCartResponseBody> getStoreOrders() {
    return storeOrders;
  }

  public CartPostResponseBody setStoreOrders(List<StoreCartResponseBody> storeOrders) {
    this.storeOrders = storeOrders;
    return this;
  }

  public double getTotalAmount() {
    return TotalAmount;
  }

  public CartPostResponseBody setTotalAmount(double totalAmount) {
    TotalAmount = totalAmount;
    return this;
  }

  public String getCode() {
    return code;
  }

  public CartPostResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public CartPostResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public CartPostResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }

  public OrderStatus getState() {
    return state;
  }

  public CartPostResponseBody setState(OrderStatus state) {
    this.state = state;
    return this;
  }
}
