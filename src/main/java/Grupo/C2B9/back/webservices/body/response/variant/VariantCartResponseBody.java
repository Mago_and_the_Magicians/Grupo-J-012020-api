package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.webservices.body.response.product.ProductCartResponseBody;

public class VariantCartResponseBody {
  private Integer id;
  private String code;
  private String description;
  private String imageUrl;
  private ProductCartResponseBody product;


  public static VariantCartResponseBody valueOf(Variant variant) {
    return new VariantCartResponseBody().setId(variant.getId())
      .setCode(variant.getCode())
      .setDescription(variant.getDescription())
      .setImageUrl(variant.getImageUrl())
      .setProduct(ProductCartResponseBody.valueOf(variant.getProduct()));
  }

  public Integer getId() {
    return id;
  }

  public VariantCartResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public VariantCartResponseBody setDescription(String description) {
    this.description = description;
    return this;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public VariantCartResponseBody setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  public String getCode() {
    return code;
  }

  public VariantCartResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public ProductCartResponseBody getProduct() {
    return product;
  }

  public VariantCartResponseBody setProduct(ProductCartResponseBody product) {
    this.product = product;
    return this;
  }
}
