package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.service.auxiliar.Filter;

import java.util.List;
import java.util.stream.Collectors;

public class ProductListResponseBody {

  private List<FilterResponseBody> filters;

  private List<ProductDetailResponseBody> results;

  public static ProductListResponseBody valueOf(List<Filter> filters, List<Product> products) {
    return new ProductListResponseBody().setResults(
      products.stream()
        .map(ProductDetailResponseBody::valueOf)
        .collect(Collectors.toList())
    ).setFilters(
      filters
        .stream()
        .map(FilterResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public List<ProductDetailResponseBody> getResults() {
    return results;
  }

  public ProductListResponseBody setResults(List<ProductDetailResponseBody> results) {
    this.results = results;
    return this;
  }

  public List<FilterResponseBody> getFilters() {
    return filters;
  }

  public ProductListResponseBody setFilters(List<FilterResponseBody> filters) {
    this.filters = filters;
    return this;
  }
}
