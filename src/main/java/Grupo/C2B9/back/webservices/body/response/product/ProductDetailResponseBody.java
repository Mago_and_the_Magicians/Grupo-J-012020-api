package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.webservices.body.response.variant.VariantDetailResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class ProductDetailResponseBody {
  private Integer id;
  private String code;
  private String name;
  private String brand;
  private List<VariantDetailResponseBody> variants;

  public static ProductDetailResponseBody valueOf(Product product) {
    return new ProductDetailResponseBody()
      .setCode(product.getCode())
      .setBrand(product.getBrand())
      .setId(product.getId())
      .setName(product.getName())
      .setVariants(product.getVariants()
        .stream()
        .map(VariantDetailResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  ;

  public List<VariantDetailResponseBody> getVariants() {
    return variants;
  }

  public ProductDetailResponseBody setVariants(List<VariantDetailResponseBody> variants) {
    this.variants = variants;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public ProductDetailResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public ProductDetailResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getBrand() {
    return brand;
  }

  public ProductDetailResponseBody setBrand(String brand) {
    this.brand = brand;
    return this;
  }

  public String getCode() {
    return code;
  }

  public ProductDetailResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
