package Grupo.C2B9.back.webservices.body.response.marketvariant;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.webservices.body.response.variant.VariantCartResponseBody;

public class StoreVariantResponseBody {

  private Integer id;
  private String code;
  private VariantCartResponseBody variant;

  public static StoreVariantResponseBody valueOf(MarketVariant marketVariant) {
    return new StoreVariantResponseBody()
      .setCode(marketVariant.getCode())
      .setId(marketVariant.getId())
      .setVariant(VariantCartResponseBody.valueOf(marketVariant.getVariant()));
  }

  public Integer getId() {
    return id;
  }

  public StoreVariantResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StoreVariantResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public VariantCartResponseBody getVariant() {
    return variant;
  }

  public StoreVariantResponseBody setVariant(VariantCartResponseBody variant) {
    this.variant = variant;
    return this;
  }
}
