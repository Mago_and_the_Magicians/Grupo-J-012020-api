package Grupo.C2B9.back.webservices.body.response.lineitem;

import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.webservices.body.response.marketvariant.StoreVariantResponseBody;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LineItemResponseBody {
  private String code;
  private Integer quantity;
  private double total;
  private double price;
  private LineItemStoreResponseBody store;
  private StoreVariantResponseBody storeVariant;


  public static LineItemResponseBody valueOf(LineItem lineItem) {
    return new LineItemResponseBody().setCode(lineItem.getMarketVariant().getCode())
      .setTotal(BigDecimal.valueOf(
        lineItem.getPrice()).setScale(2, RoundingMode.HALF_UP).doubleValue())
      .setPrice(BigDecimal.valueOf(
        lineItem.getMarketVariant().getPrice()).setScale(2, RoundingMode.HALF_UP).doubleValue())
      .setStoreVariant(StoreVariantResponseBody.valueOf(lineItem.getMarketVariant()))
      .setQuantity(lineItem.getQuantity())
      .setStore(LineItemStoreResponseBody.valueOf(lineItem.getMarketVariant().getMarket()));
  }

  public StoreVariantResponseBody getStoreVariant() {
    return storeVariant;
  }

  public LineItemResponseBody setStoreVariant(StoreVariantResponseBody storeVariant) {
    this.storeVariant = storeVariant;
    return this;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public LineItemResponseBody setQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public LineItemResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getPrice() {
    return price;
  }

  public LineItemResponseBody setPrice(double price) {
    this.price = price;
    return this;
  }

  public LineItemStoreResponseBody getStore() {
    return store;
  }

  public LineItemResponseBody setStore(LineItemStoreResponseBody store) {
    this.store = store;
    return this;
  }

  public String getCode() {
    return code;
  }

  public LineItemResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
