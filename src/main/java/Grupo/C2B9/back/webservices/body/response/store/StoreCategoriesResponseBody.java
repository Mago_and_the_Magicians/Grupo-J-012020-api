package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.MarketCategory;

import java.util.List;

public class StoreCategoriesResponseBody {

  private List<MarketCategory> results;

  public List<MarketCategory> getResults() {
    return results;
  }

  public StoreCategoriesResponseBody setResults(List<MarketCategory> results) {
    this.results = results;
    return this;
  }
}
