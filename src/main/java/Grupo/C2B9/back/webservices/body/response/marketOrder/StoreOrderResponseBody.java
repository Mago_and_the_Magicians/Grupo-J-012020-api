package Grupo.C2B9.back.webservices.body.response.marketOrder;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.webservices.body.response.lineitem.LineItemResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class StoreOrderResponseBody {

  private String code;
  private double totalAmount;
  private double total;
  private double totalDiscount;
  private List<LineItemResponseBody> lineItems;


  public static StoreOrderResponseBody valueOf(MarketOrder marketOrder) {
    return new StoreOrderResponseBody()
      .setCode(marketOrder.getCode())
      .setTotalAmount(marketOrder.getTotalAmount())
      .setTotalDiscount(0.00)
      .setTotal(marketOrder.getTotalAmount())
      .setLineItems(marketOrder.getLineItems()
        .stream()
        .map(LineItemResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public static StoreOrderResponseBody valueOf(MarketOrder marketOrder, String code) {
    return new StoreOrderResponseBody()
      .setCode(marketOrder.getCode())
      .setTotalAmount(marketOrder.getTotalAmount())
      .setTotalDiscount(0.00)
      .setTotal(marketOrder.getTotalAmount())
      .setLineItems(marketOrder.getLineItems()
        .stream()
        .filter(lineItem -> lineItem.getMarketVariant().getCode().equals(code))
        .map(LineItemResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public StoreOrderResponseBody setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  public List<LineItemResponseBody> getLineItems() {
    return lineItems;
  }

  public StoreOrderResponseBody setLineItems(List<LineItemResponseBody> lineItems) {
    this.lineItems = lineItems;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StoreOrderResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public StoreOrderResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public StoreOrderResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }
}
