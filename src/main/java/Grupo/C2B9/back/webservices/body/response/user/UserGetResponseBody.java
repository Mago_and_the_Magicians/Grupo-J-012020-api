package Grupo.C2B9.back.webservices.body.response.user;

import Grupo.C2B9.back.model.user.User;

public class UserGetResponseBody {

  private String fullName;

  private String username;

  private String email;


  public static UserGetResponseBody valueOf(User user) {
    return new UserGetResponseBody()
      .setEmail(user.getEmail())
      .setFullName(user.getFullname())
      .setUsername(user.getUsername());
  }

  public String getFullName() {
    return fullName;
  }

  public UserGetResponseBody setFullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  public String getUsername() {
    return username;
  }

  public UserGetResponseBody setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public UserGetResponseBody setEmail(String email) {
    this.email = email;
    return this;
  }
}
