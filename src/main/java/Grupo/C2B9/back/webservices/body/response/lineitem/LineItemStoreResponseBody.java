package Grupo.C2B9.back.webservices.body.response.lineitem;

import Grupo.C2B9.back.model.market.Market;

public class LineItemStoreResponseBody {
  private Integer id;
  private String code;
  private String name;

  public static LineItemStoreResponseBody valueOf(Market market) {
    return new LineItemStoreResponseBody()
      .setCode(market.getCode())
      .setId(market.getId())
      .setName(market.getName());
  }

  public Integer getId() {
    return id;
  }

  public LineItemStoreResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public LineItemStoreResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public String getName() {
    return name;
  }

  public LineItemStoreResponseBody setName(String name) {
    this.name = name;
    return this;
  }
}
