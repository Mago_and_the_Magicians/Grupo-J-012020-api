package Grupo.C2B9.back.webservices.body.response.login;

import java.util.List;

public class JwtResponse {
  private String access_token;
  private String type = "Bearer";
  private Integer id;
  private String username;
  private String email;
  private String userType;
  private List<String> roles;


  public JwtResponse(String accessToken, Integer id, String username, String email, List<String> roles,
                     String userType) {
    this.access_token = accessToken;
    this.type = "Bearer";
    this.id = id;
    this.username = username;
    this.email = email;
    this.userType = userType;
    this.roles = roles;
  }

  public Integer getId() {
    return id;
  }

  public JwtResponse setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getUsername() {
    return username;
  }

  public JwtResponse setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public JwtResponse setEmail(String email) {
    this.email = email;
    return this;
  }

  public List<String> getRoles() {
    return roles;
  }

  public JwtResponse setRoles(List<String> roles) {
    this.roles = roles;
    return this;
  }

  public String getType() {
    return type;
  }

  public JwtResponse setType(String type) {
    this.type = type;
    return this;
  }

  public String getUserType() {
    return userType;
  }

  public JwtResponse setUserType(String userType) {
    this.userType = userType;
    return this;
  }

  public String getAccess_token() {
    return access_token;
  }

  public JwtResponse setAccess_token(String access_token) {
    this.access_token = access_token;
    return this;
  }
}
