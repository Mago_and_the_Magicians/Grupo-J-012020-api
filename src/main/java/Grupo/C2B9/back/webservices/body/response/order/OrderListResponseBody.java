package Grupo.C2B9.back.webservices.body.response.order;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.webservices.body.response.cart.CartGetResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class OrderListResponseBody {

  private List<CartGetResponseBody> results;


  public static OrderListResponseBody valueOf(List<Order> orders) {
    return new OrderListResponseBody().setResults(orders
      .stream()
      .map(CartGetResponseBody::valueOf)
      .collect(Collectors.toList())
    );
  }

  public List<CartGetResponseBody> getResults() {
    return results;
  }

  public OrderListResponseBody setResults(List<CartGetResponseBody> results) {
    this.results = results;
    return this;
  }
}
