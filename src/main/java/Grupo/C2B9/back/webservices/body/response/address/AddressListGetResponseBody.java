package Grupo.C2B9.back.webservices.body.response.address;

import Grupo.C2B9.back.model.user.Address;

import java.util.List;
import java.util.stream.Collectors;

public class AddressListGetResponseBody {

  private List<AddressGetResponseBody> results;

  public List<AddressGetResponseBody> getResults() {
    return results;
  }

  public static AddressListGetResponseBody valueOf(List<Address> addresses) {
    return new AddressListGetResponseBody().setResults(
      addresses
        .stream()
        .map(AddressGetResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public AddressListGetResponseBody setResults(List<AddressGetResponseBody> results) {
    this.results = results;
    return this;
  }
}
