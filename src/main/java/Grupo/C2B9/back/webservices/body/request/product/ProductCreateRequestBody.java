package Grupo.C2B9.back.webservices.body.request.product;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.ProductCategory;

public class ProductCreateRequestBody {
  private String name;
  private String brand;
  private ProductCategory category;


  public Product toProduct() {
    return new Product()
      .setBrand(this.getBrand())
      .setName(this.getName())
      .setProductCategory(this.getCategory());
  }

  public String getName() {
    return name;
  }

  public ProductCreateRequestBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getBrand() {
    return brand;
  }

  public ProductCreateRequestBody setBrand(String brand) {
    this.brand = brand;
    return this;
  }

  public ProductCategory getCategory() {
    return category;
  }

  public ProductCreateRequestBody setCategory(ProductCategory category) {
    this.category = category;
    return this;
  }
}
