package Grupo.C2B9.back.webservices.body.response.address;

import Grupo.C2B9.back.model.user.Address;

public class AddressGetResponseBody {

  private Integer id;
  private String code;
  private String street;
  private Integer number;
  private Integer floor;
  private String department;
  private String state;
  private String town;

  public static AddressGetResponseBody valueOf(Address address) {
    return new AddressGetResponseBody()
      .setId(address.getId())
      .setCode(address.getCode())
      .setStreet(address.getStreet())
      .setDepartment(address.getDepartment())
      .setFloor(address.getFloor())
      .setNumber(address.getNumber())
      .setState(address.getProvince())
      .setTown(address.getTown());
  }


  public String getStreet() {
    return street;
  }

  public AddressGetResponseBody setStreet(String street) {
    this.street = street;
    return this;
  }

  public Integer getNumber() {
    return number;
  }

  public AddressGetResponseBody setNumber(Integer number) {
    this.number = number;
    return this;
  }

  public Integer getFloor() {
    return floor;
  }

  public AddressGetResponseBody setFloor(Integer floor) {
    this.floor = floor;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public AddressGetResponseBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getState() {
    return state;
  }

  public AddressGetResponseBody setState(String state) {
    this.state = state;
    return this;
  }

  public String getTown() {
    return town;
  }

  public AddressGetResponseBody setTown(String town) {
    this.town = town;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public AddressGetResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public AddressGetResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
