package Grupo.C2B9.back.webservices.body.request.marketOrder;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.webservices.body.request.store.LineItemRequestBody;

import java.util.List;
import java.util.stream.Collectors;

public class StoreOrderRequestBody {

  private List<LineItemRequestBody> lineItems;


  public MarketOrder toMarketOrder() {
    return new MarketOrder()
      .setLineItems(this.getLineItems()
        .stream()
        .map(LineItemRequestBody::toLineItem)
        .collect(Collectors.toList()));
  }

  public List<LineItemRequestBody> getLineItems() {
    return lineItems;
  }

  public StoreOrderRequestBody setLineItems(List<LineItemRequestBody> lineItems) {
    this.lineItems = lineItems;
    return this;
  }
}
