package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.ProductCategory;

public class ProductCartResponseBody {

  private Integer id;
  private String code;
  private String name;
  private String brand;
  private ProductCategory category;

  public static ProductCartResponseBody valueOf(Product product) {
    return new ProductCartResponseBody().setId(product.getId())
      .setBrand(product.getBrand())
      .setCode(product.getCode())
      .setName(product.getName())
      .setCategory(product.getProductCategory());
  }

  public Integer getId() {
    return id;
  }

  public ProductCartResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public ProductCartResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public String getName() {
    return name;
  }

  public ProductCartResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getBrand() {
    return brand;
  }

  public ProductCartResponseBody setBrand(String brand) {
    this.brand = brand;
    return this;
  }

  public ProductCategory getCategory() {
    return category;
  }

  public ProductCartResponseBody setCategory(ProductCategory category) {
    this.category = category;
    return this;
  }
}
