package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.Market;

import java.util.List;
import java.util.stream.Collectors;

public class StoreGetListResponseBody {

  private List<StoreGetResponseBody> results;


  public static StoreGetListResponseBody valueOf(List<Market> markets) {
    return new StoreGetListResponseBody().setResults(
      markets
        .stream()
        .map(StoreGetResponseBody::valueOf)
        .collect(Collectors.toList())
    );
  }

  public List<StoreGetResponseBody> getResults() {
    return results;
  }

  public StoreGetListResponseBody setResults(List<StoreGetResponseBody> results) {
    this.results = results;
    return this;
  }
}
