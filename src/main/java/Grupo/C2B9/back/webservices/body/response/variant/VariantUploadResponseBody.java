package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.webservices.body.response.product.ProductDetailResponseBody;

public class VariantUploadResponseBody {

  private Integer id;
  private String code;
  private String description;
  private String imageUrl;
  private ProductDetailResponseBody product;

  public static VariantUploadResponseBody valueOf(Variant variant) {
    return new VariantUploadResponseBody()
      .setId(variant.getId())
      .setCode(variant.getCode())
      .setDescription(variant.getDescription())
      .setProduct(ProductDetailResponseBody.valueOf(variant.getProduct()))
      .setImageUrl(variant.getImageUrl());
  }

  public Integer getId() {
    return id;
  }

  public VariantUploadResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public VariantUploadResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public VariantUploadResponseBody setDescription(String description) {
    this.description = description;
    return this;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public VariantUploadResponseBody setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  public ProductDetailResponseBody getProduct() {
    return product;
  }

  public VariantUploadResponseBody setProduct(ProductDetailResponseBody product) {
    this.product = product;
    return this;
  }
}
