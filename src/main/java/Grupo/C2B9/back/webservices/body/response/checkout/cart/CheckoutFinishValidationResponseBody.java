package Grupo.C2B9.back.webservices.body.response.checkout.cart;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.webservices.body.response.checkout.marketOrder.CheckoutShippingStoreOrderResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class CheckoutFinishValidationResponseBody {

  private String code;
  private OrderStatus state;
  private double totalAmount;
  private double total;
  private double totalDiscount;
  private List<CheckoutShippingStoreOrderResponseBody> storeOrders;

  public static CheckoutFinishValidationResponseBody valueOf(Order order) {
    return new CheckoutFinishValidationResponseBody()
      .setCode(order.getCode())
      .setState(order.getStatus())
      .setTotal(order.getTotalAmount())
      .setTotalAmount(order.getTotalAmount())
      .setTotalDiscount(0.00)
      .setStoreOrders(order.getMarketOrders()
        .stream()
        .map(CheckoutShippingStoreOrderResponseBody::valueOf)
        .collect(Collectors.toList())
      );
  }


  public String getCode() {
    return code;
  }

  public CheckoutFinishValidationResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public List<CheckoutShippingStoreOrderResponseBody> getStoreOrders() {
    return storeOrders;
  }

  public CheckoutFinishValidationResponseBody setStoreOrders(List<CheckoutShippingStoreOrderResponseBody> storeOrders) {
    this.storeOrders = storeOrders;
    return this;
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public CheckoutFinishValidationResponseBody setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public CheckoutFinishValidationResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public CheckoutFinishValidationResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }

  public OrderStatus getState() {
    return state;
  }

  public CheckoutFinishValidationResponseBody setState(OrderStatus state) {
    this.state = state;
    return this;
  }
}
