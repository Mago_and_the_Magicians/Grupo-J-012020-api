package Grupo.C2B9.back.webservices.body.response.marketvariant;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.webservices.body.response.store.StoreGetResponseBody;

public class MarketVariantResponseBody {

  private Integer id;
  private StoreGetResponseBody store;
  private String code;
  private double price;
  private Integer stock;

  public static MarketVariantResponseBody valueOf(MarketVariant marketVariant) {
    return new MarketVariantResponseBody()
      .setId(marketVariant.getId())
      .setCode(marketVariant.getCode())
      .setStore(StoreGetResponseBody.valueOf(marketVariant.getMarket()))
      .setPrice(marketVariant.getPrice())
      .setStock(marketVariant.getStock());
  }

  public String getCode() {
    return code;
  }

  public MarketVariantResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public double getPrice() {
    return price;
  }

  public MarketVariantResponseBody setPrice(double price) {
    this.price = price;
    return this;
  }

  public Integer getStock() {
    return stock;
  }

  public MarketVariantResponseBody setStock(Integer stock) {
    this.stock = stock;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public MarketVariantResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public StoreGetResponseBody getStore() {
    return store;
  }

  public MarketVariantResponseBody setStore(StoreGetResponseBody store) {
    this.store = store;
    return this;
  }
}
