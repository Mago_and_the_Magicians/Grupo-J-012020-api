package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;

import java.util.List;
import java.util.stream.Collectors;

public class StorePostResponseBody {

  private Integer id;
  private String code;
  private String name;
  private String department;
  private String address;
  private List<MarketCategory> categories;
  private List<OpenTimeResponseBody> openTimes;

  public static StorePostResponseBody valueOf(Market market) {
    return new StorePostResponseBody()
      .setCode(market.getCode())
      .setId(market.getId())
      .setAddress(market.getAddress())
      .setDepartment(market.getDepartment())
      .setName(market.getName())
      .setCategories(market.getMarketCategories())
      .setOpenTimes(market.getOpenTimes().stream().map(OpenTimeResponseBody::valueOf).collect(Collectors.toList()));
  }

  public Integer getId() {
    return id;
  }

  public StorePostResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public StorePostResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public StorePostResponseBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getAddress() {
    return address;
  }

  public StorePostResponseBody setAddress(String address) {
    this.address = address;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StorePostResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public List<MarketCategory> getCategories() {
    return categories;
  }

  public StorePostResponseBody setCategories(List<MarketCategory> categories) {
    this.categories = categories;
    return this;
  }

  public List<OpenTimeResponseBody> getOpenTimes() {
    return openTimes;
  }

  public StorePostResponseBody setOpenTimes(List<OpenTimeResponseBody> openTimes) {
    this.openTimes = openTimes;
    return this;
  }
}
