package Grupo.C2B9.back.webservices.body.response.marketvariant;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.webservices.body.response.store.StorePostResponseBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantCartResponseBody;

public class MarketVariantPostResponseBody {

  private Integer id;
  private String code;
  private double unitPrice;
  private Integer stock;
  private StorePostResponseBody market;
  private VariantCartResponseBody variant;

  public static MarketVariantPostResponseBody valueOf(MarketVariant marketVariant) {
    return new MarketVariantPostResponseBody()
      .setId(marketVariant.getId())
      .setCode(marketVariant.getCode())
      .setUnitPrice(marketVariant.getPrice())
      .setStock(marketVariant.getStock())
      .setVariant(VariantCartResponseBody.valueOf(marketVariant.getVariant()))
      .setMarket(StorePostResponseBody.valueOf(marketVariant.getMarket()));
  }


  public Integer getStock() {
    return stock;
  }

  public MarketVariantPostResponseBody setStock(Integer stock) {
    this.stock = stock;
    return this;
  }

  public StorePostResponseBody getMarket() {
    return market;
  }

  public MarketVariantPostResponseBody setMarket(StorePostResponseBody market) {
    this.market = market;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public MarketVariantPostResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public MarketVariantPostResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public MarketVariantPostResponseBody setUnitPrice(double unitPrice) {
    this.unitPrice = unitPrice;
    return this;
  }

  public VariantCartResponseBody getVariant() {
    return variant;
  }

  public MarketVariantPostResponseBody setVariant(VariantCartResponseBody variant) {
    this.variant = variant;
    return this;
  }
}
