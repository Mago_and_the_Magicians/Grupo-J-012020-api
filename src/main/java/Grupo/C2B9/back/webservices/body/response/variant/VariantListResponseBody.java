package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;

import java.util.List;
import java.util.stream.Collectors;

public class VariantListResponseBody {

  public List<VariantDetailResponseBody> results;

  public static VariantListResponseBody valueOf(List<Variant> variants) {
    return new VariantListResponseBody().setResults(
      variants.stream()
        .map(VariantDetailResponseBody::valueOf)
        .collect(Collectors.toList())
    );
  }

  public List<VariantDetailResponseBody> getResults() {
    return results;
  }

  public VariantListResponseBody setResults(List<VariantDetailResponseBody> results) {
    this.results = results;
    return this;
  }
}
