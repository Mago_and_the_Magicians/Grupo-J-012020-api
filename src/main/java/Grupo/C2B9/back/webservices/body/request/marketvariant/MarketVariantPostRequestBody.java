package Grupo.C2B9.back.webservices.body.request.marketvariant;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.product.Variant;

public class MarketVariantPostRequestBody {

  private String variantCode;

  private String marketCode;

  private Integer stock;

  private Double price;

  public MarketVariant toMarketVariant() {
    return new MarketVariant()
      .setStock(this.getStock())
      .setVariant(new Variant().setCode(variantCode))
      .setMarket(new Market().setCode(marketCode))
      .setStock(this.getStock());
  }

  public Integer getStock() {
    return stock;
  }

  public MarketVariantPostRequestBody setStock(Integer stock) {
    this.stock = stock;
    return this;
  }

  public Double getPrice() {
    return price;
  }

  public MarketVariantPostRequestBody setPrice(Double price) {
    this.price = price;
    return this;
  }

  public String getVariantCode() {
    return variantCode;
  }

  public MarketVariantPostRequestBody setVariantCode(String variantCode) {
    this.variantCode = variantCode;
    return this;
  }

  public String getMarketCode() {
    return marketCode;
  }

  public MarketVariantPostRequestBody setMarketCode(String marketCode) {
    this.marketCode = marketCode;
    return this;
  }
}
