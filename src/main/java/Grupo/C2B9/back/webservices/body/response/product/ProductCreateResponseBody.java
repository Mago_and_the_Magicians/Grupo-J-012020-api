package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.model.product.Product;

public class ProductCreateResponseBody {
  private Integer id;
  private String code;
  private String name;
  private String brand;

  public static ProductCreateResponseBody valueOf(Product product) {
    return new ProductCreateResponseBody()
      .setCode(product.getCode())
      .setBrand(product.getBrand())
      .setId(product.getId())
      .setName(product.getName());
  }


  public Integer getId() {
    return id;
  }

  public ProductCreateResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public ProductCreateResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public String getBrand() {
    return brand;
  }

  public ProductCreateResponseBody setBrand(String brand) {
    this.brand = brand;
    return this;
  }

  public String getCode() {
    return code;
  }

  public ProductCreateResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
