package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;

import java.util.List;
import java.util.stream.Collectors;

public class StoreGetResponseBody {

  private Integer id;
  private String code;
  private String name;
  private String department;
  private String address;
  private List<MarketCategory> categories;
  private List<OpenTimeResponseBody> openTimes;

  public static StoreGetResponseBody valueOf(Market market) {
    return new StoreGetResponseBody()
      .setCode(market.getCode())
      .setId(market.getId())
      .setAddress(market.getAddress())
      .setDepartment(market.getDepartment())
      .setName(market.getName())
      .setCategories(market.getMarketCategories())
      .setOpenTimes(market.getOpenTimes().stream().map(OpenTimeResponseBody::valueOf).collect(Collectors.toList()));
  }

  public Integer getId() {
    return id;
  }

  public StoreGetResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public StoreGetResponseBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getAddress() {
    return address;
  }

  public StoreGetResponseBody setAddress(String address) {
    this.address = address;
    return this;
  }

  public String getName() {
    return name;
  }

  public StoreGetResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public List<OpenTimeResponseBody> getOpenTimes() {
    return openTimes;
  }

  public StoreGetResponseBody setOpenTimes(List<OpenTimeResponseBody> openTimes) {
    this.openTimes = openTimes;
    return this;
  }

  public List<MarketCategory> getCategories() {
    return categories;
  }

  public StoreGetResponseBody setCategories(List<MarketCategory> categories) {
    this.categories = categories;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StoreGetResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
