package Grupo.C2B9.back.webservices.body.response.marketvariant;

import Grupo.C2B9.back.model.market.variant.MarketVariant;

import java.util.List;
import java.util.stream.Collectors;

public class MarketVariantListResponseBody {

  private List<MarketVariantPostResponseBody> results;

  public static MarketVariantListResponseBody valueOf(List<MarketVariant> marketVariants) {
    return new MarketVariantListResponseBody().setResults(
      marketVariants.stream().map(MarketVariantPostResponseBody::valueOf).collect(Collectors.toList()));

  }

  public List<MarketVariantPostResponseBody> getResults() {
    return results;
  }

  public MarketVariantListResponseBody setResults(List<MarketVariantPostResponseBody> results) {
    this.results = results;
    return this;
  }
}
