package Grupo.C2B9.back.webservices.body.request.cart;

import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.webservices.body.request.store.LineItemRequestBody;

import java.util.List;
import java.util.stream.Collectors;


public class CartPutRequestBody {

  private List<LineItemRequestBody> lineItems;

  public List<LineItem> toLineItems() {
    return lineItems.stream().map(LineItemRequestBody::toLineItem).collect(Collectors.toList());
  }

  public List<LineItemRequestBody> getLineItems() {
    return lineItems;
  }

  public CartPutRequestBody setLineItems(List<LineItemRequestBody> lineItems) {
    this.lineItems = lineItems;
    return this;
  }
}
