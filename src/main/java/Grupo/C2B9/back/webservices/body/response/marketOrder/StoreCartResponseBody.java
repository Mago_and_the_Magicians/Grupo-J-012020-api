package Grupo.C2B9.back.webservices.body.response.marketOrder;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.webservices.body.response.lineitem.LineItemResponseBody;
import Grupo.C2B9.back.webservices.body.response.store.StoreGetResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class StoreCartResponseBody {

  private String code;
  private double totalAmount;
  private double total;
  private double totalDiscount;
  private StoreGetResponseBody store;
  private List<LineItemResponseBody> lineItems;


  public static StoreCartResponseBody valueOf(MarketOrder marketOrder) {
    return new StoreCartResponseBody()
      .setCode(marketOrder.getCode())
      .setStore(StoreGetResponseBody.valueOf(marketOrder.getMarket()))
      .setTotalAmount(marketOrder.getTotalAmount())
      .setTotalDiscount(0.00)
      .setTotal(marketOrder.getTotalAmount())
      .setLineItems(marketOrder.getLineItems()
        .stream()
        .map(LineItemResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public static StoreCartResponseBody valueOf(MarketOrder marketOrder, String code) {
    return new StoreCartResponseBody()
      .setCode(marketOrder.getCode())
      .setStore(StoreGetResponseBody.valueOf(marketOrder.getMarket()))
      .setTotalAmount(marketOrder.getTotalAmount())
      .setTotalDiscount(0.00)
      .setTotal(marketOrder.getTotalAmount())
      .setLineItems(marketOrder.getLineItems()
        .stream()
        .filter(lineItem -> lineItem.getMarketVariant().getCode().equals(code))
        .map(LineItemResponseBody::valueOf)
        .collect(Collectors.toList()));
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public StoreCartResponseBody setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  public StoreGetResponseBody getStore() {
    return store;
  }

  public StoreCartResponseBody setStore(StoreGetResponseBody store) {
    this.store = store;
    return this;
  }

  public List<LineItemResponseBody> getLineItems() {
    return lineItems;
  }

  public StoreCartResponseBody setLineItems(List<LineItemResponseBody> lineItems) {
    this.lineItems = lineItems;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StoreCartResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public StoreCartResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public StoreCartResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }
}
