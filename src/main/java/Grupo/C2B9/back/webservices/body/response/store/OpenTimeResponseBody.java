package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.opentime.OpenTime;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

public class OpenTimeResponseBody {

  private Integer id;
  @JsonFormat(pattern = "HH:mm")
  private LocalTime start;
  @JsonFormat(pattern = "HH:mm")
  private LocalTime end;
  private List<DayOfWeek> days;

  public static OpenTimeResponseBody valueOf(OpenTime openTime) {
    return new OpenTimeResponseBody()
      .setStart(openTime.getStart())
      .setEnd(openTime.getEnd())
      .setId(openTime.getId())
      .setDays(openTime.getWorkDays());
  }

  public LocalTime getStart() {
    return start;
  }

  public OpenTimeResponseBody setStart(LocalTime start) {
    this.start = start;
    return this;
  }

  public LocalTime getEnd() {
    return end;
  }

  public OpenTimeResponseBody setEnd(LocalTime end) {
    this.end = end;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public OpenTimeResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public List<DayOfWeek> getDays() {
    return days;
  }

  public OpenTimeResponseBody setDays(List<DayOfWeek> days) {
    this.days = days;
    return this;
  }
}
