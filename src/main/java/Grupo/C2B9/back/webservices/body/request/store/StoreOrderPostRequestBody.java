package Grupo.C2B9.back.webservices.body.request.store;

public class StoreOrderPostRequestBody extends LineItemRequestBody {


  @Override
  public StoreOrderPostRequestBody setCode(String code) {
    this.code = code;
    return this;
  }

  @Override
  public StoreOrderPostRequestBody setQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }
}
