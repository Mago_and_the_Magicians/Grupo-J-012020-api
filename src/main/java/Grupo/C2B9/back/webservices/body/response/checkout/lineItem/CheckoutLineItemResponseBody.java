package Grupo.C2B9.back.webservices.body.response.checkout.lineItem;

import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.webservices.body.response.checkout.marketVariant.CheckoutStoreVariantResponseBody;

public class CheckoutLineItemResponseBody {
  private Integer id;
  private CheckoutStoreVariantResponseBody StoreItem;
  private Integer quantity;
  private Integer validatedQuantity;
  private Integer nonValidQuantity;
  private double price;


  public static CheckoutLineItemResponseBody valueOf(LineItem lineItem) {
    return new CheckoutLineItemResponseBody().setId(lineItem.getId())
      .setPrice(lineItem.getPrice())
      .setValidatedQuantity(lineItem.getDiscountedQuantity())
      .setNonValidQuantity(lineItem.getUnsustractedStock())
      .setStoreItem(CheckoutStoreVariantResponseBody.valueOf(lineItem.getMarketVariant()))
      .setQuantity(lineItem.getQuantity());
  }

  public Integer getId() {
    return id;
  }

  public CheckoutLineItemResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public CheckoutStoreVariantResponseBody getStoreItem() {
    return StoreItem;
  }

  public CheckoutLineItemResponseBody setStoreItem(CheckoutStoreVariantResponseBody storeItem) {
    this.StoreItem = storeItem;
    return this;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public CheckoutLineItemResponseBody setQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public double getPrice() {
    return price;
  }

  public CheckoutLineItemResponseBody setPrice(double price) {
    this.price = price;
    return this;
  }

  public Integer getValidatedQuantity() {
    return validatedQuantity;
  }

  public CheckoutLineItemResponseBody setValidatedQuantity(Integer validatedQuantity) {
    this.validatedQuantity = validatedQuantity;
    return this;
  }

  public Integer getNonValidQuantity() {
    return nonValidQuantity;
  }

  public CheckoutLineItemResponseBody setNonValidQuantity(Integer nonValidQuantity) {
    this.nonValidQuantity = nonValidQuantity;
    return this;
  }
}
