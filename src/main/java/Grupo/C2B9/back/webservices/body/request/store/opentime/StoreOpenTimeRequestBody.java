package Grupo.C2B9.back.webservices.body.request.store.opentime;

import Grupo.C2B9.back.model.market.opentime.OpenTime;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

public class StoreOpenTimeRequestBody {

  private LocalTime start;
  private LocalTime end;
  private List<DayOfWeek> workDays;

  public OpenTime toOpenTime() {
    return new OpenTime()
      .setStart(this.getStart())
      .setEnd(this.getEnd())
      .setWorkDays(this.getWorkDays());
  }

  public LocalTime getStart() {
    return start;
  }

  public StoreOpenTimeRequestBody setStart(LocalTime start) {
    this.start = start;
    return this;
  }

  public LocalTime getEnd() {
    return end;
  }

  public StoreOpenTimeRequestBody setEnd(LocalTime end) {
    this.end = end;
    return this;
  }

  public List<DayOfWeek> getWorkDays() {
    return workDays;
  }

  public StoreOpenTimeRequestBody setWorkDays(List<DayOfWeek> workDays) {
    this.workDays = workDays;
    return this;
  }
}
