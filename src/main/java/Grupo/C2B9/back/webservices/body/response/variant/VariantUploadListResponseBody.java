package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;

import java.util.List;
import java.util.stream.Collectors;

public class VariantUploadListResponseBody {

  private List<VariantUploadResponseBody> results;

  public static VariantUploadListResponseBody valueOf(List<Variant> uploadVariants) {
    return new VariantUploadListResponseBody()
      .setResults(uploadVariants.stream().map(VariantUploadResponseBody::valueOf).collect(Collectors.toList()));
  }

  public List<VariantUploadResponseBody> getResults() {
    return results;
  }

  public VariantUploadListResponseBody setResults(List<VariantUploadResponseBody> results) {
    this.results = results;
    return this;
  }
}
