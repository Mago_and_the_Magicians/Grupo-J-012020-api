package Grupo.C2B9.back.webservices.body.request.login;

import Grupo.C2B9.back.service.auxiliar.LoginData;

public class LoginRequest {

  private String username;

  private String password;

  public LoginData toLoginData() {
    return new LoginData().setUsername(this.getUsername()).setPassword(this.getPassword());
  }

  public String getUsername() {
    return username;
  }

  public LoginRequest setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public LoginRequest setPassword(String password) {
    this.password = password;
    return this;
  }
}
