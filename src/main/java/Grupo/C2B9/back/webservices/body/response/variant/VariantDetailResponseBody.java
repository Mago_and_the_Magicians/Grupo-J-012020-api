package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.webservices.body.response.marketvariant.MarketVariantResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class VariantDetailResponseBody {
  private Integer id;
  private String code;
  private String description;
  private List<MarketVariantResponseBody> storeVariants;
  private String imageUrl;

  public static VariantDetailResponseBody valueOf(Variant variant) {
    return new VariantDetailResponseBody()
      .setId(variant.getId())
      .setCode(variant.getCode())
      .setDescription(variant.getDescription())
      .setStoreVariants(variant.getMarketVariants()
        .stream()
        .map(MarketVariantResponseBody::valueOf)
        .collect(Collectors.toList()))
      .setImageUrl(variant.getImageUrl());
  }

  public Integer getId() {
    return id;
  }

  public VariantDetailResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public VariantDetailResponseBody setDescription(String description) {
    this.description = description;
    return this;
  }

  public List<MarketVariantResponseBody> getStoreVariants() {
    return storeVariants;
  }

  public VariantDetailResponseBody setStoreVariants(List<MarketVariantResponseBody> storeVariants) {
    this.storeVariants = storeVariants;
    return this;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public VariantDetailResponseBody setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  public String getCode() {
    return code;
  }

  public VariantDetailResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
