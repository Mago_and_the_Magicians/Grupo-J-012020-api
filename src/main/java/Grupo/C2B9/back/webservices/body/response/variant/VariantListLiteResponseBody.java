package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;

import java.util.List;
import java.util.stream.Collectors;

public class VariantListLiteResponseBody {

  private List<VariantLiteResponseBody> results;

  public static VariantListLiteResponseBody valueOf(List<Variant> variants) {
    return new VariantListLiteResponseBody().setResults(
      variants
        .stream()
        .map(VariantLiteResponseBody::valueOf)
        .collect(Collectors.toList())
    );
  }

  public List<VariantLiteResponseBody> getResults() {
    return results;
  }

  public VariantListLiteResponseBody setResults(List<VariantLiteResponseBody> results) {
    this.results = results;
    return this;
  }
}
