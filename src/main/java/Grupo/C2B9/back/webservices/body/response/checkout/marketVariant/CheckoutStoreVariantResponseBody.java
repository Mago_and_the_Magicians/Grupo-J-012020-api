package Grupo.C2B9.back.webservices.body.response.checkout.marketVariant;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.webservices.body.response.variant.VariantCartResponseBody;

public class CheckoutStoreVariantResponseBody {

  private Integer id;
  private String code;
  private VariantCartResponseBody variant;
  private Integer stock;
  private double unitPrice;

  public static CheckoutStoreVariantResponseBody valueOf(MarketVariant marketVariant) {
    return new CheckoutStoreVariantResponseBody()
      .setCode(marketVariant.getCode())
      .setStock(marketVariant.getStock())
      .setId(marketVariant.getId())
      .setVariant(VariantCartResponseBody.valueOf(marketVariant.getVariant()))
      .setUnitPrice(marketVariant.getPrice());
  }

  public Integer getId() {
    return id;
  }

  public CheckoutStoreVariantResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public CheckoutStoreVariantResponseBody setUnitPrice(double unitPrice) {
    this.unitPrice = unitPrice;
    return this;
  }

  public String getCode() {
    return code;
  }

  public CheckoutStoreVariantResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public Integer getStock() {
    return stock;
  }

  public CheckoutStoreVariantResponseBody setStock(Integer stock) {
    this.stock = stock;
    return this;
  }

  public VariantCartResponseBody getVariant() {
    return variant;
  }

  public CheckoutStoreVariantResponseBody setVariant(VariantCartResponseBody variant) {
    this.variant = variant;
    return this;
  }
}
