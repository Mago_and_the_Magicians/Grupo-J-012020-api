package Grupo.C2B9.back.webservices.body.request.oauth;

public class FacebookOAuthRequestBody {

  private String clientId;
  private String code;
  private String redirectUri;

  public String getClientId() {
    return clientId;
  }

  public FacebookOAuthRequestBody setClientId(String clientId) {
    this.clientId = clientId;
    return this;
  }

  public String getCode() {
    return code;
  }

  public FacebookOAuthRequestBody setCode(String code) {
    this.code = code;
    return this;
  }

  public String getRedirectUri() {
    return redirectUri;
  }

  public FacebookOAuthRequestBody setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
    return this;
  }
}
