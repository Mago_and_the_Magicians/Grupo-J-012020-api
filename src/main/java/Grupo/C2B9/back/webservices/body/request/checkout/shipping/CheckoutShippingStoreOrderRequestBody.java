package Grupo.C2B9.back.webservices.body.request.checkout.shipping;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.enumerations.ShippingEnum;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;
import Grupo.C2B9.back.model.user.Address;
import Grupo.C2B9.back.throwable.badrequest.InvalidCheckoutException;
import Grupo.C2B9.back.throwable.badrequest.InvalidInfoException;
import Grupo.C2B9.back.throwable.badrequest.InvalidTurnDateException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class CheckoutShippingStoreOrderRequestBody {
  private String code;
  private ShippingEnum shippingMethod;
  private String additionalInfo;

  public MarketOrder toMarketOrder() {
    if (this.getAdditionalInfo() == null || this.getAdditionalInfo().equals("")) {
      throw new InvalidInfoException("Additional info is not valid");
    }
    try {
      return new MarketOrder()
        .setCode(this.getCode())
        .setShipping(new Shipping().setType(this.getShippingMethod())
          .setAddress(this.getShippingMethod() == ShippingEnum.DELIVERY
            ? new Address().setCode(additionalInfo) : null)
          .setTurnDate(this.getShippingMethod() == ShippingEnum.PICKUP
            ? LocalDateTime.parse(additionalInfo,
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
            : null));
    } catch (DateTimeParseException dateTimeParseException) {
      throw new InvalidTurnDateException(
        "turn date " + additionalInfo + " is not in the correct format (yyyy-MM-dd HH:mm)");
    }
  }

  public String getCode() {
    return code;
  }

  public CheckoutShippingStoreOrderRequestBody setCode(String code) {
    this.code = code;
    return this;
  }

  public ShippingEnum getShippingMethod() {
    return shippingMethod;
  }

  public CheckoutShippingStoreOrderRequestBody setShippingMethod(ShippingEnum shippingMethod) {
    this.shippingMethod = shippingMethod;
    return this;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public CheckoutShippingStoreOrderRequestBody setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
    return this;
  }
}
