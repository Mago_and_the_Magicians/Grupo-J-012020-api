package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.model.product.Product;

import java.util.List;
import java.util.stream.Collectors;

public class ProductsListLiteResponseBody {

  private List<ProductLiteResponseBody> results;

  public static ProductsListLiteResponseBody valueOf(List<Product> products) {
    return new ProductsListLiteResponseBody().setResults(
      products
        .stream()
        .map(ProductLiteResponseBody::valueOf)
        .collect(Collectors.toList())
    );
  }

  public List<ProductLiteResponseBody> getResults() {
    return results;
  }

  public ProductsListLiteResponseBody setResults(List<ProductLiteResponseBody> results) {
    this.results = results;
    return this;
  }
}
