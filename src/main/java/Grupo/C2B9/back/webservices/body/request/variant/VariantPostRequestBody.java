package Grupo.C2B9.back.webservices.body.request.variant;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.Variant;

public class VariantPostRequestBody {

  private String productCode;
  private String description;
  private String imageUrl;


  public Variant toVariant() {
    return new Variant()
      .setDescription(this.getDescription())
      .setImageUrl(this.getImageUrl())
      .setProduct(new Product().setCode(this.getProductCode()));
  }

  public String getDescription() {
    return description;
  }

  public VariantPostRequestBody setDescription(String description) {
    this.description = description;
    return this;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public VariantPostRequestBody setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  public String getProductCode() {
    return productCode;
  }

  public VariantPostRequestBody setProductCode(String productCode) {
    this.productCode = productCode;
    return this;
  }
}
