package Grupo.C2B9.back.webservices.body.response.checkout.marketOrder;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.enumerations.ShippingEnum;

import java.time.format.DateTimeFormatter;

public class CheckoutShippingStoreOrderResponseBody {

  private String code;
  private double totalAmount;
  private double total;
  private double totalDiscount;
  private ShippingEnum shippingMethod;
  private String additionalInfo;

  public String getCode() {
    return code;
  }

  public CheckoutShippingStoreOrderResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public static CheckoutShippingStoreOrderResponseBody valueOf(MarketOrder marketOrder) {
    return new CheckoutShippingStoreOrderResponseBody()
      .setCode(marketOrder.getCode())
      .setTotal(marketOrder.getTotalAmount())
      .setTotalAmount(marketOrder.getTotalAmount())
      .setTotalDiscount(0.00)
      .setShippingMethod(marketOrder.getShipping().getType())
      .setAdditionalInfo(getAdditionalInfo(marketOrder));
  }

  private static String getAdditionalInfo(MarketOrder marketOrder) {
    return marketOrder.getShipping().getType().equals(ShippingEnum.PICKUP)
      ? marketOrder.getShipping().getTurnDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
      : marketOrder.getShipping().getAddress().getCode();
  }


  public double getTotalAmount() {
    return totalAmount;
  }

  public CheckoutShippingStoreOrderResponseBody setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public CheckoutShippingStoreOrderResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public CheckoutShippingStoreOrderResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }

  public ShippingEnum getShippingMethod() {
    return shippingMethod;
  }

  public CheckoutShippingStoreOrderResponseBody setShippingMethod(ShippingEnum shippingMethod) {
    this.shippingMethod = shippingMethod;
    return this;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public CheckoutShippingStoreOrderResponseBody setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
    return this;
  }
}
