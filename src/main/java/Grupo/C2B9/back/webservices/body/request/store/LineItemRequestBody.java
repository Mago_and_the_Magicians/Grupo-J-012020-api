package Grupo.C2B9.back.webservices.body.request.store;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.lineItem.LineItem;

public class LineItemRequestBody {

  String code;
  Integer quantity;

  public LineItem toLineItem() {
    return new LineItem().setMarketVariant(
      new MarketVariant()
        .setCode(this.getCode()))
      .setQuantity(this.getQuantity());
  }


  public Integer getQuantity() {
    return quantity;
  }

  public LineItemRequestBody setQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public String getCode() {
    return code;
  }

  public LineItemRequestBody setCode(String code) {
    this.code = code;
    return this;
  }
}
