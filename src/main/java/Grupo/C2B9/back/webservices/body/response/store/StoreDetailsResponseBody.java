package Grupo.C2B9.back.webservices.body.response.store;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;
import Grupo.C2B9.back.webservices.body.response.marketOrder.StoreOrderResponseBody;
import Grupo.C2B9.back.webservices.body.response.marketvariant.StoreVariantResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class StoreDetailsResponseBody {

  private Integer id;
  private String code;
  private String name;
  private String department;
  private String address;
  private List<MarketCategory> categories;
  private List<OpenTimeResponseBody> openTimes;
  private List<StoreOrderResponseBody> orders;
  private List<StoreVariantResponseBody> storeVariants;

  public static StoreDetailsResponseBody valueOf(Market market) {
    return new StoreDetailsResponseBody()
      .setCode(market.getCode())
      .setId(market.getId())
      .setAddress(market.getAddress())
      .setDepartment(market.getDepartment())
      .setName(market.getName())
      .setCategories(market.getMarketCategories())
      .setOpenTimes(market.getOpenTimes().stream().map(OpenTimeResponseBody::valueOf).collect(Collectors.toList()))
      .setOrders(market.getMarketOrders().stream().map(StoreOrderResponseBody::valueOf).collect(Collectors.toList()))
      .setStoreVariants(
        market.getMarketVariants().stream().map(StoreVariantResponseBody::valueOf).collect(Collectors.toList()));
  }

  public Integer getId() {
    return id;
  }

  public StoreDetailsResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public StoreDetailsResponseBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getAddress() {
    return address;
  }

  public StoreDetailsResponseBody setAddress(String address) {
    this.address = address;
    return this;
  }

  public String getName() {
    return name;
  }

  public StoreDetailsResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public List<OpenTimeResponseBody> getOpenTimes() {
    return openTimes;
  }

  public StoreDetailsResponseBody setOpenTimes(List<OpenTimeResponseBody> openTimes) {
    this.openTimes = openTimes;
    return this;
  }

  public List<MarketCategory> getCategories() {
    return categories;
  }

  public StoreDetailsResponseBody setCategories(List<MarketCategory> categories) {
    this.categories = categories;
    return this;
  }

  public String getCode() {
    return code;
  }

  public StoreDetailsResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public List<StoreOrderResponseBody> getOrders() {
    return orders;
  }

  public StoreDetailsResponseBody setOrders(List<StoreOrderResponseBody> orders) {
    this.orders = orders;
    return this;
  }

  public List<StoreVariantResponseBody> getStoreVariants() {
    return storeVariants;
  }

  public StoreDetailsResponseBody setStoreVariants(List<StoreVariantResponseBody> storeVariants) {
    this.storeVariants = storeVariants;
    return this;
  }
}
