package Grupo.C2B9.back.webservices.body.response.address;

import Grupo.C2B9.back.model.user.Address;

public class AddressPostResponseBody {

  private Integer id;
  private String code;
  private String street;
  private Integer number;
  private Integer floor;
  private String department;
  private String state;
  private String town;

  public static AddressPostResponseBody valueOf(Address address) {
    return new AddressPostResponseBody()
      .setId(address.getId())
      .setCode(address.getCode())
      .setStreet(address.getStreet())
      .setDepartment(address.getDepartment())
      .setFloor(address.getFloor())
      .setNumber(address.getNumber())
      .setState(address.getProvince())
      .setTown(address.getTown());
  }


  public String getStreet() {
    return street;
  }

  public AddressPostResponseBody setStreet(String street) {
    this.street = street;
    return this;
  }

  public Integer getNumber() {
    return number;
  }

  public AddressPostResponseBody setNumber(Integer number) {
    this.number = number;
    return this;
  }

  public Integer getFloor() {
    return floor;
  }

  public AddressPostResponseBody setFloor(Integer floor) {
    this.floor = floor;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public AddressPostResponseBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getState() {
    return state;
  }

  public AddressPostResponseBody setState(String state) {
    this.state = state;
    return this;
  }

  public String getTown() {
    return town;
  }

  public AddressPostResponseBody setTown(String town) {
    this.town = town;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public AddressPostResponseBody setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getCode() {
    return code;
  }

  public AddressPostResponseBody setCode(String code) {
    this.code = code;
    return this;
  }
}
