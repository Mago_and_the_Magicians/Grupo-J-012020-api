package Grupo.C2B9.back.webservices.body.response.turn;

import Grupo.C2B9.back.service.auxiliar.TurnStructure;

import java.util.List;

public class TurnListResponseBody {

  private List<TurnStructure> turns;


  public List<TurnStructure> getTurns() {
    return turns;
  }

  public TurnListResponseBody setTurns(List<TurnStructure> turns) {
    this.turns = turns;
    return this;
  }
}
