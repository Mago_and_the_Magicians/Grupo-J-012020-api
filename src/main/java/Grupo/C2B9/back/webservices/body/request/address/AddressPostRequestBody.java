package Grupo.C2B9.back.webservices.body.request.address;

import Grupo.C2B9.back.model.user.Address;

public class AddressPostRequestBody {
  private String street;
  private Integer number;
  private Integer floor;
  private String department;
  private String state;
  private String town;

  public Address toAddress() {
    return new Address()
      .setStreet(this.getStreet())
      .setDepartment(this.getDepartment())
      .setFloor(this.getFloor())
      .setNumber(this.getNumber())
      .setProvince(this.getState())
      .setTown(this.getTown());
  }

  public String getStreet() {
    return street;
  }

  public AddressPostRequestBody setStreet(String street) {
    this.street = street;
    return this;
  }

  public Integer getNumber() {
    return number;
  }

  public AddressPostRequestBody setNumber(Integer number) {
    this.number = number;
    return this;
  }

  public Integer getFloor() {
    return floor;
  }

  public AddressPostRequestBody setFloor(Integer floor) {
    this.floor = floor;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public AddressPostRequestBody setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getState() {
    return state;
  }

  public AddressPostRequestBody setState(String state) {
    this.state = state;
    return this;
  }

  public String getTown() {
    return town;
  }

  public AddressPostRequestBody setTown(String town) {
    this.town = town;
    return this;
  }
}
