package Grupo.C2B9.back.webservices.body.response.variant;

import Grupo.C2B9.back.model.product.Variant;

public class VariantLiteResponseBody {

  private String code;
  private String name;

  public static VariantLiteResponseBody valueOf(Variant variant) {
    return new VariantLiteResponseBody()
      .setCode(variant.getCode())
      .setName(variant.getDescription());
  }

  public String getCode() {
    return code;
  }

  public VariantLiteResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public String getName() {
    return name;
  }

  public VariantLiteResponseBody setName(String name) {
    this.name = name;
    return this;
  }
}
