package Grupo.C2B9.back.webservices.body.response.product;

import Grupo.C2B9.back.service.auxiliar.Filter;

import java.util.List;

public class FilterResponseBody {

  private String name;
  private List<String> options;


  public static FilterResponseBody valueOf(Filter filter) {
    return new FilterResponseBody().setName(filter.getName()).setOptions(filter.getOptions());
  }

  public String getName() {
    return name;
  }

  public FilterResponseBody setName(String name) {
    this.name = name;
    return this;
  }

  public List<String> getOptions() {
    return options;
  }

  public FilterResponseBody setOptions(List<String> options) {
    this.options = options;
    return this;
  }
}
