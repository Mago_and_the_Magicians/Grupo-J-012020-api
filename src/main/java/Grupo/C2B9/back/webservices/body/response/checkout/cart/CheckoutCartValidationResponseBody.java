package Grupo.C2B9.back.webservices.body.response.checkout.cart;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.webservices.body.response.checkout.marketOrder.CheckoutStoreOrderResponseBody;

import java.util.List;
import java.util.stream.Collectors;

public class CheckoutCartValidationResponseBody {

  private String code;
  private OrderStatus state;
  private double totalAmount;
  private double total;
  private double totalDiscount;
  private List<CheckoutStoreOrderResponseBody> storeOrders;

  public static CheckoutCartValidationResponseBody valueOf(Order order) {
    return new CheckoutCartValidationResponseBody()
      .setCode(order.getCode())
      .setState(order.getStatus())
      .setTotal(order.getTotalAmount())
      .setTotalAmount(order.getTotalAmount())
      .setTotalDiscount(0.00)
      .setStoreOrders(order.getMarketOrders()
        .stream()
        .map(CheckoutStoreOrderResponseBody::valueOf)
        .collect(Collectors.toList())
      );
  }


  public String getCode() {
    return code;
  }

  public CheckoutCartValidationResponseBody setCode(String code) {
    this.code = code;
    return this;
  }

  public List<CheckoutStoreOrderResponseBody> getStoreOrders() {
    return storeOrders;
  }

  public CheckoutCartValidationResponseBody setStoreOrders(List<CheckoutStoreOrderResponseBody> storeOrders) {
    this.storeOrders = storeOrders;
    return this;
  }

  public double getTotalAmount() {
    return totalAmount;
  }

  public CheckoutCartValidationResponseBody setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  public double getTotal() {
    return total;
  }

  public CheckoutCartValidationResponseBody setTotal(double total) {
    this.total = total;
    return this;
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

  public CheckoutCartValidationResponseBody setTotalDiscount(double totalDiscount) {
    this.totalDiscount = totalDiscount;
    return this;
  }

  public OrderStatus getState() {
    return state;
  }

  public CheckoutCartValidationResponseBody setState(OrderStatus state) {
    this.state = state;
    return this;
  }
}
