package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
public class
AliveController {

  @Autowired
  UserValidationService userValidationService;

  @RequestMapping("/are-you-alive")
  public ResponseEntity<?> index(
    @RequestHeader(name = "Authorization") String token
  ) {
    try {
      return ResponseEntity.ok(
        userValidationService.validateUser(token));
    } catch (RoleNotFoundException | UnsupportedEncodingException roleNotFoundException) {
      return ResponseEntity.badRequest().build();
    }
  }
}
