package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.FileUploadService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.response.marketvariant.MarketVariantListResponseBody;
import Grupo.C2B9.back.webservices.body.response.product.ProductListResponseBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantUploadListResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

@RestController
@RequestMapping("/upload")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class UploadController {

  @Autowired
  FileUploadService fileUploadService;

  @Autowired
  UserValidationService userValidationService;

  @PostMapping(path = "product")
  @Transactional
  public ResponseEntity<?> uploadProducts(
    @RequestParam("file")MultipartFile file
    ) {
    return ResponseEntity
      .ok()
      .body(ProductListResponseBody.valueOf(new ArrayList<>(),fileUploadService.UploadProducts(file)));
  }

  @PostMapping(path = "variant")
  @Transactional
  public ResponseEntity<?> uploadVariants(
    @RequestParam("file")MultipartFile file
  ) {
    return ResponseEntity
      .ok()
      .body(VariantUploadListResponseBody.valueOf(fileUploadService.UploadVariants(file)));
  }

  @PostMapping(path = "market-variant")
  @Transactional
  public ResponseEntity<?> uploadMarketVariants(
    @RequestParam("file")MultipartFile file,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return ResponseEntity
      .ok()
      .body(MarketVariantListResponseBody.valueOf(
        fileUploadService.UploadMarketVariants(file,
        userValidationService.validateUser(token))));
  }
}
