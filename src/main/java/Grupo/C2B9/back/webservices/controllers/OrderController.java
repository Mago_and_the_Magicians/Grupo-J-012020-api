package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.CartService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.request.cart.CartPutRequestBody;
import Grupo.C2B9.back.webservices.body.response.cart.CartPutResponseBody;
import Grupo.C2B9.back.webservices.body.response.order.OrderListResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/orders")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class OrderController {

  @Autowired
  UserValidationService userValidationService;

  @Autowired
  CartService cartService;

  @GetMapping()
  @Transactional
  public ResponseEntity<OrderListResponseBody> getOrders(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return ResponseEntity.ok(OrderListResponseBody.valueOf(
      cartService.getOrdersForUser(
        userValidationService.validateUser(token))));
  }

  @PutMapping()
  public ResponseEntity<CartPutResponseBody> updateOrder(
    @RequestHeader(name = "Authorization") String token,
    @RequestBody CartPutRequestBody order
    ) throws RoleNotFoundException, UnsupportedEncodingException {
    return ResponseEntity
      .ok()
      .body(CartPutResponseBody.valueOf(cartService.updateCart(
        order.toLineItems(),
        cartService.validateCartByUser(userValidationService.validateUser(token))))
      );
  }
}
