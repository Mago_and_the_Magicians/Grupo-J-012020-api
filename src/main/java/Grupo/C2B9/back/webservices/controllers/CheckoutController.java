package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.service.interfaces.CartService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.request.checkout.shipping.CheckoutShippingRequestBody;
import Grupo.C2B9.back.webservices.body.response.checkout.cart.CheckoutCartValidationResponseBody;
import Grupo.C2B9.back.webservices.body.response.checkout.cart.CheckoutFinishValidationResponseBody;
import Grupo.C2B9.back.webservices.body.response.checkout.cart.CheckoutShippingValidationResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;


@RestController
@RequestMapping("/checkout/pass-to")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class CheckoutController {

  private final CartService cartService;
  private final UserValidationService userValidationService;

  public CheckoutController(CartService cartService, UserValidationService userValidationService) {
    this.cartService = cartService;
    this.userValidationService = userValidationService;
  }

  @Transactional
  @PostMapping(path = "shipping")
  public ResponseEntity<CheckoutCartValidationResponseBody> processCartCheckoutToShipping(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    Order order = cartService.validateCartByUser(userValidationService.validateUser(token));
    return ResponseEntity.ok(
      CheckoutCartValidationResponseBody.valueOf(
        cartService.processCartCheckout(order))
    );
  }


  @Transactional
  @PostMapping(path = "payment")
  public ResponseEntity<CheckoutShippingValidationResponseBody> processShippingStatusToPayment(
    @RequestHeader(name = "Authorization") String token,
    @RequestBody CheckoutShippingRequestBody requestBody
    ) throws RoleNotFoundException, UnsupportedEncodingException {
    Order order = cartService.validateShippingByUser(
      userValidationService.validateUser(token),requestBody.toOrder());
    return ResponseEntity.ok(
      CheckoutShippingValidationResponseBody.valueOf(
        cartService.processShippingCheckout(order, requestBody.toOrder()))
    );
  }

  @Transactional
  @PostMapping(path = "finish")
  public ResponseEntity<CheckoutFinishValidationResponseBody> processCartCheckoutToFinish(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    Order order = cartService.validatePaymentByOrderAndUser(userValidationService.validateUser(token));
    return ResponseEntity.ok(
      CheckoutFinishValidationResponseBody.valueOf(
        cartService.processFinishCheckout(order))
    );
  }


}
