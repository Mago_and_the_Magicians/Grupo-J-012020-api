package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.AddressService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.request.address.AddressPostRequestBody;
import Grupo.C2B9.back.webservices.body.response.address.AddressListGetResponseBody;
import Grupo.C2B9.back.webservices.body.response.address.AddressPostResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("addresses")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class AdressController {

  private final AddressService addressService;
  private final UserValidationService userValidationService;

  public AdressController(AddressService addressService, UserValidationService userValidationService) {
    this.addressService = addressService;
    this.userValidationService = userValidationService;
  }

  @GetMapping()
  @Transactional(readOnly = true)
  public ResponseEntity<AddressListGetResponseBody> getAddresses(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      AddressListGetResponseBody
        .valueOf(
          addressService.getByUser(userValidationService.validateUser(token))
        ),
      HttpStatus.OK);
  }


  @PostMapping()
  @Transactional
  public ResponseEntity<AddressPostResponseBody> newAddress(
    @RequestBody AddressPostRequestBody addressPostRequestBody,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      AddressPostResponseBody
        .valueOf(
          addressService.saveAddress(addressPostRequestBody.toAddress(),
            userValidationService.validateUser(token))
        ),
      HttpStatus.OK);
  }
}
