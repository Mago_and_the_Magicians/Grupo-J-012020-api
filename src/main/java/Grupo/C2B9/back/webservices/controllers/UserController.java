package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.response.user.UserGetResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("users")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class UserController {

  private final UserValidationService userValidationService;

  public UserController(UserValidationService userValidationService) {
    this.userValidationService = userValidationService;
  }

  @GetMapping()
  public ResponseEntity<UserGetResponseBody> getUser(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      UserGetResponseBody
        .valueOf(
          userValidationService.validateUser(token)
        ),
      HttpStatus.OK);
  }
}
