package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.AuthService;
import Grupo.C2B9.back.service.interfaces.FacebookOAuthLoginService;
import Grupo.C2B9.back.webservices.body.request.login.LoginRequest;
import Grupo.C2B9.back.webservices.body.request.login.SignupRequest;
import Grupo.C2B9.back.webservices.body.request.oauth.FacebookOAuthRequestBody;
import Grupo.C2B9.back.webservices.body.response.login.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;


@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
@RestController
@RequestMapping("/auth")
public class AuthController {

  @Autowired
  FacebookOAuthLoginService facebookOAuthLoginService;

  @Autowired
  AuthService authService;

  @PostMapping("/login")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    return ResponseEntity.ok(authService.authenticate(loginRequest.toLoginData()));
  }

  @PostMapping("/register")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    return ResponseEntity.ok(authService.registerUser(signUpRequest.toSignUpData()));
  }

  @PostMapping("/facebook")
  public ResponseEntity<JwtResponse> facebookAuth(@RequestBody FacebookOAuthRequestBody requestBody) throws UnsupportedEncodingException {
    return ResponseEntity.ok().body(facebookOAuthLoginService.login(requestBody));
  }
}