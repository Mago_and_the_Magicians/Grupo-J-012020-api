package Grupo.C2B9.back.webservices.controllers;


import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.service.interfaces.FilterService;
import Grupo.C2B9.back.service.interfaces.ProductService;
import Grupo.C2B9.back.service.interfaces.VariantService;
import Grupo.C2B9.back.webservices.body.request.product.ProductCreateRequestBody;
import Grupo.C2B9.back.webservices.body.response.product.ProductCreateResponseBody;
import Grupo.C2B9.back.webservices.body.response.product.ProductDetailResponseBody;
import Grupo.C2B9.back.webservices.body.response.product.ProductListResponseBody;
import Grupo.C2B9.back.webservices.body.response.product.ProductsListLiteResponseBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantListResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("products")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH, RequestMethod.PUT})
public class ProductController {

  private final ProductService productService;
  private final VariantService variantService;
  private final FilterService filterService;

  public ProductController(ProductService productService,
                           VariantService variantService, FilterService filterService) {
    this.productService = productService;
    this.variantService = variantService;
    this.filterService = filterService;
  }

  @GetMapping
  public ResponseEntity<ProductListResponseBody> productsFor(
    @RequestParam Map<String, String> filters) {
    List<Product> products = variantService.filterVariants(productService.productsFor(filters), filters);
    return new ResponseEntity<>(
      ProductListResponseBody.valueOf(
        filterService.getFilters(products),
        products),
      HttpStatus.OK);
  }

  @GetMapping(path = "list")
  public ResponseEntity<ProductsListLiteResponseBody> onlyProducts() {
    List<Product> products = productService.productsFor(new HashMap<>());
    return new ResponseEntity<>(
      ProductsListLiteResponseBody.valueOf(
        products),
      HttpStatus.OK);
  }


  @PostMapping
  public ResponseEntity<ProductCreateResponseBody> addProduct(
    @RequestBody ProductCreateRequestBody product) {
    return new ResponseEntity<>(
      ProductCreateResponseBody.valueOf(
        productService.saveProduct(
          product.toProduct())
      ),
      HttpStatus.OK);
  }

  @GetMapping(path = "{code}")
  public ResponseEntity<ProductDetailResponseBody> getProduct(
    @PathVariable String code
  ) {
    return new ResponseEntity<>(
      ProductDetailResponseBody.valueOf(
        productService.getByCode(code)
      ),
      HttpStatus.OK);
  }

  @GetMapping(path = "{code}/variants")
  public ResponseEntity<VariantListResponseBody> getVariantsByProduct(
    @PathVariable String code
  ) {
    return new ResponseEntity<>(
      VariantListResponseBody.valueOf(
        variantService.getVariantsByProductCode(code)
      ),
      HttpStatus.OK);
  }


}
