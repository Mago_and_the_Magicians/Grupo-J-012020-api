package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.VariantService;
import Grupo.C2B9.back.webservices.body.request.variant.VariantPostRequestBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantDetailResponseBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantListLiteResponseBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantListResponseBody;
import Grupo.C2B9.back.webservices.body.response.variant.VariantPostResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("variants")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class VariantController {


  private final VariantService variantService;

  public VariantController(VariantService variantService) {
    this.variantService = variantService;
  }

  @GetMapping
  public ResponseEntity<VariantListResponseBody> getVariants() {
    return new ResponseEntity<>(
      VariantListResponseBody.valueOf(
        variantService.getAllVariants()),
      HttpStatus.OK);
  }

  @PostMapping()
  public ResponseEntity<VariantPostResponseBody> newVariantForProduct(
    @RequestBody VariantPostRequestBody requestBody
  ) {
    return new ResponseEntity<>(
      VariantPostResponseBody.valueOf(
        variantService.save(requestBody.toVariant())
      ),
      HttpStatus.OK);
  }

  @GetMapping(path = "/list")
  public ResponseEntity<VariantListLiteResponseBody> getVariantsForProduct(
    @RequestParam(name = "productCode") String code
  ) {
    return new ResponseEntity<>(
      VariantListLiteResponseBody.valueOf(
        variantService.getVariantsByProductCode(code)),
      HttpStatus.OK);
  }

  @GetMapping(path = "/{code}")
  public ResponseEntity<VariantDetailResponseBody> getVariantById(
    @PathVariable String code)  {
    return new ResponseEntity<>(
      VariantDetailResponseBody.valueOf(
        variantService.getByCode(code)
      ),
      HttpStatus.OK);
  }
}
