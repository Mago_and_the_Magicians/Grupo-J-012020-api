package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.service.interfaces.MarketService;
import Grupo.C2B9.back.service.interfaces.MarketVariantService;
import Grupo.C2B9.back.service.interfaces.ShippingService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.request.store.StorePostRequestBody;
import Grupo.C2B9.back.webservices.body.request.store.StorePutRequestBody;
import Grupo.C2B9.back.webservices.body.response.store.*;
import Grupo.C2B9.back.webservices.body.response.turn.TurnListResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("stores")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class StoreController {

  private final MarketVariantService marketVariantService;
  private final MarketService marketService;
  private final UserValidationService userValidationService;
  private final ShippingService shippingService;

  public StoreController(
    MarketVariantService marketVariantService,
    MarketService marketService,
    UserValidationService userValidationService, ShippingService shippingService) {
    this.marketVariantService = marketVariantService;
    this.marketService = marketService;
    this.userValidationService = userValidationService;
    this.shippingService = shippingService;
  }

  @GetMapping()
  public ResponseEntity<StoreGetListResponseBody> getStoresForLoggedUser(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      StoreGetListResponseBody.valueOf(marketService.getAllMarketsForUser(userValidationService.validateUser(token))),
      HttpStatus.OK);
  }


  @PostMapping()
  public ResponseEntity<StorePostResponseBody> saveNewMarket(
    @RequestBody StorePostRequestBody requestBody,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      StorePostResponseBody.valueOf(marketService.save(requestBody.toMarket()).setUser(
        userValidationService.validateUser(token))),
      HttpStatus.OK);

  }

  @GetMapping(path = "market-variants")
  public ResponseEntity<StoreResponse> getProduct(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      StoreResponse.valueOf(
        marketVariantService.getMarketVariantsForUser(userValidationService.validateUser(token))
      ),
      HttpStatus.OK);
  }



  @GetMapping(path = "{code}")
  public ResponseEntity<StoreDetailsResponseBody> getAMarket(
    @PathVariable String code,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    Market market = marketService.getByCode(code,userValidationService.validateUser(token));
    return new ResponseEntity<>(
      StoreDetailsResponseBody.valueOf(market
        .setMarketVariants(marketVariantService.getMarketVariantsForMarket(market))),
      HttpStatus.OK);
  }

  @PutMapping(path = "{code}")
  public ResponseEntity<StorePutResponseBody> updateAMarket(
    @PathVariable String code,
    @RequestBody StorePutRequestBody requestBody,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      StorePutResponseBody.valueOf(
        marketService.update(requestBody.toMarket().setCode(code),
          userValidationService.validateUser(token))),
      HttpStatus.OK);
  }

  @GetMapping(path = "{code}/turns")
  public ResponseEntity<TurnListResponseBody> getTurnsForMarket(
    @PathVariable String code
  ) {
    return new ResponseEntity<>(
        new TurnListResponseBody()
          .setTurns(shippingService.getTurnsForMarket(marketService.getByCode(code))),
      HttpStatus.OK);
  }

  @GetMapping(path = "categories")
  public ResponseEntity<StoreCategoriesResponseBody> getCategories(
  ) {
    return new ResponseEntity<>(
      new StoreCategoriesResponseBody()
        .setResults(marketService.getCategories()),
      HttpStatus.OK);
  }

  public MarketVariantService getMarketVariantService() {
    return marketVariantService;
  }
}
