package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.CartService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.request.store.ListOrderPatchRequestBody;
import Grupo.C2B9.back.webservices.body.request.store.StoreOrderPostRequestBody;
import Grupo.C2B9.back.webservices.body.response.cart.CartGetResponseBody;
import Grupo.C2B9.back.webservices.body.response.cart.CartPatchResponseBody;
import Grupo.C2B9.back.webservices.body.response.cart.CartPostResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/cart")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class CartController {

  @Autowired
  UserValidationService userValidationService;

  @Autowired
  CartService cartService;

  @GetMapping()
  public ResponseEntity<CartGetResponseBody> getCart(
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return ResponseEntity.ok(CartGetResponseBody.valueOf(cartService.getOrderDetails(
      cartService.validateCartByUser(userValidationService.validateUser(token)))));
  }


  @Transactional
  @PostMapping(path = "items")
  public ResponseEntity<CartPostResponseBody> addProductToCart(
    @RequestBody StoreOrderPostRequestBody requestBody,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return ResponseEntity.ok(
      CartPostResponseBody.valueOf(
        cartService.addProductToOrder(
          cartService.validateCartByUser(userValidationService.validateUser(token)),
          requestBody.toLineItem())));
  }

  @Transactional
  @PatchMapping(path = "items")
  public ResponseEntity<CartPatchResponseBody> addLineItemsToCart(
    @RequestBody ListOrderPatchRequestBody requestBody,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return ResponseEntity.ok(
      CartPatchResponseBody.valueOf(
        cartService.addLineItemsToOrder(
          cartService.validateCartByUser(userValidationService.validateUser(token)),
          requestBody.toLineItems())));
  }

}
