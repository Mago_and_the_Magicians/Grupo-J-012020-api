package Grupo.C2B9.back.webservices.controllers;

import Grupo.C2B9.back.service.interfaces.MarketService;
import Grupo.C2B9.back.service.interfaces.MarketVariantService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.body.request.marketvariant.MarketVariantPostRequestBody;
import Grupo.C2B9.back.webservices.body.response.marketvariant.MarketVariantPostResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("store-variants")
@CrossOrigin(origins = "*", methods = {
  RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class StoreVariantController {

  private final MarketVariantService marketVariantService;
  private final UserValidationService userValidationService;
  private final MarketService marketService;

  public StoreVariantController(MarketVariantService marketVariantService, UserValidationService userValidationService, MarketService marketService) {
    this.marketVariantService = marketVariantService;
    this.userValidationService = userValidationService;
    this.marketService = marketService;
  }

  @PostMapping()
  public ResponseEntity<MarketVariantPostResponseBody> saveNewMarketVariant(
    @RequestBody MarketVariantPostRequestBody requestBody,
    @RequestHeader(name = "Authorization") String token
  ) throws RoleNotFoundException, UnsupportedEncodingException {
    return new ResponseEntity<>(
      MarketVariantPostResponseBody.valueOf(marketVariantService
        .addMarketVariant(requestBody.toMarketVariant().setMarket(marketService.getByCode(requestBody.getMarketCode())),
        userValidationService.validateUser(token))),
      HttpStatus.OK);

  }
}
