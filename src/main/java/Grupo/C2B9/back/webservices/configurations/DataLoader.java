package Grupo.C2B9.back.webservices.configurations;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;
import Grupo.C2B9.back.model.market.opentime.OpenTime;
import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.ProductCategory;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.model.user.Address;
import Grupo.C2B9.back.model.user.AuthenticationType;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.model.user.role.Role;
import Grupo.C2B9.back.model.user.role.RoleEnum;
import Grupo.C2B9.back.repository.OpenTimeRepository;
import Grupo.C2B9.back.repository.RoleRepository;
import Grupo.C2B9.back.repository.UserRepository;
import Grupo.C2B9.back.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;

@Component
public class DataLoader implements ApplicationRunner {

  private final UserRepository userRepository;
  private final ProductService productService;
  private final VariantService variantService;
  private final MarketService marketService;
  private final MarketVariantService marketVariantService;
  private final AddressService addressService;
  private final RoleRepository roleRepository;
  private final PasswordEncoder encoder;
  private final OpenTimeRepository openTimeRepository;
  private final CartService cartService;

  @Autowired
  public DataLoader(UserRepository userRepository, ProductService productService, VariantService variantService
    , MarketService marketService, MarketVariantService marketVariantService,
                    AddressService addressService, RoleRepository roleRepository, PasswordEncoder encoder,
                    OpenTimeRepository openTimeRepository, CartService cartService) {
    this.userRepository = userRepository;
    this.productService = productService;
    this.variantService = variantService;
    this.marketService = marketService;
    this.marketVariantService = marketVariantService;
    this.addressService = addressService;
    this.roleRepository = roleRepository;
    this.encoder = encoder;
    this.openTimeRepository = openTimeRepository;
    this.cartService = cartService;
  }

  public void run(ApplicationArguments args) {
    roleRepository.save(new Role().setName(RoleEnum.ROLE_USER));
    roleRepository.save(new Role().setName(RoleEnum.ROLE_ADMIN));
    roleRepository.save(new Role().setName(RoleEnum.ROLE_MODERATOR));

    User koru = userRepository.save(
      new User()
        .setId(1)
        .setUsername("test1")
        .setFullname("test the first")
        .setEmail("test1@gmail.com")
        .setPassword(encoder.encode("test1234"))
        .setRoles(new HashSet<>(roleRepository.findAll()))
        .setAuthenticationType(AuthenticationType.LOCAL));
    addressService.saveAddress(
      new Address()
        .setStreet("Av Castro Barros")
        .setNumber(2224)
        .setFloor(2)
        .setDepartment("g")
        .setProvince("CABA")
        .setTown("Boedo"),koru);
    User gunns = userRepository.save(
      new User()
        .setId(2)
        .setUsername("test2")
        .setFullname("test the second")
        .setPassword(encoder.encode("test1234"))
        .setEmail("test2@gmail.com")
        .setRoles(new HashSet<>(roleRepository.findAll()))
        .setAuthenticationType(AuthenticationType.LOCAL));
    addressService.saveAddress(
      new Address()
        .setStreet("Av Mozart")
        .setNumber(724)
        .setFloor(0)
        .setDepartment("PB")
        .setProvince("Bs As")
        .setTown("Quilmes")
        ,gunns);
    Product coca = productService.saveProduct(
      new Product().setBrand("Coca Cola").setName("Coca Cola").setProductCategory(ProductCategory.DRINK));
    Variant coca600 = variantService.save(
      new Variant().setProduct(coca).setDescription("600 ml")
        .setImageUrl("https://www.cocacola.es/content/dam/GO/CokeZone/Spain/Coca-Cola-Full-Red-vidrio-Sabor-Original" +
          ".jpg"));
    Variant coca15 = variantService.save(
      new Variant().setProduct(coca).setDescription("1.5 l")
        .setImageUrl("https://d29nyx213so7hn.cloudfront" +
          ".net/media/catalog/product/cache/52185b3cc8785d37a875707ba86119a3/1/0/107_coca-cola-1.5-l-pet.jpg"));
    Product pepsi = productService.saveProduct(
      new Product().setBrand("Pepsi").setName("Pepsi").setProductCategory(ProductCategory.DRINK));
    Variant pepsi600 = variantService.save(
      new Variant().setProduct(pepsi).setDescription("600 ml")
        .setImageUrl("https://lagranbodega.vteximg.com.br/arquivos/ids/221750-1000-1000/30APEP023_2" +
          ".jpg?v=636866999451300000"));
    Variant pepsi15 = variantService.save(
      new Variant().setProduct(pepsi).setDescription("1.5 l")
        .setImageUrl("https://ddc.com.ve/wp-content/uploads/2020/04/25-600x600.jpg"));
    Product pureza = productService.saveProduct(
      new Product().setBrand("Pureza").setName("Harina Pureza").setProductCategory(ProductCategory.WAREHOUSE));
    Variant purezapizza = variantService.save(new Variant().setProduct(pureza).setDescription("Leudante 1kg")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00249800/00249890.jpg?3.0.115"));
    Variant pureza0000 = variantService.save(new Variant().setProduct(pureza).setDescription("0000 1kg")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00253600/00253696.jpg?3.0.115"));
    Product milkaut = productService.saveProduct(
      new Product().setBrand("Milkaut").setName("Dulce de leche Milkaut").setProductCategory(ProductCategory.MILK));
    Variant milkautrepostero = variantService.save(new Variant().setProduct(milkaut)
      .setDescription("Repostero 400g")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00194400/00194481.jpg?3.0.115"));
    Product yogurisimo = productService.saveProduct(
      new Product().setBrand("La Serenisima").setName("Yogurisimo").setProductCategory(ProductCategory.MILK));
    Variant yogurisimobebiblefrutilla = variantService.save(
      new Variant().setProduct(yogurisimo).setDescription("Bebible Sachet 1l Frutilla")
        .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00187000/00187021.jpg?3.0.115"));
    Variant yogurisimobebiblevainilla = variantService.save(
      new Variant().setProduct(yogurisimo).setDescription("Bebible Sachet 1l Vainilla")
        .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00187000/00187025.jpg?3.0.115"));
    Product ddlSerenisima = productService.saveProduct(
      new Product().setBrand("La Serenisima").setName("Dulce de leche La Serenisima").setProductCategory(ProductCategory.MILK));
    Variant ddlSerenisima400 = variantService.save(new Variant().setProduct(ddlSerenisima)
      .setDescription("Clasico 400g")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00251800/00251874.jpg?3.0.115"));
    Variant ddlSerenisima400Colonial = variantService.save(new Variant().setProduct(ddlSerenisima)
      .setDescription("Estilo Colonial 400g")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00251800/00251873.jpg?3.0.115"));
    Product jabonLiquidoAriel = productService.saveProduct(
      new Product().setBrand("Ariel").setName("Jabon Liquido Ariel").setProductCategory(ProductCategory.CLEANING));
    Variant ariel3lClasico = variantService.save(new Variant().setProduct(jabonLiquidoAriel)
      .setDescription("Clasico 3l")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00272600/00272680.jpg?3.0.115"));
    Variant ariel3lSuavizante = variantService.save(new Variant().setProduct(jabonLiquidoAriel)
      .setDescription("Con suavizant 3l")
      .setImageUrl("https://static.cotodigital3.com.ar/sitios/fotos/medium/00297000/00297092.jpg?3.0.115"));
    Market maxi = marketService.save(
      new Market().setName("maxi el quiosko").setDepartment("Quilmes").setUser(koru).setMarketCategories(
        Arrays.asList(MarketCategory.Carniceria, MarketCategory.Almacen, MarketCategory.Verduleria)));
    Market minnie = marketService.save(
      new Market().setName("minnie quiosco").setDepartment("Florencio Varela").setUser(gunns).setMarketCategories(
        Arrays.asList(MarketCategory.Carniceria, MarketCategory.Almacen)
      ));
    openTimeRepository.save(
      new OpenTime().setMarket(maxi).setWorkDays(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY))
        .setStart(LocalTime.MIDNIGHT.plusHours(12).plusMinutes(0))
        .setEnd(LocalTime.MIDNIGHT.plusHours(19).plusMinutes(0))
    );
    openTimeRepository.save(
      new OpenTime().setMarket(minnie).setWorkDays(Arrays.asList(DayOfWeek.TUESDAY, DayOfWeek.THURSDAY))
        .setStart(LocalTime.MIDNIGHT.plusHours(11).plusMinutes(0))
        .setEnd(LocalTime.MIDNIGHT.plusHours(20).plusMinutes(0)));
    MarketVariant coca600Maxi = marketVariantService.addMarketVariant(new MarketVariant().setVariant(coca600)
      .setMarket(maxi).setPrice(25.00).setStock(4), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(coca15)
      .setMarket(maxi).setPrice(52.00).setStock(10), koru);
    MarketVariant pepsi600minnie = marketVariantService.addMarketVariant(new MarketVariant().setVariant(pepsi600)
      .setMarket(minnie).setPrice(30.00).setStock(4), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(coca15)
      .setMarket(minnie).setPrice(53.00).setStock(7), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(pepsi600)
      .setMarket(maxi).setPrice(35.00).setStock(4), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(pepsi15)
      .setMarket(minnie).setPrice(54.00).setStock(10), gunns);
    MarketVariant purezaPizzaMaxi = marketVariantService.addMarketVariant(new MarketVariant().setVariant(purezapizza)
      .setMarket(maxi).setPrice(86.99).setStock(10), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(pureza0000)
      .setMarket(maxi).setPrice(79.99).setStock(10), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(purezapizza)
      .setMarket(minnie).setPrice(88.99).setStock(10), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(pureza0000)
      .setMarket(minnie).setPrice(73.99).setStock(10), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(milkautrepostero)
      .setMarket(maxi).setPrice(44.99).setStock(4), koru);
    MarketVariant milkautReposteroMinnie = marketVariantService.addMarketVariant(new MarketVariant().setVariant(milkautrepostero)
      .setMarket(minnie).setPrice(56.99).setStock(6), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(yogurisimobebiblefrutilla)
      .setMarket(maxi).setPrice(79.99).setStock(22), koru);
    MarketVariant yogurisimoFrutillaMinnie = marketVariantService
      .addMarketVariant(new MarketVariant().setVariant(yogurisimobebiblefrutilla)
      .setMarket(minnie).setPrice(88.99).setStock(14), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(yogurisimobebiblevainilla)
      .setMarket(maxi).setPrice(79.99).setStock(16), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(yogurisimobebiblevainilla)
      .setMarket(minnie).setPrice(88.99).setStock(13), gunns);
    MarketVariant ddlSerenisima400Maxi =
      marketVariantService.addMarketVariant(new MarketVariant().setVariant(ddlSerenisima400)
      .setMarket(maxi).setPrice(59.99).setStock(8), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ddlSerenisima400)
      .setMarket(minnie).setPrice(55.99).setStock(15), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ddlSerenisima400Colonial)
      .setMarket(maxi).setPrice(62.99).setStock(10), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ddlSerenisima400Colonial)
      .setMarket(minnie).setPrice(61.99).setStock(8), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ariel3lClasico)
      .setMarket(maxi).setPrice(65.99).setStock(12), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ariel3lClasico)
      .setMarket(minnie).setPrice(66.99).setStock(18), gunns);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ariel3lSuavizante)
      .setMarket(maxi).setPrice(72.99).setStock(15), koru);
    marketVariantService.addMarketVariant(new MarketVariant().setVariant(ariel3lSuavizante)
      .setMarket(minnie).setPrice(71.99).setStock(16), gunns);
    Order koruOrder1 = cartService.validateCartByUser(koru);
    cartService.addProductToOrder(koruOrder1,new LineItem().setMarketVariant(purezaPizzaMaxi).setQuantity(1));
    cartService.addProductToOrder(koruOrder1,new LineItem().setMarketVariant(coca600Maxi).setQuantity(2));
    cartService.addProductToOrder(koruOrder1,new LineItem().setMarketVariant(milkautReposteroMinnie).setQuantity(1));
    cartService.save(koruOrder1.setStatus(OrderStatus.FINISHED));
    Order koruOrder2 = cartService.validateCartByUser(koru);
    cartService.addProductToOrder(koruOrder2,new LineItem().setMarketVariant(pepsi600minnie).setQuantity(2));
      cartService.addProductToOrder(koruOrder2,new LineItem().setMarketVariant(yogurisimoFrutillaMinnie).setQuantity(7));
    cartService.addProductToOrder(koruOrder2,new LineItem().setMarketVariant(ddlSerenisima400Maxi).setQuantity(2));
    cartService.save(koruOrder2.setStatus(OrderStatus.FINISHED));

  }
}