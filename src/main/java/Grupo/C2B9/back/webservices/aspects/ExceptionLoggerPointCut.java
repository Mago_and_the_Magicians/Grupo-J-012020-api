package Grupo.C2B9.back.webservices.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Configuration

@Aspect

public class ExceptionLoggerPointCut {

  private static final Logger LOGGER = LogManager.getLogger(ExceptionLoggerPointCut.class);

  @AfterThrowing(pointcut = "execution(* Grupo.C2B9.back.service.*.*.*(..))", throwing = "ex")

  public void logError(Exception ex) {
    LOGGER.error("Exception " + ex.getClass().toString() + " thrown with message: " + ex.getMessage());

  }

}