package Grupo.C2B9.back.model.market.opentime;

import Grupo.C2B9.back.model.market.Market;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(OpenTime.class)
public class OpenTime_ {
  public static volatile SingularAttribute<OpenTime, Market> market;
}
