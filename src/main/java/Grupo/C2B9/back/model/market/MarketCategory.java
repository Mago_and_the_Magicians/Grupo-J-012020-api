package Grupo.C2B9.back.model.market;

public enum MarketCategory {
  Ferreteria,
  Almacen,
  Verduleria,
  Carniceria
}
