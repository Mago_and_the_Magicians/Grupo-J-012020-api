package Grupo.C2B9.back.model.market.variant;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.product.Variant;

import javax.persistence.*;

@Entity
public class MarketVariant {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @ManyToOne
  private Market market;

  @ManyToOne
  private Variant variant;

  @Column
  private Integer stock;

  @Column
  private Double price;

  @Column
  private String code;

  public Integer getId() {
    return id;
  }

  public MarketVariant setId(Integer id) {
    this.id = id;
    return this;
  }

  public Market getMarket() {
    return market;
  }

  public MarketVariant setMarket(Market market) {
    this.market = market;
    return this;
  }

  public Variant getVariant() {
    return variant;
  }

  public MarketVariant setVariant(Variant variant) {
    this.variant = variant;
    return this;
  }

  public Double getPrice() {
    return price;
  }

  public MarketVariant setPrice(Double price) {
    this.price = price;
    return this;
  }

  public Integer getStock() {
    return stock;
  }

  public MarketVariant setStock(Integer stock) {
    this.stock = stock;
    return this;
  }

  public String getCode() {
    return code;
  }

  public MarketVariant setCode(String code) {
    this.code = code;
    return this;
  }
}
