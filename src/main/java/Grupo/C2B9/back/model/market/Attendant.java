package Grupo.C2B9.back.model.market;

import Grupo.C2B9.back.model.user.User;

import javax.persistence.*;

@Entity
public class Attendant {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @OneToOne
  private Market market;

  @OneToOne
  private User user;

  public Integer getId() {
    return id;
  }

  public Attendant setId(Integer id) {
    this.id = id;
    return this;
  }

  public Market getMarket() {
    return market;
  }

  public Attendant setMarket(Market market) {
    this.market = market;
    return this;
  }
}
