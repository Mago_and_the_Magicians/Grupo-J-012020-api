package Grupo.C2B9.back.model.market;

import Grupo.C2B9.back.model.user.User;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Market.class)
public class Market_ {
  public static volatile SingularAttribute<Market, Integer> id;
  public static volatile SingularAttribute<Market, User> user;
  public static volatile SingularAttribute<Market, String> code;
}
