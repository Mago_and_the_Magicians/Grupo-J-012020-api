package Grupo.C2B9.back.model.market;

import java.util.Comparator;

public enum WorkDay {
  monday(1),
  tuesday(2),
  wednesday(3),
  thursday(4),
  friday(5),
  saturday(6),
  sunday(0);

  private final int calendarPosition;

  WorkDay(int position) {
    calendarPosition = position;
  }

  int getCalendarPosition() {
    return calendarPosition;
  }

  public static Comparator<WorkDay> calendarPositionComparator = new Comparator<WorkDay>() {
    public int compare(WorkDay d1, WorkDay d2) {
      return d1.getCalendarPosition() - d2.getCalendarPosition();
    }
  };
}
