package Grupo.C2B9.back.model.market.variant;


import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.product.Variant;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(MarketVariant.class)
public class MarketVariant_ {
  public static volatile SingularAttribute<MarketVariant, Integer> id;
  public static volatile SingularAttribute<MarketVariant, Variant> variant;
  public static volatile SingularAttribute<MarketVariant, Double> price;
  public static volatile SingularAttribute<MarketVariant, Market> market;
  public static volatile SingularAttribute<MarketVariant, String> code;
}