package Grupo.C2B9.back.model.market;

import Grupo.C2B9.back.model.user.User;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Attendant.class)
public class Attendant_ {
  private static volatile SingularAttribute<Attendant, User> user;
  private static volatile SingularAttribute<Attendant, Market> market;

}
