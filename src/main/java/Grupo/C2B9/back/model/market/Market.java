package Grupo.C2B9.back.model.market;

//Cada comercio debe poder
// crear su perfil indicando su
// rubro, domicilio, días y
// horarios de atención,
// medios de pago y
// distancia máxima para realizar un envío.

import Grupo.C2B9.back.model.market.opentime.OpenTime;
import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Market {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Column
  private String department;

  @Column
  private String address;

  @Column
  private String name;

  @ManyToOne
  private User user;

  @Column
  private String code;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "market")
  private List<OpenTime> openTimes;

  @OneToMany(mappedBy = "market", fetch = FetchType.LAZY)
  private List<MarketVariant> marketVariants = new ArrayList<>();

  @OneToMany(mappedBy = "market", fetch = FetchType.LAZY)
  private List<MarketOrder> marketOrders = new ArrayList<>();

  @ElementCollection(targetClass = MarketCategory.class)
  @Enumerated(value = EnumType.STRING)
  private List<MarketCategory> marketCategories = new ArrayList<>();

  public static Market anyMarket() {
    Integer id = (int) (Math.random() * 100);
    return new Market().setId(id)
      .setAddress("any address")
      .setDepartment("any department")
      .setName("a random market name " + id);
  }

  public Integer getId() {
    return id;
  }

  public Market setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public Market setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getAddress() {
    return address;
  }

  public Market setAddress(String address) {
    this.address = address;
    return this;
  }

  public String getName() {
    return name;
  }

  public Market setName(String name) {
    this.name = name;
    return this;
  }

  public List<MarketVariant> getMarketVariants() {
    return marketVariants;
  }

  public Market setMarketVariants(List<MarketVariant> marketVariants) {
    this.marketVariants = marketVariants;
    return this;
  }

  public Market addMarketVariant(MarketVariant marketVariant) {
    this.getMarketVariants().add(marketVariant);
    return this;
  }

  public List<Variant> getVariantsForMarket(Product product) {
    return this.getMarketVariants().stream()
      .filter(marketVariant -> marketVariant.getVariant().getProduct() == product)
      .map(MarketVariant::getVariant)
      .collect(Collectors.toList());
  }

  public User getUser() {
    return user;
  }

  public Market setUser(User user) {
    this.user = user;
    return this;
  }

  public List<MarketCategory> getMarketCategories() {
    return marketCategories;
  }

  public Market setMarketCategories(List<MarketCategory> marketCategories) {
    this.marketCategories = marketCategories;
    return this;
  }

  public List<OpenTime> getOpenTimes() {
    return openTimes;
  }

  public Market setOpenTimes(List<OpenTime> openTimes) {
    this.openTimes = openTimes;
    return this;
  }

  public String getCode() {
    return code;
  }

  public Market setCode(String code) {
    this.code = code;
    return this;
  }

  public List<MarketOrder> getMarketOrders() {
    return marketOrders;
  }

  public Market setMarketOrders(List<MarketOrder> marketOrders) {
    this.marketOrders = marketOrders;
    return this;
  }
}
