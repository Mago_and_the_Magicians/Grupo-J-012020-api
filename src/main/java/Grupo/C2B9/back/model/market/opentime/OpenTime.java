package Grupo.C2B9.back.model.market.opentime;

import Grupo.C2B9.back.model.market.Market;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

@Entity
public class OpenTime {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Column
  private LocalTime start;

  @Column
  private LocalTime end;

  @ManyToOne
  private Market market;

  @ElementCollection(targetClass = DayOfWeek.class)
  @Enumerated(value = EnumType.STRING)
  private List<DayOfWeek> workDays;


  public Integer getId() {
    return id;
  }

  public OpenTime setId(Integer id) {
    this.id = id;
    return this;
  }

  public LocalTime getStart() {
    return start;
  }

  public OpenTime setStart(LocalTime start) {
    this.start = start;
    return this;
  }

  public LocalTime getEnd() {
    return end;
  }

  public OpenTime setEnd(LocalTime end) {
    this.end = end;
    return this;
  }


  public Market getMarket() {
    return market;
  }

  public OpenTime setMarket(Market market) {
    this.market = market;
    return this;
  }

  public boolean isEqual(OpenTime openTime1) {
    return openTime1.getWorkDays().equals(this.getWorkDays())
      && openTime1.getStart().equals(this.getStart())
      && openTime1.getEnd().equals(this.getEnd());
  }

  public List<DayOfWeek> getWorkDays() {
    return workDays;
  }

  public OpenTime setWorkDays(List<DayOfWeek> workDays) {
    this.workDays = workDays;
    return this;
  }
}
