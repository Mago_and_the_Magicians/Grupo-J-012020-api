package Grupo.C2B9.back.model.order.marketorder;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class MarketOrder {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Column
  private String code;

  @OneToMany(fetch = FetchType.EAGER)
  private List<LineItem> lineItems = new ArrayList<>();

  @ManyToOne
  private Market market;

  @ManyToOne
  private Order order;

  @OneToOne(fetch = FetchType.EAGER)
  private Shipping shipping;


  public Integer getId() {
    return id;
  }

  public MarketOrder setId(Integer id) {
    this.id = id;
    return this;
  }

  public List<LineItem> getLineItems() {
    return lineItems;
  }

  public MarketOrder setLineItems(List<LineItem> lineItems) {
    this.lineItems = lineItems;
    return this;
  }

  public Double getTotalAmount() {
    return lineItems
      .stream()
      .mapToDouble(LineItem::getPrice)
      .sum();
  }

  public Market getMarket() {
    return market;
  }

  public MarketOrder setMarket(Market market) {
    this.market = market;
    return this;
  }

  public Order getOrder() {
    return order;
  }

  public MarketOrder setOrder(Order order) {
    this.order = order;
    return this;
  }

  public String getCode() {
    return code;
  }

  public MarketOrder setCode(String code) {
    this.code = code;
    return this;
  }

  public void validateStock() {
    lineItems.forEach(
      LineItem::validateStock
    );
  }

  public MarketOrder substractStock() {
    return this.setLineItems(lineItems.stream().map(
      LineItem::substractStock).collect(Collectors.toList())
    );
  }

  public Shipping getShipping() {
    return shipping;
  }

  public MarketOrder setShipping(Shipping shipping) {
    this.shipping = shipping;
    return this;
  }
}
