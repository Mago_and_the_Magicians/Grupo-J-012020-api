package Grupo.C2B9.back.model.order;

import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.model.user.User;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Order.class)
public class Order_ {
  public static volatile SingularAttribute<Order, Integer> id;
  public static volatile SingularAttribute<Order, String> code;
  public static volatile SingularAttribute<Order, OrderStatus> status;
  public static volatile SingularAttribute<Order, User> user;
}
