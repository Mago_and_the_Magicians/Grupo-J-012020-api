package Grupo.C2B9.back.model.order.marketorder.shipping;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Shipping.class)
public class Shipping_ {

  public static volatile SingularAttribute<Shipping, MarketOrder> marketOrder;
}
