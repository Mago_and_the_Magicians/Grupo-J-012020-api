package Grupo.C2B9.back.model.order.marketorder;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.Order;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(MarketOrder.class)
public class MarketOrder_ {
  public static volatile SingularAttribute<MarketOrder, Integer> id;
  public static volatile SingularAttribute<MarketOrder, Market> market;
  public static volatile SingularAttribute<MarketOrder, Order> order;
}
