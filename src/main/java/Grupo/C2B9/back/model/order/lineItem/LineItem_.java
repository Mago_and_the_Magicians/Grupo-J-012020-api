package Grupo.C2B9.back.model.order.lineItem;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(LineItem.class)
public class LineItem_ {
  public static volatile SingularAttribute<LineItem, Integer> id;
  public static volatile SingularAttribute<LineItem, MarketOrder> marketOrder;
  public static volatile SingularAttribute<LineItem, MarketVariant> marketVariant;
}
