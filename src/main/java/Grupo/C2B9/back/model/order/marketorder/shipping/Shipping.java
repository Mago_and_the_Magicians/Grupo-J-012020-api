package Grupo.C2B9.back.model.order.marketorder.shipping;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.enumerations.ShippingEnum;
import Grupo.C2B9.back.model.user.Address;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Shipping {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Enumerated
  @Column
  private ShippingEnum type;

  @ManyToOne
  private Address address;

  @OneToOne
  private MarketOrder marketOrder;

  @Column
  private LocalDateTime turnDate;

  @Column
  private double amount;

  public Integer getId() {
    return id;
  }

  public Shipping setId(Integer id) {
    this.id = id;
    return this;
  }

  public ShippingEnum getType() {
    return type;
  }

  public Shipping setType(ShippingEnum type) {
    this.type = type;
    return this;
  }

  public MarketOrder getMarketOrder() {
    return marketOrder;
  }

  public Shipping setMarketOrder(MarketOrder marketOrder) {
    this.marketOrder = marketOrder;
    return this;
  }

  public Address getAddress() {
    return address;
  }

  public Shipping setAddress(Address address) {
    this.address = address;
    return this;
  }

  public LocalDateTime getTurnDate() {
    return turnDate;
  }

  public Shipping setTurnDate(LocalDateTime turnDate) {
    this.turnDate = turnDate;
    return this;
  }

  public double getAmount() {
    return amount;
  }

  public Shipping setAmount(double amount) {
    this.amount = amount;
    return this;
  }
}
