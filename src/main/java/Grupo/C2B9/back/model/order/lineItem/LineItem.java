package Grupo.C2B9.back.model.order.lineItem;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.throwable.badrequest.InvalidStockException;

import javax.persistence.*;

@Entity
public class LineItem {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @ManyToOne
  private MarketOrder marketOrder;

  @ManyToOne
  private MarketVariant marketVariant;

  @Column
  private double price = 0.00;

  @Column
  private Integer quantity = 0;

  @Column
  private Integer discountedQuantity = 0;

  public MarketVariant getMarketVariant() {
    return marketVariant;
  }

  public LineItem setMarketVariant(MarketVariant marketVariant) {
    this.marketVariant = marketVariant;
    return this;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public LineItem setQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public double getPrice() {
    return this.marketVariant.getPrice() * this.getQuantity();
  }

  public MarketOrder getMarketOrder() {
    return marketOrder;
  }

  public LineItem setMarketOrder(MarketOrder marketOrder) {
    this.marketOrder = marketOrder;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public LineItem setId(Integer id) {
    this.id = id;
    return this;
  }

  public void validateStock() {
    if (marketVariant.getStock() < this.getQuantity()) {
      throw new InvalidStockException(this.getMarketVariant().getCode()
        + " actual stock is "
        + this.getMarketVariant().getStock()
        + " but required quantity was "
        + this.getQuantity());
    }
  }

  public LineItem substractStock() {
    this.getMarketVariant().setStock(this.getMarketVariant().getStock() - this.getQuantity());

    return this;
  }

  public String getProductName() {
    return this.getMarketVariant().getVariant().getProduct().getName();
  }

  public String getVariantName() {
    return this.getMarketVariant().getVariant().getDescription();
  }

  public boolean validStock() {
    return marketVariant.getStock() >= this.getUnsustractedStock();
  }

  public Integer getDiscountedQuantity() {
    return discountedQuantity;
  }

  public LineItem setDiscountedQuantity(Integer discountedQuantity) {
    this.discountedQuantity = discountedQuantity;
    return this;
  }

  public Integer getStockToSubstract() {
    return Integer.min(this.getQuantity() - this.getDiscountedQuantity(), this.getMarketVariant().getStock());
  }

  public Integer getAvailableCartStock() {
    return Integer.min(this.getQuantity(), this.getMarketVariant().getStock());
  }

  public Integer getStock() {
    return this.marketVariant.getStock();
  }

  public Integer getUnsustractedStock() {
    return this.getQuantity() - this.getDiscountedQuantity();
  }

  public LineItem setPrice(double price) {
    this.price = price;
    return this;
  }
}
