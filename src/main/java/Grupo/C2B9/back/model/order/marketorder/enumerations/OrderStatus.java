package Grupo.C2B9.back.model.order.marketorder.enumerations;

public enum OrderStatus {
  CART,
  SHIPPING,
  PAYMENT,
  FINISHED
}
