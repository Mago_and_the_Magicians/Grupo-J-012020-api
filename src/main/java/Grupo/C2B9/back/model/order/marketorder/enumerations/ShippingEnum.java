package Grupo.C2B9.back.model.order.marketorder.enumerations;

public enum ShippingEnum {
  PICKUP,
  DELIVERY {
    @Override
    public double getAmount() {
      return 40.00;
    }
  };

  public double getAmount() {
    return 0;
  }
}
