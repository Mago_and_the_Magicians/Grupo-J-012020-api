package Grupo.C2B9.back.model.order;


import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "User_Order")
public class Order {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Column
  private String code;

  @OneToMany(fetch = FetchType.LAZY)
  private List<MarketOrder> marketOrders = new ArrayList<>();

  @ManyToOne
  private User user;

  @Enumerated(value = EnumType.STRING)
  private OrderStatus status;


  public Double getTotalAmount() {
    if (marketOrders.size() == 0) {
      return 0.00;
    }
    return marketOrders.stream()
      .mapToDouble(marketOrder -> marketOrder.getTotalAmount() + (
        marketOrder.getShipping() == null ? 0.00 : marketOrder.getShipping().getAmount())   )
      .sum();
  }


  private List<Variant> getVariants() {
    return null;
  }

  public User getUser() {
    return user;
  }

  public Order setUser(User user) {
    this.user = user;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public Order setId(Integer id) {
    this.id = id;
    return this;
  }

  public List<MarketOrder> getMarketOrders() {
    return marketOrders;
  }

  public Order setMarketOrders(List<MarketOrder> marketOrders) {
    this.marketOrders = marketOrders;
    return this;
  }

  public OrderStatus getStatus() {
    return status;
  }

  public Order setStatus(OrderStatus status) {
    this.status = status;
    return this;
  }

  public String getCode() {
    return code;
  }

  public Order setCode(String code) {
    this.code = code;
    return this;
  }

  public List<LineItem> getLineItems() {
    return this.getMarketOrders()
      .stream()
      .map(MarketOrder::getLineItems)
      .collect(ArrayList::new, List::addAll, List::addAll);
  }
}
