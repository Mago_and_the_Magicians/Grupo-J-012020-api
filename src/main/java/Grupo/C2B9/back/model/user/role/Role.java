package Grupo.C2B9.back.model.user.role;

import javax.persistence.*;

@Entity

public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  @Column(length = 20)
  private RoleEnum name;

  public Role() {

  }

  public Role(RoleEnum name) {
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public Role setId(Integer id) {
    this.id = id;
    return this;
  }

  public RoleEnum getName() {
    return name;
  }

  public Role setName(RoleEnum name) {
    this.name = name;
    return this;
  }
}