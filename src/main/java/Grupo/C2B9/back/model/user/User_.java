package Grupo.C2B9.back.model.user;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(User.class)
public class User_ {

  public static volatile SingularAttribute<User, Integer> id;
}
