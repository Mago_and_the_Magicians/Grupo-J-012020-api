package Grupo.C2B9.back.model.user;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Address.class)
public class Address_ {

  public static volatile SingularAttribute<Address, User> user;
  public static volatile SingularAttribute<Address, String> code;
}
