package Grupo.C2B9.back.model.user.role;

public enum RoleEnum {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN
}
