package Grupo.C2B9.back.model.user;

import javax.persistence.*;

@Entity
public class Address {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column
  private String code;

  @Column
  private String street;

  @Column
  private Integer number;

  @Column
  private Integer floor;

  @Column
  private String department;

  @Column
  private String province;

  @Column
  private String town;

  @ManyToOne(fetch = FetchType.EAGER)
  private User user;


  public Integer getId() {
    return id;
  }

  public Address setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getStreet() {
    return street;
  }

  public Address setStreet(String street) {
    this.street = street;
    return this;
  }

  public Integer getNumber() {
    return number;
  }

  public Address setNumber(Integer number) {
    this.number = number;
    return this;
  }

  public Integer getFloor() {
    return floor;
  }

  public Address setFloor(Integer floor) {
    this.floor = floor;
    return this;
  }

  public String getDepartment() {
    return department;
  }

  public Address setDepartment(String department) {
    this.department = department;
    return this;
  }

  public String getProvince() {
    return province;
  }

  public Address setProvince(String province) {
    this.province = province;
    return this;
  }

  public String getTown() {
    return town;
  }

  public Address setTown(String town) {
    this.town = town;
    return this;
  }

  public User getUser() {
    return user;
  }

  public Address setUser(User user) {
    this.user = user;
    return this;
  }

  public boolean equalAddress(Address address) {
    return
      (
        address.getDepartment() == null
          || address.getDepartment().equals(this.getDepartment())
          || this.getDepartment() == null
      )
        &&
        (
          address.getFloor() == null
            || address.getFloor().equals(this.getFloor())
            || this.getFloor() == null
        )
        &&
        (
          address.getNumber() == null
            || address.getNumber().equals(this.getNumber())
            || this.getNumber() == null
        )
        &&
        (
          address.getProvince() == null
            || address.getProvince().equals(this.getProvince())
            || this.getProvince() == null
        )
        &&
        (
          address.getStreet() == null
            || address.getStreet().equals(this.getStreet())
            || this.getStreet() == null
        )
        &&
        (
          address.getTown() == null
            || address.getTown().equals(this.getTown())
            || this.getTown() == null
        );
  }

  @Override
  public String toString() {
    return "{ " +
      "street='" + street + '\'' +
      ", number=" + number +
      ", floor=" + floor +
      ", department='" + department + '\'' +
      ", province='" + province + '\'' +
      ", town='" + town + '\'' +
      " }";
  }

  public String getCode() {
    return code;
  }

  public Address setCode(String code) {
    this.code = code;
    return this;
  }
}
