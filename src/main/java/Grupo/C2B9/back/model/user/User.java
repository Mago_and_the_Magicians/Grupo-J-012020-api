package Grupo.C2B9.back.model.user;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.user.role.Role;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = "username"),
  @UniqueConstraint(columnNames = "email")
})
public class User {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Column(unique = true)
  @Size(max = 20)
  private String username;

  @Column
  @Size(max = 120)
  private String password;

  @Column
  private String fullname;

  @Column
  @Size(max = 60)
  private String email;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  private List<Market> markets;

  @OneToMany(mappedBy = "user")
  private List<Address> addresses = new ArrayList<>();

  @ManyToMany(fetch = FetchType.LAZY)
  private Set<Role> roles = new HashSet<>();

  @Enumerated(value = EnumType.STRING)
  @Column
  private AuthenticationType authenticationType;

  public User() {
  }

  public User(String username, String email, String password) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

  public Integer getId() {
    return id;
  }

  public User setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getUsername() {
    return username;
  }

  public User setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getFullname() {
    return fullname;
  }

  public User setFullname(String fullname) {
    this.fullname = fullname;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public User setEmail(String email) {
    this.email = email;
    return this;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  public User setAddresses(List<Address> addresses) {
    this.addresses = addresses;
    return this;
  }

  public List<Market> getMarkets() {
    return markets;
  }

  public User setMarkets(List<Market> markets) {
    this.markets = markets;
    return this;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public User setRoles(Set<Role> roles) {
    this.roles = roles;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public User setPassword(String password) {
    this.password = password;
    return this;
  }

  public AuthenticationType getAuthenticationType() {
    return authenticationType;
  }

  public User setAuthenticationType(AuthenticationType authenticationType) {
    this.authenticationType = authenticationType;
    return this;
  }
}
