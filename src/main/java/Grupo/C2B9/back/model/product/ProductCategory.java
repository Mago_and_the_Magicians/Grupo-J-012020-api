package Grupo.C2B9.back.model.product;

public enum ProductCategory {
  WAREHOUSE,
  CLEANING,
  MILK,
  DRINK
}
