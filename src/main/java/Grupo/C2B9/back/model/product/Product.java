package Grupo.C2B9.back.model.product;

import Grupo.C2B9.back.webservices.body.request.product.ProductCreateRequestBody;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//La aplicación debe permitir listar,
// para un comercio específico,
// los productos a la venta para lo
//cual se debe poder cargar
// nombre, marca, stock, precio y una imagen.

@Entity
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column
  private String name;

  @Column
  private String brand;

  @Column
  private ProductCategory productCategory;

  @Column
  private String code;

  @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
  private List<Variant> variants = new ArrayList<>();

  public static Product valueOf(ProductCreateRequestBody product) {
    return new Product()
      .setBrand(product.getBrand())
      .setName(product.getName());
  }

  public List<Variant> getVariants() {
    return variants;
  }

  public Product setVariants(List<Variant> variants) {
    this.variants = variants;
    return this;
  }

  public Product addVariant(Variant variant) {
    this.variants.add(variant);
    return this;
  }

  public Product addVariant(List<Variant> variants) {
    this.variants.addAll(variants);
    return this;
  }

  public Integer getId() {
    return id;
  }

  public Product setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public Product setName(String name) {
    this.name = name;
    return this;
  }

  public String getBrand() {
    return brand;
  }

  public Product setBrand(String brand) {
    this.brand = brand;
    return this;
  }

  public static Product anyProduct() {
    return new Product().setName("any Product")
      .setBrand("any brand")
      .setId((int) (Math.random() * 100));
  }

  public ProductCategory getProductCategory() {
    return productCategory;
  }

  public Product setProductCategory(ProductCategory productCategory) {
    this.productCategory = productCategory;
    return this;
  }

  public static Product copyOf(Product product) {
    return new Product()
      .setId(product.getId())
      .setBrand(product.getBrand())
      .setName(product.getName())
      .setVariants(product.getVariants())
      .setProductCategory(product.getProductCategory());
  }

  public String getCode() {
    return code;
  }

  public Product setCode(String code) {
    this.code = code;
    return this;
  }
}
