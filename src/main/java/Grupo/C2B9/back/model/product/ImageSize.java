package Grupo.C2B9.back.model.product;

public enum ImageSize {
  MINI,
  LARGE,
  GRID
}
