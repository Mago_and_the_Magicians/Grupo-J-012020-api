package Grupo.C2B9.back.model.product;

import javax.persistence.*;

@Entity
public class Image {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @Column
  private String url;

  @Column
  private ImageSize imageSize;

  public Integer getId() {
    return id;
  }

  public Image setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getUrl() {
    return url;
  }

  public Image setUrl(String url) {
    this.url = url;
    return this;
  }

  public ImageSize getImageSize() {
    return imageSize;
  }

  public Image setImageSize(ImageSize imageSize) {
    this.imageSize = imageSize;
    return this;
  }
}
