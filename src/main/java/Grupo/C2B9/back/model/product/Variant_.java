package Grupo.C2B9.back.model.product;

import Grupo.C2B9.back.model.market.variant.MarketVariant;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Variant.class)
public class Variant_ {
  public static volatile
  ListAttribute<Variant, MarketVariant> marketVariants;
  public static volatile SingularAttribute<Variant, Integer> id;
  public static volatile SingularAttribute<Variant, String> code;
  public static volatile SingularAttribute<Variant, Product> product;
}
