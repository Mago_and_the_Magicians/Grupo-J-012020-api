package Grupo.C2B9.back.model.product;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Product.class)
public class Product_ {
  public static volatile SingularAttribute<Product, Integer> id;
  public static volatile SingularAttribute<Product, String> name;
  public static volatile SingularAttribute<Product, String> brand;
  public static volatile SingularAttribute<Product, String> code;
  public static volatile SingularAttribute<Product, ProductCategory> category;
  public static volatile ListAttribute<Product, Variant> variants;
}
