package Grupo.C2B9.back.model.product;

import Grupo.C2B9.back.model.market.variant.MarketVariant;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Variant {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column
  private String description;

  @Column
  private String imageUrl;

  @Column
  private String code;

  @ManyToOne(fetch = FetchType.EAGER)
  private Product product;

  @OneToMany(mappedBy = "variant")
  private List<MarketVariant> marketVariants = new ArrayList<>();


  public String getDescription() {
    return description;
  }

  public Variant setDescription(String description) {
    this.description = description;
    return this;
  }

  public Integer getId() {
    return id;
  }

  public Variant setId(Integer id) {
    this.id = id;
    return this;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public Variant setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  public Product getProduct() {
    return product;
  }

  public Variant setProduct(Product product) {
    this.product = product;
    return this;
  }

  public static Variant anyVariant() {
    return new Variant().setDescription("any description")
      .setId((int) (Math.random() * 100));
  }

  public List<MarketVariant> getMarketVariants() {
    return marketVariants;
  }

  public Variant setMarketVariants(List<MarketVariant> marketVariants) {
    this.marketVariants = marketVariants;
    return this;
  }

  public Variant addMarketVariant(MarketVariant marketVariant) {
    this.marketVariants.add(marketVariant);
    return this;
  }

  public String getCode() {
    return code;
  }

  public Variant setCode(String code) {
    this.code = code;
    return this;
  }
}
