package Grupo.C2B9.back.service.enums;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.repository.specs.ProductSpecs;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.Specification;

import java.util.function.Function;

public enum ProductFilterEnum {
  @JsonProperty("brand")
  BRAND(ProductSpecs::byBrand),
  @JsonProperty("category")
  CATEGORY(ProductSpecs::byCategory),
  @JsonProperty("name")
  NAME(ProductSpecs::byName),
  @JsonProperty("market")
  MARKET(ProductSpecs::byMarket),
  @JsonProperty("min_price")
  MIN_PRICE(ProductSpecs::byMinPrice),
  @JsonProperty("max_price")
  MAX_PRICE(ProductSpecs::byMaxPrice);


  private final Function<String, Specification<Product>> function;

  ProductFilterEnum(Function<String, Specification<Product>> function) {
    this.function = function;
  }

  public Specification<Product> getForString(String s) {
    return this.function.apply(s);
  }
}


