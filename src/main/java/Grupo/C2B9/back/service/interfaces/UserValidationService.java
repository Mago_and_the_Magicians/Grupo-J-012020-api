package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.user.User;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

public interface UserValidationService {

  User validateUser(String token) throws RoleNotFoundException, UnsupportedEncodingException;
}
