package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.user.User;

import java.util.List;

public interface MarketVariantService {

  MarketVariant getMarketVariant(MarketVariant marketVariant);

  MarketVariant addMarketVariant(MarketVariant marketVariant, User user);

  //List<MarketVariant> getMarketVariantsForMarket(Market market);

  List<MarketVariant> getMarketVariantsForUser(User user);

  List<MarketVariant> saveBatchWithSubstractedStock(List<MarketVariant> marketVariants);

  MarketVariant retrieveItems(MarketVariant marketVariant, Integer itemsToRetrieve);

  MarketVariant substractStock(MarketVariant marketVariant, Integer stockToSubstract);

  List<MarketVariant> getMarketVariantsForMarket(Market market);
}
