package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;

import java.util.List;

public interface LineItemService {
  LineItem getByMarketOrderAndMarketVariant(MarketOrder marketOrder, MarketVariant marketVariant);

  LineItem save(LineItem setQuantity);

  List<LineItem> getByMarketOrder(MarketOrder marketOrder);

  LineItem delete(LineItem preExisting);

  LineItem substractStock(LineItem lineItem);

  LineItem retrieveDiscountedItems(LineItem preExisting, LineItem lineItem);
}
