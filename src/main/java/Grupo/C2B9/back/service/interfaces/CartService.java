package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.user.User;

import java.util.List;

public interface CartService {

  Order save(Order order);

  Order addProductToOrder(Order order, LineItem lineItem);

  Order addUncheckedProductToOrder(Order order, LineItem lineItem);

  Order getOrderById(Integer id);

  Order validateCartByUser(User user);

  Order validateShippingByUser(User user, Order order);

  Order addLineItemsToOrder(Order order, List<LineItem> lineItems);


  Order processCartCheckout(Order order);

  Order processShippingCheckout(Order order, Order shipped);

  List<LineItem> getStockConflictedItems(Order order);

  Order getOrderDetails(Order order);

  List<Order> getOrdersForUser(User user);

  Order updateCart(List<LineItem> lineItems, Order order);

  Order processFinishCheckout(Order order);

  Order validatePaymentByOrderAndUser(User validateUser);
}
