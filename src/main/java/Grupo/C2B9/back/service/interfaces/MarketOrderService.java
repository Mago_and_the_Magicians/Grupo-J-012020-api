package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;

import java.util.List;

public interface MarketOrderService {

  List<MarketOrder> getByMarket(Market market);

  MarketOrder getByMarketAndOrder(Market market, Order order);

  MarketOrder save(MarketOrder marketOrder);

  MarketOrder saveWithShipment(MarketOrder setShipping);

  MarketOrder addProductToMarketOrder(MarketOrder marketOrder, LineItem setMarketVariant);

  MarketOrder addUncheckedProductToMarketOrder(MarketOrder marketOrder, LineItem setMarketVariant);

  List<MarketOrder> getByOrder(Order order);

  List<MarketOrder> findAll();
}
