package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;
import Grupo.C2B9.back.service.auxiliar.TurnStructure;

import java.util.List;

public interface ShippingService {
  Shipping save(Shipping shipping);

  Shipping getByMarketOrder(MarketOrder marketOrder);

  List<TurnStructure> getTurnsForMarket(Market market);
}
