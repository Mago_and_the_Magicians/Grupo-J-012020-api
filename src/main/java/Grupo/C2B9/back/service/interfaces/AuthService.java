package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.service.auxiliar.LoginData;
import Grupo.C2B9.back.service.auxiliar.SignupData;
import Grupo.C2B9.back.webservices.body.response.login.JwtResponse;

public interface AuthService {

  JwtResponse authenticate(LoginData loginData);

  JwtResponse registerUser(SignupData signupData);
}
