package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.model.user.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileUploadService {

  List<Product> UploadProducts(MultipartFile productCSV);

  List<Variant> UploadVariants(MultipartFile productCSV);

  List<MarketVariant> UploadMarketVariants(MultipartFile file, User user);
}
