package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.user.Address;
import Grupo.C2B9.back.model.user.User;

import java.util.List;

public interface AddressService {
  List<Address> getByUser(User user);

  Address saveAddress(Address address, User user);

  Address getByCode(String code);
}
