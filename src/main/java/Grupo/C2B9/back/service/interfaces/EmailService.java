package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.service.auxiliar.EmailBody;

public interface EmailService {

  String getMessage(String nombre);

  void sendEmail(EmailBody emailBody);

  void informPurchase(Order order);
}
