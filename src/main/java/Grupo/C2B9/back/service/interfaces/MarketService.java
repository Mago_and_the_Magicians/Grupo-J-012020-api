package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;
import Grupo.C2B9.back.model.user.User;

import java.util.List;

public interface MarketService {

  Market getById(Integer id);

  Market getByCode(String code, User user);

  Market getByCode(String code);

  Market save(Market market);

  Market update(Market market, User user);

  List<Market> getAllMarketsForUser(User validateUser);

  List<MarketCategory> getCategories();
}
