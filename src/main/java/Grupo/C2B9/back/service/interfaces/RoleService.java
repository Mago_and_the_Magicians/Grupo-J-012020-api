package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.user.role.Role;
import Grupo.C2B9.back.model.user.role.RoleEnum;

import java.util.Optional;

public interface RoleService {
  Optional<Role> findByName(RoleEnum roleUser);
}
