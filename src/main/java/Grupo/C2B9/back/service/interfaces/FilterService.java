package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.service.auxiliar.Filter;

import java.util.List;

public interface FilterService {

  List<Filter> getFilters(List<Product> products);
}
