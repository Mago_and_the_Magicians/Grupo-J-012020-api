package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.user.User;

public interface UserService {
  User completeUserData(User user);

  void validateUser(User user);

  User getUserById(Integer userId);

  User getUserByUsernameOrEmail(String username);

  boolean existsByUsername(String username);

  boolean existsByEmail(String email);

  User save(User user);
}
