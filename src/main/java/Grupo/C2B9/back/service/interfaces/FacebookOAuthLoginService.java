package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.webservices.body.request.oauth.FacebookOAuthRequestBody;
import Grupo.C2B9.back.webservices.body.response.login.JwtResponse;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

public interface FacebookOAuthLoginService {

  JwtResponse login(FacebookOAuthRequestBody facebookOAuthRequestBody) throws UnsupportedEncodingException;

  boolean validateToken(String oAuthToken) throws UnsupportedEncodingException;

  User getUserFromFacebook(String oAuthtoken) throws RoleNotFoundException, UnsupportedEncodingException;
}
