package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.product.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {

  List<Product> productsFor(Map<String, String> filters);

  Product saveProduct(Product product);

  Product getById(Integer id);

  Product getByCode(String code);
}
