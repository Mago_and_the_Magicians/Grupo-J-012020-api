package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.Variant;

import java.util.List;
import java.util.Map;

public interface VariantService {


  Variant getById(Integer id);

  Variant getByCode(String code);

  List<Variant> getAllVariants();

  List<Variant> getVariantsByProductId(Integer id);

  List<Variant> getVariantsByProductCode(String code);

  Variant save(Variant variant);

  List<Product> filterVariants(List<Product> productsFor, Map<String, String> filters);
}
