package Grupo.C2B9.back.service.interfaces;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.opentime.OpenTime;

import java.util.List;

public interface OpenTimeService {

  List<OpenTime> getOpenTimesForMarket(Market market);

  List<OpenTime> saveOpenTimesForMarket(Market toSave, List<OpenTime> openTimes);
}
