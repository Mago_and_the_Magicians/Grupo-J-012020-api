package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;
import Grupo.C2B9.back.model.user.Address;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.repository.OrderRepository;
import Grupo.C2B9.back.repository.specs.OrderSpecs;
import Grupo.C2B9.back.service.interfaces.*;
import Grupo.C2B9.back.throwable.badrequest.InvalidCheckoutException;
import Grupo.C2B9.back.throwable.notfound.AddressNotFoundException;
import Grupo.C2B9.back.throwable.notfound.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {


  @Autowired
  OrderRepository orderRepository;

  @Autowired
  MarketOrderService marketOrderService;

  @Autowired
  MarketService marketService;

  @Autowired
  MarketVariantService marketVariantService;

  @Autowired
  LineItemService lineItemService;

  @Autowired
  AddressService addressService;

  @Autowired
  ShippingService shippingService;

  @Autowired
  EmailService emailService;

  public CartServiceImpl(OrderRepository orderRepository) {
    this.orderRepository = orderRepository;
  }

  @Override
  public Order save(Order order) {
    return orderRepository.save(order.getCode() == null ? order.setCode(getCode()) : order);
  }

  private String getCode() {
    return "ORD" + String.format("%05d", orderRepository.getMaxId() + 1);
  }


  public Order addLineItemsToOrder(Order order, List<LineItem> lineItems) {
    Order orderToModify = getOrderById(order.getId());
    lineItems.forEach(lineItem -> addProductToOrder(order, lineItem));
    return orderToModify.setMarketOrders(marketOrderService.getByOrder(orderToModify));
  }

  @Override
  public Order processCartCheckout(Order order) {
    List<MarketOrder> marketOrders = marketOrderService.getByOrder(order);
    if (marketOrders.isEmpty()) {
      throw new InvalidCheckoutException("order is empty");
    }
    List<LineItem> lineItems = marketOrders
      .stream()
      .map(MarketOrder::getLineItems)
      .flatMap(List::stream)
      .filter(LineItem::validStock)
      .collect(Collectors.toList());
    lineItems.stream().map(lineItemService::substractStock)
      .forEach(lineItemService::save);
    if (lineItems.size() ==
      marketOrders.stream().map(MarketOrder::getLineItems).mapToLong(List::size).sum()) {
      return orderRepository.save(order.setStatus(OrderStatus.SHIPPING))
        .setMarketOrders(marketOrderService.getByOrder(order));
    }
    return order.setMarketOrders(marketOrderService.getByOrder(order));
  }


  @Override
  public Order processShippingCheckout(Order order, Order shipped) {
    List<Address> userAddresses = addressService.getByUser(order.getUser());
    List<MarketOrder> marketOrders = marketOrderService.getByOrder(order);
    List<Shipping> shippings = marketOrders
      .stream()
      .map(marketOrder -> marketOrder.setShipping(getShipping(shipped, marketOrder)))
      .map(MarketOrder::getShipping)
      .collect(Collectors.toList());
    shippings
      .stream()
      .map(shipping -> shipping.setAddress(getAddressFromCode(userAddresses, shipping.getAddress())))
      .forEach(shippingService::save);
    return orderRepository.save(getOrderByCode(order.getCode()).setStatus(OrderStatus.PAYMENT))
      .setMarketOrders(marketOrderService.getByOrder(order));
  }

  private Address getAddressFromCode(List<Address> addresses, Address address) {
    if (address == null) {
      return null;
    }
    return addresses
      .stream()
      .filter(address1 -> address.getCode().equals(address1.getCode()))
      .findFirst()
      .orElseThrow(() -> new AddressNotFoundException("address with code " + address.getCode() + "not found"));
  }

  private Shipping getShipping(Order shipped, MarketOrder marketOrder) {
    return shipped.getMarketOrders()
      .stream()
      .filter(marketOrder1 -> marketOrder.getCode().equals(marketOrder1.getCode()))
      .map(marketOrder1 -> marketOrder.setShipping(marketOrder1.getShipping().setMarketOrder(marketOrder)))
      .map(MarketOrder::getShipping)
      .findFirst().orElse(null);
  }

  @Override
  public List<LineItem> getStockConflictedItems(Order order) {
    List<MarketOrder> marketOrders = marketOrderService.getByOrder(order);
    return marketOrders
      .stream()
      .map(MarketOrder::getLineItems)
      .flatMap(List::stream)
      .filter(lineItem -> !lineItem.validStock())
      .map(lineItemService::substractStock)
      .collect(Collectors.toList());

  }

  @Override
  public Order getOrderDetails(Order order) {
    return order.setMarketOrders(marketOrderService.getByOrder(order));
  }

  @Override
  public List<Order> getOrdersForUser(User user) {
    return orderRepository.findAll(OrderSpecs.byUser(user).and(OrderSpecs.notByOrderStatus(OrderStatus.CART)))
      .stream()
      .map(order -> order.setMarketOrders(marketOrderService.getByOrder(order)))
      .collect(Collectors.toList());
  }

  @Override
  public Order updateCart(List<LineItem> lineItems, Order order) {
    lineItems.forEach(lineItem -> addUncheckedProductToOrder(order, lineItem));
    return getOrderByCode(order.getCode()).setMarketOrders(marketOrderService.getByOrder(order));
  }

  @Override
  public Order processFinishCheckout(Order order) {
    if (order.getStatus().equals(OrderStatus.PAYMENT)) {
      Order processedOrder = orderRepository.save(order.setStatus(OrderStatus.FINISHED)).setMarketOrders(marketOrderService.getByOrder(order));
      emailService.informPurchase(processedOrder);
      return processedOrder;
    }
    throw new OrderNotFoundException("Order not updated");
  }

  @Override
  public Order validatePaymentByOrderAndUser(User validateUser) {
    return orderRepository.findOne(OrderSpecs.byOrderStatus(OrderStatus.PAYMENT)
      .and(OrderSpecs.byUser(validateUser)))
      .orElseThrow(() -> new OrderNotFoundException(
        "Order for status PAYMENT and user " + validateUser.getEmail() + " not found"));
  }

  public boolean containsLineItem(List<LineItem> lineItems, LineItem lineItem) {
    return
      lineItems
        .stream()
        .anyMatch(lineItem1 -> lineItem1.getMarketVariant().getCode().equals(lineItem.getMarketVariant().getCode()));

  }


  private Order advanceCartStatus(Order order) {
    order.getMarketOrders().forEach(
      MarketOrder::validateStock
    );
    order.getMarketOrders().stream().map(
      MarketOrder::substractStock
    ).forEach(this::saveSubstractedStock);
    return orderRepository.save(order.setStatus(OrderStatus.SHIPPING))
      .setMarketOrders(marketOrderService.getByOrder(order));
  }

  private void saveSubstractedStock(MarketOrder marketOrder) {
    List<MarketVariant> marketVariants = marketOrder.getLineItems()
      .stream()
      .map(LineItem::getMarketVariant)
      .collect(Collectors.toList());
    marketVariantService.saveBatchWithSubstractedStock(marketVariants);
  }


  @Override
  public Order addProductToOrder(Order order, LineItem lineItem) {
    Order orderToModify = getOrderByCode(order.getCode());
    MarketVariant marketVariant = marketVariantService.getMarketVariant(lineItem.getMarketVariant());
    MarketOrder marketOrder = marketOrderService.getByMarketAndOrder(
      marketVariant.getMarket(),
      orderToModify);
    if (marketOrder == null && lineItem.getQuantity() > 0) {
      marketOrder = marketOrderService.save(new MarketOrder().setMarket(marketVariant.getMarket())
        .setOrder(orderToModify));
    }
    marketOrderService.addProductToMarketOrder(marketOrder, lineItem.setMarketVariant(marketVariant));
    orderRepository.save(orderToModify.setStatus(OrderStatus.CART));
    return orderToModify.setMarketOrders(marketOrderService.getByOrder(orderToModify));
  }

  @Override
  public Order addUncheckedProductToOrder(Order order, LineItem lineItem) {
    MarketVariant marketVariant = marketVariantService.getMarketVariant(lineItem.getMarketVariant());
    MarketOrder marketOrder = marketOrderService.getByMarketAndOrder(
      marketVariant.getMarket(),
      order);
    if (marketOrder == null && lineItem.getQuantity() > 0) {
      marketOrder = marketOrderService.save(new MarketOrder().setMarket(marketVariant.getMarket())
        .setOrder(order));
    }
    marketOrderService.addUncheckedProductToMarketOrder(marketOrder, lineItem.setMarketVariant(marketVariant));
    return order.setMarketOrders(marketOrderService.getByOrder(order));
  }

  private Order getOrderByCode(String code) {
    return orderRepository.findOne(OrderSpecs.byCode(code)).orElseThrow(() -> new OrderNotFoundException(
      "Order with code " + code + " not found"));
  }

  @Override
  public Order getOrderById(Integer id) {
    return orderRepository.findOne(OrderSpecs.byId(id)).orElseThrow(() -> new OrderNotFoundException(
      "Order with id " + id + " not found"));
  }

  public Order getCartByUser(User user) {
    return orderRepository.findOne(
      OrderSpecs.notByOrderStatus(OrderStatus.FINISHED).and(OrderSpecs.byUser(user))).orElse(null);
  }

  @Override
  public Order validateCartByUser(User user) {
    Order order = getCartByUser(user);
    if (order == null) {
      return orderRepository.save(new Order().setUser(user).setStatus(OrderStatus.CART).setCode(getCode()));
    }
    return order;
  }

  @Override
  public Order validateShippingByUser(User user, Order order) {
    return orderRepository.findOne(
      OrderSpecs.byOrderStatus(OrderStatus.SHIPPING).and(OrderSpecs.byUser(user)))
      .orElseThrow(() -> new OrderNotFoundException("order not found for user " + user.getEmail()));
  }
}
