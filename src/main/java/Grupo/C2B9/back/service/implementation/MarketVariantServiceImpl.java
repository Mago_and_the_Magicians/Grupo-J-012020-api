package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.repository.MarketVariantRepository;
import Grupo.C2B9.back.repository.specs.MarketVariantSpecs;
import Grupo.C2B9.back.service.interfaces.MarketService;
import Grupo.C2B9.back.service.interfaces.MarketVariantService;
import Grupo.C2B9.back.service.interfaces.VariantService;
import Grupo.C2B9.back.throwable.badrequest.InvalidMarketVariantException;
import Grupo.C2B9.back.throwable.notfound.MarketVariantNotFoundException;
import Grupo.C2B9.back.throwable.unauthorized.AccessNotAllowedException;
import Grupo.C2B9.back.throwable.unauthorized.UserValidationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MarketVariantServiceImpl implements MarketVariantService {

  private final MarketVariantRepository marketVariantRepository;
  private final VariantService variantService;


  public MarketVariantServiceImpl(
    MarketVariantRepository marketVariantRepository,
    VariantService variantService) {
    this.marketVariantRepository = marketVariantRepository;
    this.variantService = variantService;
  }

  @Override
  public MarketVariant getMarketVariant(MarketVariant marketVariant) {
    return this.marketVariantRepository.findOne(MarketVariantSpecs.byCode(validateMarketVariantCode(marketVariant)))
      .orElseThrow(() -> new MarketVariantNotFoundException(marketVariant.getCode()));
  }

  @Override
  @Transactional
  public MarketVariant addMarketVariant(MarketVariant marketVariant, User user) {
    //Market market = marketService.getByCode(marketVariant.getMarket().getCode());
    if (!marketVariant.getMarket().getUser().getId().equals(user.getId())) {
      throw new AccessNotAllowedException("you are not allowed to modify another user market");
    }
    return marketVariantRepository.save(marketVariant
      .setVariant(variantService.getByCode(marketVariant.getVariant().getCode()))
      .setMarket(marketVariant.getMarket())
      .setCode(
        "MKV" + String.format("%05d", marketVariantRepository.getMaxId() + 1)));
  }

  /*
  @Override
  public List<MarketVariant> getMarketVariantsForMarket(Market market) {
    Market market1 = validateMarket(market.getId());
    return marketVariantRepository.findAll(
      MarketVariantSpecs.byMarket(market1.getId()));
  }*/

  @Override
  public List<MarketVariant> getMarketVariantsForUser(User user) {
    if (user == null) {
      throw new UserValidationException("User is not valid");
    }
    return marketVariantRepository.findAll(
      MarketVariantSpecs.byUser(user.getId()));
  }

  @Override
  public List<MarketVariant> saveBatchWithSubstractedStock(List<MarketVariant> marketVariants) {
    return marketVariantRepository.saveAll(marketVariants);
  }

  @Override
  public MarketVariant retrieveItems(MarketVariant marketVariant, Integer itemsToRetrieve) {
    return marketVariantRepository.save(marketVariant.setStock(marketVariant.getStock() + itemsToRetrieve));
  }

  @Override
  public MarketVariant substractStock(MarketVariant marketVariant, Integer stockToSubstract) {
    return marketVariantRepository.save(marketVariant.setStock(marketVariant.getStock() - stockToSubstract));
  }

  @Override
  public List<MarketVariant> getMarketVariantsForMarket(Market market) {
    return marketVariantRepository.findAll(MarketVariantSpecs.byMarket(market));
  }


  private String validateMarketVariantCode(MarketVariant marketVariant) {
    if (marketVariant.getCode() == null) {
      throw new InvalidMarketVariantException("Market variant id is not valid");
    }
    return marketVariant.getCode();
  }
}
