package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.opentime.OpenTime;
import Grupo.C2B9.back.repository.OpenTimeRepository;
import Grupo.C2B9.back.repository.specs.OpenTimeSpecs;
import Grupo.C2B9.back.service.interfaces.OpenTimeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OpenTimeServiceImpl implements OpenTimeService {

  private final OpenTimeRepository openTimeRepository;

  public OpenTimeServiceImpl(OpenTimeRepository openTimeRepository) {
    this.openTimeRepository = openTimeRepository;
  }

  @Override
  public List<OpenTime> getOpenTimesForMarket(Market market) {
    return openTimeRepository.findAll(OpenTimeSpecs.byMarket(market));
  }

  @Override
  public List<OpenTime> saveOpenTimesForMarket(Market market, List<OpenTime> openTimes) {
    List<OpenTime> filtered = getOpenTimesForMarket(market)
      .stream()
      .filter(openTime -> openTimes.stream().anyMatch(openTime::isEqual))
      .collect(Collectors.toList());
    List<OpenTime> toDelete = getOpenTimesForMarket(market)
      .stream()
      .filter(openTime -> openTimes.stream().noneMatch(openTime::isEqual))
      .collect(Collectors.toList());
    toDelete.forEach(openTimeRepository::delete);
    filtered.forEach(openTimeRepository::save);
    return getOpenTimesForMarket(market);
  }
}
