package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.user.AuthenticationType;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.model.user.role.Role;
import Grupo.C2B9.back.model.user.role.RoleEnum;
import Grupo.C2B9.back.service.auxiliar.EmailBody;
import Grupo.C2B9.back.service.interfaces.EmailService;
import Grupo.C2B9.back.service.interfaces.FacebookOAuthLoginService;
import Grupo.C2B9.back.service.interfaces.RoleService;
import Grupo.C2B9.back.service.interfaces.UserService;
import Grupo.C2B9.back.throwable.unauthorized.AuthenticationTypeException;
import Grupo.C2B9.back.webservices.body.request.oauth.FacebookOAuthRequestBody;
import Grupo.C2B9.back.webservices.body.response.login.JwtResponse;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class FacebookOAuthLoginServiceImpl implements FacebookOAuthLoginService {

  private static final Logger LOGGER =
    LoggerFactory.getLogger(EmailServiceImpl.class);
  @Value("${guavi.facebook.client_id}")
  private String clientId;

  @Value("${guavi.facebook.client_secret}")
  private String clientSecret;

  @Value("${guavi.facebook.redirect_uri}")
  private String redirectUri;

  @Autowired
  private EmailService emailService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private UserService userService;

  @Override
  public JwtResponse login(FacebookOAuthRequestBody facebookOAuthRequestBody) throws UnsupportedEncodingException {
    String url = "https://graph.facebook.com/oauth/access_token";
    JsonElement root = new JsonParser().parse(new String("{\"access_token\":\"asd123\"}"));
    String queryString = "client_id=" + this.getClientId()
      + "&redirect_uri=" + facebookOAuthRequestBody.getRedirectUri()
      + "&client_secret=" + this.getClientSecret()
      + "&code=" + URLEncoder.encode(facebookOAuthRequestBody.getCode(), StandardCharsets.UTF_8.toString());
    root = apiCall(url, queryString, root);
    String accessToken = root.getAsJsonObject().get("access_token").getAsString();
    User user = new User();
    try {
      user = this.getUserFromFacebook(accessToken);
    } catch (Exception e) {
      LOGGER.error("Failed to retrieve user data from facebook");
    }
    LOGGER.info("User with mail " + user.getEmail() + " retrieved from facebook");
    return new JwtResponse(
      root.getAsJsonObject().get("access_token").getAsString(), user.getId(), null, user.getEmail(),
      new ArrayList<String>(Collections.singletonList("ROLE_USER")), user.getAuthenticationType().name());
  }

  private JsonElement apiCall(String url, String queryString, JsonElement root) {

    HttpClient client = new HttpClient();
    GetMethod method = new GetMethod(url);
    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
      new DefaultHttpMethodRetryHandler(3, false));
    try {
      method.setQueryString(queryString);
      int statusCode = client.executeMethod(method);
      if (statusCode != HttpStatus.SC_OK) {
        LOGGER.error("Method failed: " + method.getStatusLine());
      }
      byte[] responseBody = method.getResponseBody();
      root = new JsonParser().parse(new String(responseBody));
    } catch (HttpException e) {
      LOGGER.error("Fatal protocol violation: " + e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      LOGGER.error("Fatal transport error: " + e.getMessage());
      e.printStackTrace();
    } finally {
      method.releaseConnection();
    }
    return root;
  }

  @Override
  public boolean validateToken(String oAuthToken) throws UnsupportedEncodingException {
    String accessToken = this.getAppToken();
    String url = "https://graph.facebook.com/debug_token";
    String queryString = "input_token=" + URLEncoder.encode(oAuthToken, StandardCharsets.UTF_8.toString())
      + "&access_token=" + URLEncoder.encode(accessToken, StandardCharsets.UTF_8.toString());
    try {
      JsonElement root = new JsonParser().parse(new String("{\"access_token\":\"asd123\"}"));
      root = this.apiCall(url, queryString, root);
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  @Override
  public User getUserFromFacebook(String oAuthtoken) throws RoleNotFoundException, UnsupportedEncodingException {
    String url = "https://graph.facebook.com/me";
    JsonElement root = new JsonParser().parse(new String("{\"access_token\":\"asd123\"}"));
    String queryString = "client_id=" + this.getClientId()
      + "&fields=id,name,email"
      + "&access_token=" + URLEncoder.encode(oAuthtoken, StandardCharsets.UTF_8.toString());
    root = this.apiCall(url, queryString, root);
    String email = root.getAsJsonObject().get("email").getAsString();
    if (userService.existsByEmail(email)) {
      if (!userService.getUserByUsernameOrEmail(email).getAuthenticationType().equals(AuthenticationType.FACEBOOK)) {
        throw new AuthenticationTypeException("Facebook user can not login with user and password");
      }
      return userService.getUserByUsernameOrEmail(email);
    }
    Set<Role> roles = new HashSet<>();
    roles.add(roleService.findByName(RoleEnum.ROLE_USER)
      .orElseThrow(
        () -> new RoleNotFoundException("role user not found"))
    );
    String fullname = root.getAsJsonObject().get("name").getAsString();
    emailService.sendEmail(
      new EmailBody()
        .setEmail(email)
        .setContent(emailService.getMessage(
          fullname.substring(0,fullname.indexOf(' '))))
        .setSubject("Bienvenido a Guavi"));
    return userService.save(
      new User().setEmail(email)
        .setRoles(roles)
        .setFullname(fullname)
        .setAuthenticationType(AuthenticationType.FACEBOOK));
  }


  private String getAppToken() {
    String url = "https://graph.facebook.com/oauth/access_token";
    JsonElement root = new JsonParser().parse(new String("{\"access_token\":\"asd123\"}"));
    String queryString = "client_id=" + this.getClientId()
      + "&client_secret=" + this.getClientSecret() +
      "&grant_type=client_credentials";
    root = this.apiCall(url, queryString, root);
    return root.getAsJsonObject().get("access_token").getAsString();
  }


  public String getClientId() {
    return clientId;
  }

  public String getClientSecret() {
    return clientSecret;
  }

  public String getRedirectUri() {
    return redirectUri;
  }

  public RoleService getRoleService() {
    return roleService;
  }

  public FacebookOAuthLoginServiceImpl setRoleService(RoleService roleService) {
    this.roleService = roleService;
    return this;
  }
}

