package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.user.AuthenticationType;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.model.user.role.Role;
import Grupo.C2B9.back.model.user.role.RoleEnum;
import Grupo.C2B9.back.service.auxiliar.EmailBody;
import Grupo.C2B9.back.service.auxiliar.LoginData;
import Grupo.C2B9.back.service.auxiliar.SignupData;
import Grupo.C2B9.back.service.interfaces.AuthService;
import Grupo.C2B9.back.service.interfaces.EmailService;
import Grupo.C2B9.back.service.interfaces.RoleService;
import Grupo.C2B9.back.service.interfaces.UserService;
import Grupo.C2B9.back.service.security.UserDetailsImpl;
import Grupo.C2B9.back.throwable.conflict.DuplicateUserException;
import Grupo.C2B9.back.webservices.body.response.login.JwtResponse;
import Grupo.C2B9.back.webservices.configurations.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {

  private final AuthenticationManager authenticationManager;
  private final UserService userService;
  private final RoleService roleService;
  private final PasswordEncoder encoder;
  private final JwtUtils jwtUtils;
  private final EmailService emailService;

  public AuthServiceImpl(AuthenticationManager authenticationManager, UserService userService,
                         RoleService roleService, PasswordEncoder encoder, JwtUtils jwtUtils,
                         EmailService emailService) {
    this.authenticationManager = authenticationManager;
    this.userService = userService;
    this.roleService = roleService;
    this.encoder = encoder;
    this.jwtUtils = jwtUtils;
    this.emailService = emailService;
  }

  @Override
  public JwtResponse authenticate(LoginData loginData) {
    User userToValidate = userService.getUserByUsernameOrEmail(loginData.getUsername());
    Authentication authentication = authenticationManager.authenticate(
      new UsernamePasswordAuthenticationToken(userToValidate.getUsername(), loginData.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
      .map(GrantedAuthority::getAuthority)
      .collect(Collectors.toList());
    return new JwtResponse(jwt,
      userDetails.getId(),
      userDetails.getUsername(),
      userDetails.getEmail(),
      roles,
      userToValidate.getAuthenticationType().name());
  }

  @Override
  public JwtResponse registerUser(SignupData signupData) {

    if (userService.existsByUsername(signupData.getUsername())) {
      throw new DuplicateUserException("Username id already taken: " + signupData.getUsername());
    }

    if (userService.existsByEmail(signupData.getEmail())) {
      throw new DuplicateUserException("Email is already taken" + signupData.getEmail());
    }

    // Create new user's account
    User user = new User(signupData.getUsername(),
      signupData.getEmail(),
      encoder.encode(signupData.getPassword()));

    Set<String> strRoles = signupData.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleService.findByName(RoleEnum.ROLE_USER)
        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleService.findByName(RoleEnum.ROLE_ADMIN)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

            break;
          case "mod":
            Role modRole = roleService.findByName(RoleEnum.ROLE_MODERATOR)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(modRole);

            break;
          default:
            Role userRole = roleService.findByName(RoleEnum.ROLE_USER)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
      });
    }

    user.setRoles(roles);
    user.setAuthenticationType(AuthenticationType.LOCAL);
    userService.save(user);
    emailService.sendEmail(
      new EmailBody()
        .setEmail(user.getEmail())
        .setContent(emailService.getMessage(
          user.getUsername()))
        .setSubject("Bienvenido a Guavi"));
    return authenticate(new LoginData()
      .setUsername(signupData.getUsername())
      .setPassword(signupData.getPassword()));
  }
}
