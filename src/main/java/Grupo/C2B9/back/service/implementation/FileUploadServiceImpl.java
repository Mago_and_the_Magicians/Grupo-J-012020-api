package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.ProductCategory;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.service.interfaces.*;
import Grupo.C2B9.back.throwable.badrequest.FileFormatException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class FileUploadServiceImpl implements FileUploadService {

  private final ProductService productService;
  private final VariantService variantService;
  private final MarketVariantService marketVariantService;
  private final MarketService marketService;

  private final Pattern pattern = Pattern.compile(",");

  public FileUploadServiceImpl(ProductService productService, VariantService variantService,
                               MarketVariantService marketVariantService, MarketService marketService) {
    this.productService = productService;
    this.variantService = variantService;
    this.marketVariantService = marketVariantService;
    this.marketService = marketService;
  }


  @Override
  public List<Product> UploadProducts(MultipartFile productCSV) {
    List<Product> products;
    try (BufferedReader in = new BufferedReader(new FileReader(getFileFromMultipart(productCSV)))) {
      products = in.lines().skip(1).map(line -> {
        String[] x = pattern.split(line);
        return productService.saveProduct(new Product()
          .setName(x[0])
          .setBrand(x[1])
          .setProductCategory(ProductCategory.valueOf(x[2])));
      }).collect(Collectors.toList());
    } catch (IOException ex) {
      throw new FileFormatException("Invalid file");
    }
    return products;
  }

  @Override
  public List<Variant> UploadVariants(MultipartFile variantsCSV) {
    List<Variant> variants;
    try (BufferedReader in = new BufferedReader(new FileReader(getFileFromMultipart(variantsCSV)))) {
      variants = in.lines().skip(1).map(line -> {
        String[] x = pattern.split(line);
        return variantService.save(new Variant()
          .setProduct(productService.getByCode(x[0]))
          .setDescription(x[1])
          .setImageUrl(x[2]));
      }).collect(Collectors.toList());
    } catch (IOException ex) {
      throw new FileFormatException("Invalid file");
    }
    return variants;
  }

  @Override
  public List<MarketVariant> UploadMarketVariants(MultipartFile marketVariantCSV, User user) {
    List<MarketVariant> marketVariants;
    try (BufferedReader in = new BufferedReader(new FileReader(getFileFromMultipart(marketVariantCSV)))) {
      marketVariants = in.lines().skip(1).map(line -> {
        String[] x = pattern.split(line);
        return marketVariantService.addMarketVariant(new MarketVariant()
          .setVariant(variantService.getByCode(x[0]))
          .setMarket(marketService.getByCode(x[1]))
          .setPrice(Double.parseDouble(x[2]))
          .setStock(Integer.parseInt(x[3]))
          , user);
      }).collect(Collectors.toList());
    } catch (IOException ex) {
      throw new FileFormatException("Invalid file");
    }
    return marketVariants;
  }


  private File getFileFromMultipart(MultipartFile csv) {
    File convFile = new File(Objects.requireNonNull(csv.getOriginalFilename()));
    try {
      convFile.createNewFile();
      FileOutputStream fos = new FileOutputStream(convFile);
      fos.write(csv.getBytes());
      fos.close();
    } catch (IOException ex) {
      throw new FileFormatException("Invalid file");
    }
    return convFile;

  }
}
