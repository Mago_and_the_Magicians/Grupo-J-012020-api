package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.repository.ProductRepository;
import Grupo.C2B9.back.repository.specs.ProductSpecs;
import Grupo.C2B9.back.service.enums.ProductFilterEnum;
import Grupo.C2B9.back.service.interfaces.ProductService;
import Grupo.C2B9.back.throwable.conflict.DuplicateProductException;
import Grupo.C2B9.back.throwable.notfound.ProductNotFoundException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {


  private final ProductRepository productRepository;

  public ProductServiceImpl(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }


  public List<Product> productsFor(Map<String, String> filters) {
    if (filters.isEmpty()) {
      return productRepository.findAll();
    }
    List<ProductFilterEnum> keys = new ArrayList<String>(filters.keySet())
      .stream()
      .map(s -> ProductFilterEnum.valueOf(s.toUpperCase()))
      .collect(Collectors.toList());
    return productRepository.findAll(getSpecsForProduct(filters));
  }

  private Specification<Product> getSpecsForProduct(Map<String, String> filters) {
    Map<ProductFilterEnum, String> updatedFilters = new HashMap<>();
    filters.forEach((key, value) -> updatedFilters.put(ProductFilterEnum.valueOf(key.toUpperCase()), value));
    List<Specification<Product>> specs =
      updatedFilters.entrySet().stream().map(kv -> kv.getKey().getForString(kv.getValue())).collect(Collectors.toList());
    Specification<Product> returnedSpec = ProductSpecs.active();
    for (Specification<Product> spec : specs) {
      returnedSpec = returnedSpec.and(spec);
    }
    return returnedSpec;
  }

  public Product saveProduct(Product product) {
    if (!productRepository.findAll(ProductSpecs.byName(product.getName())).isEmpty()) {
      throw new DuplicateProductException("Product with name " + product.getName() + " already exists");
    }
    return productRepository.save(product
      .setCode("PRD" + String.format("%05d", productRepository.getMaxId() + 1)));

  }

  @Override
  public Product getById(Integer id) {
    return productRepository.findById(id)
      .orElseThrow(() -> new ProductNotFoundException("Product with id " + id + " not found"));
  }

  @Override
  public Product getByCode(String code) {
    return productRepository.findOne(ProductSpecs.byCode(code))
      .orElseThrow(() -> new ProductNotFoundException("Product with code " + code + " not found"));
  }
}
