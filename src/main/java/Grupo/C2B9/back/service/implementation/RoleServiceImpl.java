package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.user.role.Role;
import Grupo.C2B9.back.model.user.role.RoleEnum;
import Grupo.C2B9.back.repository.RoleRepository;
import Grupo.C2B9.back.service.interfaces.RoleService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

  private final RoleRepository roleRepository;

  public RoleServiceImpl(RoleRepository roleRepository) {
    this.roleRepository = roleRepository;
  }

  @Override
  public Optional<Role> findByName(RoleEnum roleUser) {
    return roleRepository.findByName(roleUser);
  }
}
