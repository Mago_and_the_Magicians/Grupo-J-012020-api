package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.repository.LineItemRepository;
import Grupo.C2B9.back.repository.specs.LineItemSpecs;
import Grupo.C2B9.back.service.interfaces.LineItemService;
import Grupo.C2B9.back.service.interfaces.MarketVariantService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LineItemServiceImpl implements LineItemService {

  private final LineItemRepository lineItemRepository;
  private final MarketVariantService marketVariantService;

  public LineItemServiceImpl(LineItemRepository lineItemRepository, MarketVariantService marketVariantService) {
    this.lineItemRepository = lineItemRepository;
    this.marketVariantService = marketVariantService;
  }

  @Override
  public LineItem getByMarketOrderAndMarketVariant(MarketOrder marketOrder, MarketVariant marketVariant) {
    return lineItemRepository.findOne(
      LineItemSpecs.byMarketOrder(marketOrder).and(
        LineItemSpecs.byMarketVariant(marketVariant))).orElse(
      null
    );
  }

  @Override
  public LineItem save(LineItem lineItem) {
    return lineItemRepository.save(lineItem);
  }

  @Override
  public List<LineItem> getByMarketOrder(MarketOrder marketOrder) {
    return lineItemRepository.findAll(
      LineItemSpecs.byMarketOrder(marketOrder));
  }


  @Override
  public LineItem delete(LineItem preExisting) {
    lineItemRepository.delete(preExisting);
    return preExisting;
  }

  @Override
  public LineItem substractStock(LineItem lineItem) {
    Integer discountedQuantity = lineItem.getDiscountedQuantity() + lineItem.getStockToSubstract();
    marketVariantService.substractStock(lineItem.getMarketVariant(), lineItem.getStockToSubstract());
    lineItemRepository.save(lineItem.setDiscountedQuantity(discountedQuantity));
    return getByMarketOrderAndMarketVariant(lineItem.getMarketOrder(), lineItem.getMarketVariant());
  }

  @Override
  public LineItem retrieveDiscountedItems(LineItem preExisting, LineItem lineItem) {
    marketVariantService.retrieveItems(
      preExisting.getMarketVariant(),
      preExisting.getDiscountedQuantity() - preExisting.getQuantity());
    return getByMarketOrderAndMarketVariant(lineItem.getMarketOrder(), lineItem.getMarketVariant());
  }
}
