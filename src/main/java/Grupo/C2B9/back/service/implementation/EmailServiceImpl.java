package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.service.auxiliar.EmailBody;
import Grupo.C2B9.back.service.interfaces.EmailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {
  private static final Logger LOGGER = LogManager.getLogger(EmailServiceImpl.class);

  @Value("${spring.mail.username}")
  private String from;

  @Autowired
  private JavaMailSender sender;

  @Override
  public String getMessage(String nombre) {
    return "Bienvenido a Guavi " + nombre + ".\n" +
      "Su cuenta ha sido creada con éxito. \n" +
      "puede dirigirse a https://grupo-j-012020-web.herokuapp.com/ para " +
      "empezar a comprar y vender";
  }

  @Override
  @Async
  public void sendEmail(EmailBody emailBody) {
    LOGGER.info("EmailBody: {}", emailBody.toString());
    sendEmailTool(emailBody.getContent(), emailBody.getEmail(), emailBody.getSubject());
  }

  @Override
  @Async
  public void informPurchase(Order order) {
    StringBuilder informPurchaseBody = new StringBuilder("Guavi App le informa acerca de su compra: \n \n");
    order.getMarketOrders().forEach(
      marketOrder -> informPurchaseBody.append(getMarketOrderData(marketOrder))
    );
    sendEmailTool(
      informPurchaseBody.toString(),order.getUser().getEmail(),"Informe de compra numero: " + order.getCode());
  }

  private String getMarketOrderData(MarketOrder marketOrder) {
    StringBuilder informMarketOrderBody = new StringBuilder("Datos de su compra en " + marketOrder.getMarket().getName());
    informMarketOrderBody.append("Producto \t \t Precio unitario \t Total \t Cantidad");
    marketOrder.getLineItems().forEach(
      lineItem -> informMarketOrderBody.append(getLineItemData(lineItem))
    );
    return informMarketOrderBody.append(" \n ------------------------------ \n \n").toString();
  }

  private String getLineItemData(LineItem lineItem) {
    return lineItem.getProductName() +
      " " +
      lineItem.getVariantName() +
      "\t" +
      "\t" +
      lineItem.getMarketVariant().getPrice() +
      "\t" +
      lineItem.getPrice() +
      "\t" +
      lineItem.getQuantity();
  }


  private void sendEmailTool(String textMessage, String email, String subject) {
    MimeMessage message = sender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message);
    try {
      helper.setTo(email);
      helper.setFrom(from, "Guavi App");
      helper.setText(textMessage, true);
      helper.setSubject(subject);
      sender.send(message);
      LOGGER.info("Mail enviado!");
    } catch (Exception e) {
      LOGGER.error("Hubo un error al enviar el mail: {}", e);
    }
  }



}