package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.service.auxiliar.Filter;
import Grupo.C2B9.back.service.interfaces.FilterService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FilterServiceImpl implements FilterService {


  @Override
  public List<Filter> getFilters(List<Product> products) {
    if (products.size() <= 1) {
      return new ArrayList<>();
    }
    Filter brandFilter = new Filter().setName("Brand");
    Filter categoryFilter = new Filter().setName("Category").setOptions(new ArrayList<>());
    products.forEach(
      product -> {
        if (!brandFilter.hasOption(product.getBrand())) {
          brandFilter.addOption(product.getBrand());
        }
        if (product.getProductCategory() != null && !categoryFilter.hasOption(product.getProductCategory().toString())) {
          categoryFilter.addOption(product.getProductCategory().toString());
        }
      });
    return addFilterIfValid(brandFilter, addFilterIfValid(categoryFilter,
      new ArrayList<>()));
  }

  private List<Filter> addFilterIfValid(Filter filter, List<Filter> returned) {
    List<Filter> toReturn = new ArrayList<>(returned);
    if (filter.getOptions().size() > 1) {
      toReturn.add(filter);
    }
    return toReturn;
  }

}
