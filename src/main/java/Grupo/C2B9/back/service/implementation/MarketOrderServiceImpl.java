package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.repository.MarketOrderRepository;
import Grupo.C2B9.back.repository.specs.MarketOrderSpecs;
import Grupo.C2B9.back.service.interfaces.AddressService;
import Grupo.C2B9.back.service.interfaces.LineItemService;
import Grupo.C2B9.back.service.interfaces.MarketOrderService;
import Grupo.C2B9.back.service.interfaces.ShippingService;
import Grupo.C2B9.back.throwable.badrequest.InvalidQuantityException;
import Grupo.C2B9.back.throwable.conflict.DuplicateMarketOrderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MarketOrderServiceImpl implements MarketOrderService {

  @Autowired
  MarketOrderRepository marketOrderRepository;

  @Autowired
  LineItemService lineItemService;

  @Autowired
  ShippingService shippingService;

  @Autowired
  AddressService addressService;


  @Override
  public List<MarketOrder> getByMarket(Market market) {
    return marketOrderRepository.findAll(MarketOrderSpecs.byMarket(market))
      .stream()
      .filter(marketOrder -> marketOrder.getOrder().getStatus() != OrderStatus.CART)
      .map(marketOrder -> marketOrder.setLineItems(lineItemService.getByMarketOrder(marketOrder)))
      .collect(Collectors.toList());
  }

  @Override
  public MarketOrder getByMarketAndOrder(Market market, Order order) {
    return marketOrderRepository.findOne(MarketOrderSpecs.byMarket(market).and(MarketOrderSpecs.byOrder(order)))
      .orElse(null);
  }

  @Override
  public MarketOrder save(MarketOrder marketOrder) {
    if (getByMarketAndOrder(marketOrder.getMarket(), marketOrder.getOrder()) != null) {
      throw new DuplicateMarketOrderException(
        "Market order with market "
          + marketOrder.getMarket().getCode()
          + " and order "
          + marketOrder.getOrder().getCode()
          + " already exists.");
    }

    return marketOrderRepository.save(marketOrder.setCode(
      "MKO" + String.format("%05d", marketOrderRepository.getMaxId() + 1)));
  }

  @Override
  public MarketOrder saveWithShipment(MarketOrder marketOrder) {
    return marketOrder
      .setShipping(shippingService.save(marketOrder.getShipping().setMarketOrder(marketOrder)))
      .setLineItems(lineItemService.getByMarketOrder(marketOrder));
  }

  @Override
  public MarketOrder addProductToMarketOrder(MarketOrder marketOrder, LineItem lineItem) {
    LineItem preExisting = lineItemService.getByMarketOrderAndMarketVariant(marketOrder, lineItem.getMarketVariant());
    if (lineItem.getQuantity() > 0) {
      return processPositiveQuantity(marketOrder, preExisting, lineItem);
    }
    if (lineItem.getQuantity() == 0) {
      return processZeroQuantity(marketOrder, preExisting, lineItem);
    }
    throw new InvalidQuantityException("Quantity " + lineItem.getQuantity() + " is not valid");
  }

  @Override
  public MarketOrder addUncheckedProductToMarketOrder(MarketOrder marketOrder, LineItem lineItem) {
    LineItem preExisting = lineItemService.getByMarketOrderAndMarketVariant(marketOrder, lineItem.getMarketVariant());
    if (lineItem.getQuantity() > 0) {
      if (preExisting != null) {
        lineItemService.save(preExisting
          .setQuantity(Integer.max(preExisting.getQuantity(),lineItem.getQuantity())));
      } else {
        lineItemService.save(lineItem.setMarketOrder(marketOrder)
          .setMarketVariant(lineItem.getMarketVariant()));
        return marketOrderRepository.getById(marketOrder.getId());
      }
    }
    if (lineItem.getQuantity() == 0) {
      return processZeroQuantity(marketOrder, preExisting, lineItem);
    }
    return null;
  }

  private MarketOrder processZeroQuantity(MarketOrder marketOrder, LineItem preExisting, LineItem lineItem) {
    if (preExisting == null) {
      return marketOrderRepository.getById(marketOrder.getId());
    }
    lineItemService.delete(preExisting);
    if (lineItemService.getByMarketOrder(marketOrder).isEmpty()) {
      marketOrderRepository.delete(marketOrder);
      return null;
    }
    return marketOrder;

  }

  private MarketOrder processPositiveQuantity(MarketOrder marketOrder, LineItem preExisting, LineItem lineItem) {
    if (preExisting != null) {
      if (preExisting.getDiscountedQuantity() > lineItem.getQuantity()) {
        lineItemService.retrieveDiscountedItems(preExisting, lineItem);
      }
      if( lineItem.getQuantity() > preExisting.getDiscountedQuantity() + preExisting.getStock()) {
        throw new InvalidQuantityException(lineItem.getQuantity() + " is greater than avaiable stock for this product: " + (preExisting.getDiscountedQuantity() + preExisting.getStock()));
      }
      lineItemService.save(preExisting.setQuantity(Integer.min(
        preExisting.getStock() + preExisting.getDiscountedQuantity(),
        lineItem.getQuantity())));
    } else {
      if (lineItem.getStock() < lineItem.getQuantity()) {
        throw new InvalidQuantityException(lineItem.getQuantity() + " is greater than avaiable stock for this product: " + lineItem.getStock());
      }
      lineItemService.save(
        new LineItem()
          .setMarketOrder(marketOrder)
          .setMarketVariant(lineItem.getMarketVariant())
          .setQuantity(
            Integer.min(lineItem.getQuantity(), lineItem.getStock())
          ));
    }
    return marketOrderRepository.getById(marketOrder.getId());
  }

  @Override
  public List<MarketOrder> getByOrder(Order order) {
    return marketOrderRepository.findAll(MarketOrderSpecs.byOrder(order))
      .stream()
      .map(marketOrder -> marketOrder.setLineItems(lineItemService.getByMarketOrder(marketOrder)))
      .collect(Collectors.toList());
  }

  @Override
  public List<MarketOrder> findAll() {
    return marketOrderRepository.findAll();
  }
}
