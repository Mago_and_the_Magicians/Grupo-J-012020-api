package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.repository.VariantRepository;
import Grupo.C2B9.back.repository.specs.VariantSpecs;
import Grupo.C2B9.back.service.interfaces.ProductService;
import Grupo.C2B9.back.service.interfaces.VariantService;
import Grupo.C2B9.back.throwable.notfound.VariantNotFoundException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class VariantServiceImpl implements VariantService {

  private final VariantRepository variantRepository;
  private final ProductService productService;

  public VariantServiceImpl(VariantRepository variantRepository, ProductService productService) {
    this.variantRepository = variantRepository;
    this.productService = productService;
  }


  @Override
  public Variant getById(Integer id) {
    return variantRepository.findById(id)
      .orElseThrow(() -> new VariantNotFoundException("Variant with id " + id + " not found"));
  }

  @Override
  public Variant getByCode(String code) {
    return variantRepository.findOne(VariantSpecs.byCode(code))
      .orElseThrow(() -> new VariantNotFoundException("Variant with code " + code + " not found"));
  }

  @Override
  public List<Variant> getAllVariants() {
    return variantRepository.findAll();
  }

  @Override
  public List<Variant> getVariantsByProductId(Integer id) {
    return variantRepository.findAll(VariantSpecs.byProduct(productService.getById(id)));
  }

  @Override
  public List<Variant> getVariantsByProductCode(String code) {
    return variantRepository.findAll(VariantSpecs.byProduct(productService.getByCode(code)));
  }

  @Override
  public Variant save(Variant variant) {
    return variantRepository.save(
      variant
        .setProduct(
          productService.getByCode(variant.getProduct().getCode()))
        .setCode("VRN" + String.format("%05d", variantRepository.getMaxId() + 1)));
  }

  @Override
  public List<Product> filterVariants(List<Product> products, Map<String, String> filters) {
    if (filters.entrySet().stream().noneMatch(kv -> kv.getKey().equals("market"))) {
      return products;
    }
    List<Specification<Variant>> vars =
      filters.entrySet().stream().filter(
        kv -> kv.getKey().equals("market")).map(
        kv -> VariantSpecs.byMarket(
          kv.getValue())).collect(Collectors.toList());
    Specification<Variant> returnedSpec = VariantSpecs.active();
    for (Specification<Variant> spec : vars) {
      returnedSpec = returnedSpec.and(spec);
    }
    List<Product> productList = new ArrayList<>();
    Specification<Variant> finalReturnedSpec = returnedSpec;
    products.forEach(product -> productList.add(getProductFiltered(product, finalReturnedSpec)));
    return productList;
  }


  Product getProductFiltered(Product product, Specification<Variant> filter) {
    return Product.copyOf(product).setVariants(variantRepository.findAll(filter.and(VariantSpecs.byProduct(product))));
  }
}
