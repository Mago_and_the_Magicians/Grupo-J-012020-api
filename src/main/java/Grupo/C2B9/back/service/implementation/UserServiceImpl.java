package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.repository.UserRepository;
import Grupo.C2B9.back.service.interfaces.UserService;
import Grupo.C2B9.back.throwable.badrequest.EmptyIdException;
import Grupo.C2B9.back.throwable.conflict.DuplicateUserException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public User completeUserData(User user) {
    User userDB = getUserById(user.getId());
    return user.setUsername(userDB.getUsername());
  }

  @Override
  public void validateUser(User user) {
    validateId(user.getId());
  }

  @Override
  public User getUserById(Integer userId) {
    return userRepository.getById(validateId(userId));
  }

  private Integer validateId(Integer id) {
    if (id == null) throw new EmptyIdException("Unexistent id");
    return id;
  }

  @Override
  public User getUserByUsernameOrEmail(String username) {
    if (userRepository.existsByEmail(username)) {
      return userRepository.findByEmail(
        username)
        .orElseThrow(() -> new UsernameNotFoundException(username));
    }
    return userRepository.findByUsername(username)
      .orElseThrow(() -> new UsernameNotFoundException(username));

  }

  @Override
  public boolean existsByUsername(String username) {
    return userRepository.existsByUsername(username);
  }

  @Override
  public boolean existsByEmail(String email) {
    return userRepository.existsByEmail(email);
  }

  @Override
  public User save(User user) {
    if (existsByEmail(user.getEmail()) || existsByUsername(user.getUsername())) {
      throw new DuplicateUserException("User already exists by username or name");
    }
    return userRepository.save(user);
  }
}
