package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.MarketCategory;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.repository.MarketRepository;
import Grupo.C2B9.back.repository.specs.MarketSpecs;
import Grupo.C2B9.back.service.interfaces.MarketOrderService;
import Grupo.C2B9.back.service.interfaces.MarketService;
import Grupo.C2B9.back.service.interfaces.OpenTimeService;
import Grupo.C2B9.back.throwable.notfound.MarketNotFoundException;
import Grupo.C2B9.back.throwable.unauthorized.AccessNotAllowedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MarketServiceImpl implements MarketService {

  private final MarketRepository marketRepository;
  private final MarketOrderService marketOrderService;
  private final OpenTimeService openTimeService;

  public MarketServiceImpl(MarketRepository marketRepository, MarketOrderService marketOrderService,
                           OpenTimeService openTimeService) {
    this.marketRepository = marketRepository;
    this.marketOrderService = marketOrderService;
    this.openTimeService = openTimeService;
  }

  @Override
  @Transactional(readOnly = true)
  public Market getById(Integer id) {
    return this.marketRepository.findById(id)
      .orElseThrow(() -> new MarketNotFoundException("Market with id " + id + " not found"));
  }

  @Override
  public Market getByCode(String code, User user) {
    Market market = this.marketRepository.findOne(MarketSpecs.byUser(user).and(MarketSpecs.byCode(code)))
      .orElseThrow(() -> new MarketNotFoundException("Market with code " + code + " not found"));
    return market.setMarketOrders(marketOrderService.getByMarket(market));
  }

  @Override
  public Market getByCode(String code) {
    Market market = this.marketRepository.findOne(MarketSpecs.byCode(code))
      .orElseThrow(() -> new MarketNotFoundException("Market with code " + code + " not found"));
    return market.setMarketOrders(marketOrderService.getByMarket(market));
  }

  @Override
  @Transactional
  public Market save(Market market) {
    Market savedStore = marketRepository.save(
      market.setCode("MKT" + String.format("%05d", marketRepository.getMaxId() + 1)));
    return savedStore.setOpenTimes(
      openTimeService.saveOpenTimesForMarket(savedStore, market.getOpenTimes()));
  }

  @Override
  @Transactional
  public Market update(Market market, User user) {
    Market toSave = getById(market.getId());
    if (!toSave.getUser().equals(user)) {
      throw new AccessNotAllowedException("User " + user.getId() + " does not have access to that market");
    }
    return save(market).setOpenTimes(openTimeService.saveOpenTimesForMarket(toSave, market.getOpenTimes()));
  }

  @Override
  public List<Market> getAllMarketsForUser(User user) {
    return this.marketRepository
      .findAll(MarketSpecs.byUser(user))
      .stream()
      .map(market -> market.setOpenTimes(openTimeService.getOpenTimesForMarket(market)))
      .collect(Collectors.toList());
  }

  @Override
  public List<MarketCategory> getCategories() {
    return Arrays.stream(MarketCategory.values()).collect(Collectors.toList());
  }


}
