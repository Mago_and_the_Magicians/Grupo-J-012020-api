package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.service.interfaces.FacebookOAuthLoginService;
import Grupo.C2B9.back.service.interfaces.UserService;
import Grupo.C2B9.back.service.interfaces.UserValidationService;
import Grupo.C2B9.back.webservices.configurations.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.management.relation.RoleNotFoundException;
import java.io.UnsupportedEncodingException;

@Service
public class UserValidationServiceImpl implements UserValidationService {

  @Autowired
  private FacebookOAuthLoginService facebookOAuthLoginService;

  @Autowired
  JwtUtils jwtUtils;

  @Autowired
  UserService userService;

  @Override
  public User validateUser(String token) throws RoleNotFoundException, UnsupportedEncodingException {
    if (token == null) {
      return null;
    }
    String headerAuth = token;
    if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
      headerAuth = headerAuth.substring(7, headerAuth.length());
    }
    String name = jwtUtils.validateJwtToken(headerAuth) ?
      jwtUtils.getUserNameFromJwtToken(headerAuth) :
      facebookOAuthLoginService.getUserFromFacebook(headerAuth).getEmail();
    return userService.getUserByUsernameOrEmail(name);
  }
}
