package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.opentime.OpenTime;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;
import Grupo.C2B9.back.repository.ShippingRepository;
import Grupo.C2B9.back.repository.specs.ShippingSpecs;
import Grupo.C2B9.back.service.auxiliar.TurnStructure;
import Grupo.C2B9.back.service.interfaces.ShippingService;
import Grupo.C2B9.back.throwable.notfound.ShippingNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShippingServiceImpl implements ShippingService {

  @Autowired
  ShippingRepository shippingRepository;

  @Override
  public Shipping save(Shipping shipping) {
    return shippingRepository.save(shipping.setAmount(shipping.getType().getAmount()));
  }

  public Shipping getByMarketOrder(MarketOrder marketOrder) {
    return shippingRepository.findOne(ShippingSpecs.byMarketOrder(marketOrder))
      .orElseThrow(() -> new ShippingNotFoundException(
        "Shipping was not found for market order with code " + marketOrder.getCode()));
  }


  @Override
  public List<TurnStructure> getTurnsForMarket(Market market) {

    List<OpenTime> days = market.getOpenTimes();
    List<Shipping> occupied = getOccupiedTurnsForMarket(market);
    return days.stream().map(openTime -> fromOpenTime(openTime, occupied)).collect(ArrayList::new, ArrayList::addAll,
      ArrayList::addAll);
  }

  private List<Shipping> getOccupiedTurnsForMarket(Market market) {
    return shippingRepository.findAll(ShippingSpecs.byMarket(market));
  }

  private List<TurnStructure> fromOpenTime(OpenTime openTime, List<Shipping> occupied) {
    return openTime.getWorkDays()
      .stream()
      .map(
        dayOfWeek -> {
          LocalDate date = calcNextDay(dayOfWeek);
          return new TurnStructure()
            .setDay(dayOfWeek)
            .setDate(date)
            .setTurns(generateTurnsByFifteenMinutes(date, openTime.getStart(), openTime.getEnd(), occupied));
        })
      .collect(Collectors.toList());

  }

  private LocalDate calcNextDay(DayOfWeek dayOfWeek) {
    return LocalDate.now().with(TemporalAdjusters.next(dayOfWeek));
  }

  private List<LocalTime> generateTurnsByFifteenMinutes(
    LocalDate date, LocalTime start, LocalTime end, List<Shipping> occupied) {
    LocalTime lt = LocalTime.from(start);
    List<LocalTime> returningLocalTimes = new ArrayList<>();
    while (lt.isBefore(end)) {
      LocalDateTime comparingDateTime = date.atStartOfDay().with(lt);
      if (occupied.stream().noneMatch(shipping -> shipping.getTurnDate().isEqual(comparingDateTime))) {
        returningLocalTimes.add(LocalTime.from(lt));
      }
      lt = lt.plusMinutes(15);
    }
    return returningLocalTimes;
  }
}
