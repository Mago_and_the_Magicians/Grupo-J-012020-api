package Grupo.C2B9.back.service.implementation;

import Grupo.C2B9.back.model.user.Address;
import Grupo.C2B9.back.model.user.User;
import Grupo.C2B9.back.repository.AddressRepository;
import Grupo.C2B9.back.repository.specs.AddressSpecs;
import Grupo.C2B9.back.service.interfaces.AddressService;
import Grupo.C2B9.back.service.interfaces.UserService;
import Grupo.C2B9.back.throwable.conflict.DuplicateAddressException;
import Grupo.C2B9.back.throwable.notfound.AddressNotFoundException;
import Grupo.C2B9.back.throwable.notfound.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

  private final AddressRepository addressRepository;
  private final UserService userService;

  public AddressServiceImpl(AddressRepository addressRepository, UserService userService) {
    this.addressRepository = addressRepository;
    this.userService = userService;
  }

  @Override
  public List<Address> getByUser(User user) {
    return addressRepository.findAll(AddressSpecs.byUser(user));
  }

  @Override
  public Address saveAddress(Address address, User user) {
    if (user == null) {
      throw new UserNotFoundException("User could not be found");
    }
    validateAddressForUser(address, user);
    return addressRepository.save(address.setUser(user)
      .setCode("ADD" + String.format("%05d", addressRepository.getMaxId() + 1)));
  }

  @Override
  public Address getByCode(String code) {
    return addressRepository.findOne(AddressSpecs.byCode(code))
      //.orElse(null);//
      .orElseThrow(() -> new AddressNotFoundException("Address with code " + code + " not found"));
  }

  private void validateAddressForUser(Address address, User user) {
    List<Address> addresses = addressRepository.findAll(AddressSpecs.byUser(user));
    addresses.forEach(address1 -> {
      if (address1.equalAddress(address)) {
        throw new DuplicateAddressException("Address " + address.toString() + "already exists");
      }
    });
  }
}
