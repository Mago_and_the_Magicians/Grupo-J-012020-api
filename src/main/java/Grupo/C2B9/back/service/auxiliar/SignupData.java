package Grupo.C2B9.back.service.auxiliar;

import java.util.Set;

public class SignupData {

  private String username;
  private String email;
  private String password;
  private Set<String> role;

  public String getUsername() {
    return username;
  }

  public SignupData setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public SignupData setEmail(String email) {
    this.email = email;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public SignupData setPassword(String password) {
    this.password = password;
    return this;
  }

  public Set<String> getRole() {
    return role;
  }

  public SignupData setRole(Set<String> role) {
    this.role = role;
    return this;
  }
}
