package Grupo.C2B9.back.service.auxiliar;

public class LoginData {

  private String username;
  private String password;

  public String getUsername() {
    return username;
  }

  public LoginData setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public LoginData setPassword(String password) {
    this.password = password;
    return this;
  }
}
