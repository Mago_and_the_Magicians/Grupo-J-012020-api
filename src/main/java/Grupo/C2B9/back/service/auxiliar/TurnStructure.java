package Grupo.C2B9.back.service.auxiliar;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class TurnStructure {

  private DayOfWeek day;
  private LocalDate date;
  @JsonFormat(pattern = "HH:mm")
  private List<LocalTime> turns;


  public LocalDate getDate() {
    return date;
  }

  public TurnStructure setDate(LocalDate date) {
    this.date = date;
    return this;
  }

  public List<LocalTime> getTurns() {
    return turns;
  }

  public TurnStructure setTurns(List<LocalTime> turns) {
    this.turns = turns;
    return this;
  }

  public DayOfWeek getDay() {
    return day;
  }

  public TurnStructure setDay(DayOfWeek day) {
    this.day = day;
    return this;
  }
}
