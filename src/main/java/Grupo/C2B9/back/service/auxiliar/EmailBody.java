package Grupo.C2B9.back.service.auxiliar;

public class EmailBody {

  private String content;
  private String email;
  private String subject;

  public String getContent() {
    return content;
  }

  public EmailBody setContent(String content) {
    this.content = content;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public EmailBody setEmail(String email) {
    this.email = email;
    return this;
  }

  public String getSubject() {
    return subject;
  }

  public EmailBody setSubject(String subject) {
    this.subject = subject;
    return this;
  }
}
