package Grupo.C2B9.back.service.auxiliar;

import java.util.ArrayList;
import java.util.List;

public class Filter {

  private String name;

  private List<String> options;

  public String getName() {
    return name;
  }

  public Filter setName(String name) {
    this.name = name;
    return this;
  }

  public List<String> getOptions() {
    return options;
  }

  public Filter setOptions(List<String> options) {
    this.options = options;
    return this;
  }

  public boolean hasOption(String option) {
    if (this.options == null) {
      return false;
    }
    return this.options.contains(option);
  }

  public void addOption(String option) {
    if (this.options == null) {
      this.options = new ArrayList<>();
    }
    this.options.add(option);
  }
}
