package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.user.role.Role;
import Grupo.C2B9.back.model.user.role.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
  Optional<Role> findByName(RoleEnum name);
}
