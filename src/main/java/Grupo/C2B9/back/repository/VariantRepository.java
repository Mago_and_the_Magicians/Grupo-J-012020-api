package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.product.Variant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariantRepository extends JpaRepository<Variant, Integer>, JpaSpecificationExecutor<Variant> {


  List<Variant> getAllByProductId(Integer id);

  @Query("select coalesce(max(id),0) from Variant ")
  Integer getMaxId();
}
