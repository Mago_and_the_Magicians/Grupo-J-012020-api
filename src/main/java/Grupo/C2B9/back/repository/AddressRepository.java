package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.user.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaSpecificationExecutor<Address>, JpaRepository<Address, Integer> {

  @Query("select coalesce(max(id),0) from MarketOrder ")
  Integer getMaxId();
}
