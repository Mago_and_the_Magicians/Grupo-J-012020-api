package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.market.opentime.OpenTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OpenTimeRepository extends JpaRepository<OpenTime, Integer>, JpaSpecificationExecutor<OpenTime> {
}
