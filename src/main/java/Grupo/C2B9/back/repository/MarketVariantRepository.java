package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketVariantRepository extends
  JpaRepository<MarketVariant, Integer>, JpaSpecificationExecutor<MarketVariant> {

  MarketVariant getById(Integer id);


  @Query("select coalesce(max(id),0) from MarketVariant ")
  Integer getMaxId();

}
