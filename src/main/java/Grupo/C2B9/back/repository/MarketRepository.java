package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.market.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketRepository extends
  JpaRepository<Market, Integer>, JpaSpecificationExecutor<Market> {

  @Query("select coalesce(max(id),0) from Market ")
  Integer getMaxId();
}
