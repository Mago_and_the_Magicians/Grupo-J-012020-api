package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.order.lineItem.LineItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LineItemRepository extends JpaRepository<LineItem, Integer>, JpaSpecificationExecutor<LineItem> {
}
