package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ShippingRepository extends JpaSpecificationExecutor<Shipping>, JpaRepository<Shipping,Integer> {
}
