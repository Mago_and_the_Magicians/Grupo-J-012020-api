package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository
  extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

  Product getById(Integer id);

  @Query("select coalesce(max(id),0) from Product ")
  Integer getMaxId();
}
