package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketOrderRepository extends JpaRepository<MarketOrder, Integer>,
  JpaSpecificationExecutor<MarketOrder> {

  MarketOrder getById(Integer id);

  @Query("select coalesce(max(id),0) from MarketOrder ")
  Integer getMaxId();
}
