package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.opentime.OpenTime;
import Grupo.C2B9.back.model.market.opentime.OpenTime_;
import org.springframework.data.jpa.domain.Specification;

public class OpenTimeSpecs {

  public static Specification<OpenTime> byMarket(Market market) {
    return (Specification<OpenTime>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.equal(
        root.get(OpenTime_.market),
        market
      );
    };
  }
}
