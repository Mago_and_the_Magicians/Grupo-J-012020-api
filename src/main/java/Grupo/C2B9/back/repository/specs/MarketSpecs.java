package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.Market_;
import Grupo.C2B9.back.model.user.User;
import org.springframework.data.jpa.domain.Specification;

public class MarketSpecs {
  public static Specification<Market> byUser(User user) {
    return (Specification<Market>) (root, criteriaQuery, criteriaBuilder)
      -> {
      return criteriaBuilder.equal(
        root.get(Market_.user),
        user
      );
    };
  }

  public static Specification<Market> byCode(String code) {
    return (Specification<Market>) (root, criteriaQuery, criteriaBuilder)
      -> {
      return criteriaBuilder.equal(
        root.get(Market_.code),
        code
      );
    };
  }
}
