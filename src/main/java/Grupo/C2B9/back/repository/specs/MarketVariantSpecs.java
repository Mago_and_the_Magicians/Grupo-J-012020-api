package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.market.Market_;
import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.market.variant.MarketVariant_;
import Grupo.C2B9.back.model.product.Variant_;
import Grupo.C2B9.back.model.user.User_;
import org.springframework.data.jpa.domain.Specification;

public class MarketVariantSpecs {

  public static Specification<MarketVariant> byMarket(Market market) {
    return (Specification<MarketVariant>) (root, criteriaQuery, criteriaBuilder)
      -> {
      return criteriaBuilder.equal(
        root.get(MarketVariant_.market),
        market
      );
    };
  }

  public static Specification<MarketVariant> byUser(Integer userId) {
    return (Specification<MarketVariant>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.equal(
        root.join(MarketVariant_.market)
          .join(Market_.user)
          .get(User_.id),
        userId
      );
    };
  }

  public static Specification<MarketVariant> byVariant(Integer variantId) {
    return (Specification<MarketVariant>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.equal(
        root.join(MarketVariant_.variant)
          .get(Variant_.id),
        variantId
      );
    };
  }

  public static Specification<MarketVariant> byCode(String code) {
    return (Specification<MarketVariant>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(MarketVariant_.code), code);
  }
}
