package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.Order_;
import Grupo.C2B9.back.model.order.marketorder.enumerations.OrderStatus;
import Grupo.C2B9.back.model.user.User;
import org.springframework.data.jpa.domain.Specification;

public class OrderSpecs {
  public static Specification<Order> byId(Integer id) {
    return (Specification<Order>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Order_.id), id);
  }

  public static Specification<Order> byOrderStatus(OrderStatus orderStatus) {
    return (Specification<Order>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Order_.status), orderStatus);
  }

  public static Specification<Order> byUser(User user) {
    return (Specification<Order>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Order_.user), user);
  }

  public static Specification<Order> byCode(String code) {
    return (Specification<Order>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Order_.code), code);
  }

  public static Specification<Order> notByOrderStatus(OrderStatus orderStatus) {
    return (Specification<Order>) (root, criteriaQuery, criteriaBuilder)
    -> criteriaBuilder.notEqual(root.get(Order_.status), orderStatus);
  }
}
