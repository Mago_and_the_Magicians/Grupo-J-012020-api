package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market_;
import Grupo.C2B9.back.model.market.variant.MarketVariant_;
import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.ProductCategory;
import Grupo.C2B9.back.model.product.Product_;
import Grupo.C2B9.back.model.product.Variant_;
import org.springframework.data.jpa.domain.Specification;

public class ProductSpecs {
  public static Specification<Product> byBrand(String brandName) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.like(criteriaBuilder.upper(root.get(Product_.brand)), "%" + brandName.toUpperCase() + "%");
  }

  public static Specification<Product> byCategory(String category) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Product_.category), ProductCategory.valueOf(category.toUpperCase()));
  }

  public static Specification<Product> byMarket(String marketId) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.equal(
        root.join(Product_.variants)
          .join(Variant_.marketVariants)
          .join(MarketVariant_.market)
          .get(Market_.id),
        marketId
      );
    };
  }

  public static Specification<Product> byMaxPrice(String max) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.lessThanOrEqualTo(
        root.join(Product_.variants)
          .join(Variant_.marketVariants)
          .get(MarketVariant_.price),
        Double.valueOf(max));
    };
  }

  public static Specification<Product> byMinPrice(String min) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.greaterThanOrEqualTo(
        root.join(Product_.variants)
          .join(Variant_.marketVariants)
          .get(MarketVariant_.price),
        Double.valueOf(min));
    };
  }

  // TODO: revisar cuando son primera y ultima palabra
  public static Specification<Product> byName(String name) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.like(criteriaBuilder.upper(root.get(Product_.name)), "%" + name.toUpperCase() + "%");
  }

  public static Specification<Product> active() {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.greaterThan(root.get(Product_.id), -1);
  }

  public static Specification<Product> byCode(String code) {
    return (Specification<Product>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Product_.code), code);

  }
}
