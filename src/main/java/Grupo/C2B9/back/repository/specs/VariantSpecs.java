package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market_;
import Grupo.C2B9.back.model.market.variant.MarketVariant_;
import Grupo.C2B9.back.model.product.Product;
import Grupo.C2B9.back.model.product.Variant;
import Grupo.C2B9.back.model.product.Variant_;
import org.springframework.data.jpa.domain.Specification;

public class VariantSpecs {


  public static Specification<Variant> active() {
    return (Specification<Variant>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.greaterThan(root.get(Variant_.id), -1);
  }

  public static Specification<Variant> byMarket(String marketId) {
    return (Specification<Variant>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.equal(
        root.join(Variant_.marketVariants)
          .join(MarketVariant_.market)
          .get(Market_.id),
        marketId
      );
    };
  }

  public static Specification<Variant> byProduct(Product product) {
    return (Specification<Variant>) (root, criteriaQuery, criteriaBuilder)
      -> {
      return criteriaBuilder.equal(
        root.get(Variant_.product),
        product
      );
    };
  }


  public static Specification<Variant> byCode(String code) {
    return (Specification<Variant>) (root, criteriaQuery, criteriaBuilder)
      -> {
      return criteriaBuilder.equal(
        root.get(Variant_.code),
        code
      );
    };
  }
}
