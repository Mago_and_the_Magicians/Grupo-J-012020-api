package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder_;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping;
import Grupo.C2B9.back.model.order.marketorder.shipping.Shipping_;
import org.springframework.data.jpa.domain.Specification;

public class ShippingSpecs {
  public static Specification<Shipping> byMarketOrder(MarketOrder marketOrder) {
    return (Specification<Shipping>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Shipping_.marketOrder), marketOrder);
  }

  public static Specification<Shipping> byMarket(Market market) {
    return (Specification<Shipping>) (root, criteriaQuery, criteriaBuilder)
      -> {
      criteriaQuery.distinct(true);
      return criteriaBuilder.equal(
        root.join(Shipping_.marketOrder)
          .get(MarketOrder_.market),
        market
      );
    };
  }
}
