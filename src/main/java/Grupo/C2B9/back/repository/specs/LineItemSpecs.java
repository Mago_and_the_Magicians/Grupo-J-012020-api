package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.variant.MarketVariant;
import Grupo.C2B9.back.model.order.lineItem.LineItem;
import Grupo.C2B9.back.model.order.lineItem.LineItem_;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import org.springframework.data.jpa.domain.Specification;

public class LineItemSpecs {

  public static Specification<LineItem> byId(Integer id) {
    return (Specification<LineItem>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(LineItem_.id), id);
  }


  public static Specification<LineItem> byMarketOrder(MarketOrder marketOrder) {
    return (Specification<LineItem>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(LineItem_.marketOrder), marketOrder);
  }

  public static Specification<LineItem> byMarketVariant(MarketVariant marketVariant) {
    return (Specification<LineItem>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(LineItem_.marketVariant), marketVariant);
  }
}
