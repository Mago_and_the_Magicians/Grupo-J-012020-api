package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.market.Market;
import Grupo.C2B9.back.model.order.Order;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder;
import Grupo.C2B9.back.model.order.marketorder.MarketOrder_;
import org.springframework.data.jpa.domain.Specification;

public class MarketOrderSpecs {

  public static Specification<MarketOrder> byId(Integer id) {
    return (Specification<MarketOrder>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(MarketOrder_.id), id);
  }

  public static Specification<MarketOrder> byMarket(Market market) {
    return (Specification<MarketOrder>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(MarketOrder_.market), market);
  }

  public static Specification<MarketOrder> byOrder(Order order) {
    return (Specification<MarketOrder>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(MarketOrder_.order), order);
  }
}
