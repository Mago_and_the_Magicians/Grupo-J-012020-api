package Grupo.C2B9.back.repository.specs;

import Grupo.C2B9.back.model.user.Address;
import Grupo.C2B9.back.model.user.Address_;
import Grupo.C2B9.back.model.user.User;
import org.springframework.data.jpa.domain.Specification;

public class AddressSpecs {

  public static Specification<Address> byUser(User user) {
    return (Specification<Address>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Address_.user), user);
  }

  public static Specification<Address> byCode(String code) {
    return (Specification<Address>) (root, criteriaQuery, criteriaBuilder)
      -> criteriaBuilder.equal(root.get(Address_.code), code);
  }
}
