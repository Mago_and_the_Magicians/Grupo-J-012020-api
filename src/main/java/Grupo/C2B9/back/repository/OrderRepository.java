package Grupo.C2B9.back.repository;

import Grupo.C2B9.back.model.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>, JpaSpecificationExecutor<Order> {


  @Query("select coalesce(max(id),0) from User_Order ")
  Integer getMaxId();
}
